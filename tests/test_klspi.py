from concurrent import futures
import copy
import functools
import itertools
import logging
import multiprocessing as mproc
import operator
import os
import random
import sys
import unittest
import warnings

import numpy as np


sys.path.insert(1, os.path.abspath(os.path.join(
    os.path.dirname(__file__), os.pardir)
))
from hrl.klspi import *
from hrl.simple import SimpleMDP, SimpleMultiMDP


@unittest.skip("Not working reliably")
class TestSingleKLSPI(unittest.TestCase):
    def simple_preprocess(self, sa):
        (s, a) = sa
        S = self.mdp.state_space
        A = self.mdp.action_space

        out = np.empty(1 + 1)
        # out[:len(S)] = 0
        # out[S.index(s)] = 1

        # out[len(S):] = 0
        # out[len(S) + A.index(a)] = 1

        out[0] = S.index(s) / (len(S) - 1) * 2 - 1

        action_map = {'stay': 0.0, 'left': -1.0, 'right': 1.0}
        out[-1] = action_map[a['ag0']] * 0.5

        #out += np.random.randn(2) * 0.1

        return out

    def simple_rbf_kernel(self, sa1, sa2, sigma_sq):
        pp1 = self.simple_preprocess(sa1)
        pp2 = self.simple_preprocess(sa2)

        #return (1 + np.vdot(pp1, pp2)) ** 7
        return rbf_kernel(pp1, pp2, sigma_sq)

    def setUp(self):
        # random.seed(291893077)
        # np.random.seed(291893077)

        self.mdp = SimpleMDP()
        self.kern = functools.partial(self.simple_rbf_kernel, sigma_sq=0.5**2)
        self.gamma = 0.5
        self.klspi = KLSPILearner(self.mdp, kern=self.kern,
                                  gamma=self.gamma,
                                  ald_thresh=1e-4,
                                  epsilon=0)

    # def test_make_policy(self, ql):
    #     ql.learn(500)
    #     p = ql.policy()
    #     Q = ql.Qs['ag0']
    #     S = self.mdp.state_space
    #     A = self.mdp.action_space.sub('ag0')
    #     for s in S:
    #         a_p = p(s)['ag0']
    #         a_ql = ql.greedy_action(s)['ag0']
    #         self.assertAlmostEqual(Q[S.index(s), A.index(a_p)],
    #                                Q[S.index(s), A.index(a_ql)])

    def test_optimal_policy(self):
        def S(n):
            return self.mdp.state_space.make_item(ag0=n)

        for _ in range(50):
            self.klspi.learn(500)
            self.mdp.reset()
        p = self.klspi.policy()

        self.assertEqual(p(S('A'))['ag0'], 'right')
        self.assertEqual(p(S('B'))['ag0'], 'right')
        self.assertEqual(p(S('C'))['ag0'], 'right')
        self.assertEqual(p(S('D'))['ag0'], 'stay')

    def test_optimal_policy_deterministic_mdp(self):
        def S(n):
            return self.mdp.state_space.make_item(ag0=n)

        self.mdp.epsilon = 0

        for _ in range(50):
            self.klspi.learn(500)
            self.mdp.reset()
        p = self.klspi.policy()

        self.assertEqual(p(S('A'))['ag0'], 'right')
        self.assertEqual(p(S('B'))['ag0'], 'right')
        self.assertEqual(p(S('C'))['ag0'], 'right')
        self.assertEqual(p(S('D'))['ag0'], 'stay')

    def test_q_estimation(self):
        def S(n):
            return self.mdp.state_space.make_item(ag0=n)

        def A(n):
            return self.mdp.action_space.make_item(ag0=n)

        self.mdp.epsilon = 0
        self.klspi.gamma = 0.05

        self.klspi.learn(3000)
        Q = self.klspi.Q

        expected = functools.partial(self.mdp.randomQ,
                                     gamma=self.klspi.gamma, maxsteps=10)

        self.assertAlmostEqual(Q(S('A'), A('stay')),
                               expected(S('A'), A('stay')), places=1)
        self.assertAlmostEqual(Q(S('A'), A('right')),
                               expected(S('A'), A('right')), places=1)
        self.assertAlmostEqual(Q(S('B'), A('left')),
                               expected(S('B'), A('left')), places=1)
        self.assertAlmostEqual(Q(S('B'), A('stay')),
                               expected(S('B'), A('stay')), places=1)
        self.assertAlmostEqual(Q(S('B'), A('right')),
                               expected(S('B'), A('right')), places=1)
        self.assertAlmostEqual(Q(S('C'), A('left')),
                               expected(S('C'), A('left')), places=1)
        self.assertAlmostEqual(Q(S('C'), A('stay')),
                               expected(S('C'), A('stay')), places=1)
        self.assertAlmostEqual(Q(S('C'), A('right')),
                               expected(S('C'), A('right')), places=1)
        self.assertAlmostEqual(Q(S('D'), A('stay')),
                               expected(S('D'), A('stay')), places=1)
        self.assertAlmostEqual(Q(S('D'), A('left')),
                               expected(S('D'), A('left')), places=1)


    def test_optimal_q_deterministic_mdp(self):
        def S(n):
            return self.mdp.state_space.make_item(ag0=n)

        def A(n):
            return self.mdp.action_space.make_item(ag0=n)

        self.mdp.epsilon = 0

        for _ in range(50):
            self.klspi.learn(500)
            self.mdp.reset()
        Q = self.klspi.Q

        def expected(steps):
            return sum(-1 * self.gamma ** i for i in range(steps))

        self.assertAlmostEqual(Q(S('A'), A('stay')), expected(4), delta=0.5)
        self.assertAlmostEqual(Q(S('A'), A('right')), expected(3), delta=0.5)
        self.assertAlmostEqual(Q(S('B'), A('left')), expected(4), delta=0.5)
        self.assertAlmostEqual(Q(S('B'), A('stay')), expected(3), delta=0.5)
        self.assertAlmostEqual(Q(S('B'), A('right')), expected(2), delta=0.5)
        self.assertAlmostEqual(Q(S('C'), A('left')), expected(3), delta=0.5)
        self.assertAlmostEqual(Q(S('C'), A('stay')), expected(2), delta=0.5)
        self.assertAlmostEqual(Q(S('C'), A('right')), expected(1), delta=0.5)
        self.assertAlmostEqual(Q(S('D'), A('stay')), 0, delta=0.5)
        self.assertAlmostEqual(Q(S('D'), A('left')), expected(2), delta=0.5)
