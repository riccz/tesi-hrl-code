import os
import sys
import unittest

import numpy as np


sys.path.insert(1, os.path.abspath(os.path.join(
    os.path.dirname(__file__), os.pardir)
))
from hrl.lookahead import *
from hrl.simple import SimpleMDP


class TestLookaheadLearner(unittest.TestCase):
    def setUp(self):
        self.mdp = SimpleMDP()
        self.la_long = LookaheadLearner(self.mdp, gamma=0.4, la_steps=6)

    def test_optimal_policy_deterministic_mdp(self):
        def S(n):
            return self.mdp.state_space.make_item(ag0=n)

        self.mdp.epsilon = 0

        p = self.la_long.policy()

        self.assertEqual(p(S('A'))['ag0'], 'right')
        self.assertEqual(p(S('B'))['ag0'], 'right')
        self.assertEqual(p(S('C'))['ag0'], 'right')
        self.assertEqual(p(S('D'))['ag0'], 'stay')

    def test_optimal_policy(self):
        def S(n):
            return self.mdp.state_space.make_item(ag0=n)

        self.mdp.epsilon = 0.1

        p = self.la_long.policy()

        self.assertEqual(p(S('A'))['ag0'], 'right')
        self.assertEqual(p(S('B'))['ag0'], 'right')
        self.assertEqual(p(S('C'))['ag0'], 'right')
        self.assertEqual(p(S('D'))['ag0'], 'stay')

    def test_optimal_q_deterministic_mdp(self):
        def Si(n):
            return self.mdp.state_space.index({'ag0': n})

        def Ai(n):
            return self.mdp.action_space.sub('ag0').index(n)

        self.mdp.epsilon = 0
        self.la_long.gamma = 1

        Q = np.empty((len(self.mdp.state_space),
                      len(self.mdp.action_space.sub('ag0'))))
        for s in self.mdp.state_space:
            for a, Qsa in self.la_long.allQs('ag0', s):
                Q[Si(s['ag0']), Ai(a)] = Qsa

        self.assertAlmostEqual(Q[Si('A'), Ai('stay')], -4)
        self.assertAlmostEqual(Q[Si('A'), Ai('right')], -3)
        self.assertAlmostEqual(Q[Si('B'), Ai('left')], -4)
        self.assertAlmostEqual(Q[Si('B'), Ai('stay')], -3)
        self.assertAlmostEqual(Q[Si('B'), Ai('right')], -2)
        self.assertAlmostEqual(Q[Si('C'), Ai('left')], -3)
        self.assertAlmostEqual(Q[Si('C'), Ai('stay')], -2)
        self.assertAlmostEqual(Q[Si('C'), Ai('right')], -1)
        self.assertAlmostEqual(Q[Si('D'), Ai('stay')], 0)
        self.assertAlmostEqual(Q[Si('D'), Ai('left')], -2)
