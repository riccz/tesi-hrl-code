import math
import os
import random
import sys
import unittest

import numpy as np

sys.path.insert(1, os.path.abspath(os.path.join(
    os.path.dirname(__file__), os.pardir)
))
from hrl import utils
from hrl.utils import *

class TestClippedNormal(unittest.TestCase):
    def setUp(self):
        random.seed(4223128210)
        np.random.seed(4223128210)

        self.mus = [random.uniform(-100, 100) for _ in range(5)]
        self.sigmas = [random.uniform(0, 10) for _ in range(5)]
        self.ks = [random.uniform(0.1, 6) for _ in range(5)]

    @property
    def params(self):
        return itertools.product(self.mus, self.sigmas, self.ks)

    def test_full_range(self):
        for mu, sigma, k in self.params:
            with self.subTest(mu=mu, sigma=sigma, k=k):
                i = clipped_norm_integral(-math.inf, math.inf,
                                          mu=mu, sigma=sigma, k=k)
                self.assertAlmostEqual(i, 1)

    def test_symmetric(self):
        for mu, sigma, k in self.params:
            with self.subTest(mu=mu, sigma=sigma, k=k):
                i = clipped_norm_integral(mu, math.inf,
                                          mu=mu, sigma=sigma, k=k)
                j = clipped_norm_integral(-math.inf, mu,
                                          mu=mu, sigma=sigma, k=k)
                self.assertAlmostEqual(i, 0.5)
                self.assertAlmostEqual(j, 0.5)

    def test_clipping(self):
        for mu, sigma, k in self.params:
            with self.subTest(mu=mu, sigma=sigma, k=k):
                i = clipped_norm_integral(mu, mu+k*sigma,
                                          mu=mu, sigma=sigma, k=k)
                j = clipped_norm_integral(mu+k*sigma, math.inf,
                                          mu=mu, sigma=sigma, k=k)
                self.assertAlmostEqual(i, 0.5)
                self.assertAlmostEqual(j, 0)


class TestUnFlattenDicts(unittest.TestCase):
    def test_unflatten_lists(self):
        flat = {'x': [1, 2, 3], 'y': [4, 5, 6], 'z': [7, 8, 9]}
        unflat = list(utils.iter.unflatten_dicts(flat))
        self.assertEqual(len(unflat), 3)
        self.assertDictEqual(unflat[0], {'x': 1, 'y': 4, 'z': 7})
        self.assertDictEqual(unflat[1], {'x': 2, 'y': 5, 'z': 8})
        self.assertDictEqual(unflat[2], {'x': 3, 'y': 6, 'z': 9})


class TestHashWrap(unittest.TestCase):
    def test_hash_wrapper(self):
        x = 42
        hw = utils.misc.HashWrap(x, hash)
        self.assertEqual(hash(x), hash(hw))

    def test_wrap_unhashable(self):
        x = [1, 2, 3]

        def hashfunc(x):
            return hash(tuple(x))

        hw = utils.misc.HashWrap(x, hashfunc)
        self.assertEqual(hash(tuple(x)), hash(hw))
        with self.assertRaises(TypeError):
            hash(x)

    def test_hash_wrap_factory(self):
        wrapper = utils.misc.hash_wrap_factory(hash)
        xs = [1, 2, 3, 4, 5]
        hws = [wrapper(x) for x in xs]
        self.assertSequenceEqual([hash(hw) for hw in hws],
                                 [hash(x) for x in xs])


class TestAverageCounter(unittest.TestCase):
    def setUp(self):
        self.avgc = utils.stats.AverageCounter()
        self.avgc_hist = utils.stats.AverageCounter(keep_all=True)

    def test_simple_average(self):
        for n in 'avgc', 'avgc_hist':
            with self.subTest(n=n):
                avgc = getattr(self, n)
                samples = 10 * np.random.randn(50000) - 5

                for s in samples:
                    avgc.add(s)

                self.assertAlmostEqual(np.mean(samples), avgc.average)
                self.assertAlmostEqual(np.std(samples), avgc.std_dev)
                self.assertAlmostEqual(np.var(samples), avgc.variance)
                self.assertEqual(len(samples), avgc.total_weight)

                if avgc.keep_all:
                    saved_samples = [s for s, w in avgc.samples]
                    saved_weights = [w for s, w in avgc.samples]
                    self.assertTrue(np.all(samples == saved_samples))
                    self.assertTrue(all(w == 1 for w in saved_weights))

    def test_weighted_average(self):
        for n in 'avgc', 'avgc_hist':
            with self.subTest(n=n):
                avgc = getattr(self, n)
                samples = 10 * np.random.randn(50000) - 5
                weights = np.random.rand(len(samples)) * 2 + 0.5

                for s, w in zip(samples, weights):
                    avgc.add(s, w)

                npavg = np.average(samples, weights=weights)
                self.assertAlmostEqual(npavg, avgc.average)
                # self.assertAlmostEqual(np.std(weighted_samples), avgc.std_dev)
                # self.assertAlmostEqual(np.var(weighted_samples), avgc.variance)
                self.assertEqual(sum(weights), avgc.total_weight)

                if avgc.keep_all:
                    saved_samples = [s for s, w in avgc.samples]
                    saved_weights = [w for s, w in avgc.samples]
                    self.assertTrue(np.all(samples == saved_samples))
                    self.assertTrue(np.all(weights == saved_weights))