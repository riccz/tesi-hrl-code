from unittest import mock
import copy
import os
import sys
import tempfile
import unittest

import h5py
import numpy as np

sys.path.insert(1, os.path.abspath(os.path.join(
    os.path.dirname(__file__), os.pardir)
))

from hrl_app import test_and_time, aggregate_test_and_time
from hrl.abstr import Learner
from hrl.grid import GridMDP
from hrl.mcts import MCTS
from hrl.qlearn import (
    IndepQLearners,
    hyp_decay,
    SoftmaxPolicy,
    EpsilonGreedyPolicy,
)
import hrl


def make_eps_greedy(start, exp):
    return EpsilonGreedyPolicy(hyp_decay(start=start, exp=exp))


def make_softmax(start, exp):
    return SoftmaxPolicy(hyp_decay(start=start, exp=exp))


def same_Qs(lhs, rhs):
    assert(lhs.mdp.agent_keys == rhs.mdp.agent_keys)
    for ag in lhs.mdp.agent_keys:
        if np.any(lhs.Qs[ag] != rhs.Qs[ag]):
            return False
    return True


class MockUpload(mock.Mock):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs, side_effect=self._record_uploaded)
        self.uploaded_files = []

    def _record_uploaded(self, fname, bucket, key, **kwargs):
        tmp = tempfile.NamedTemporaryFile(delete=False)
        self.uploaded_files.append(tmp)
        with open(fname, 'rb') as f:
            data = f.read()
            tmp.write(data)
        tmp.seek(0)
        return mock.DEFAULT

    def uploaded_as_dict(self):
        return dict((c[0][2], u) for c, u in
                    zip(self.call_args_list, self.uploaded_files))

    def __del__(self):
        for f in self.uploaded_files:
            f.close()
            if os.path.exists(f.name):
                os.unlink(f.name)


class MockFetch(mock.Mock):
    def __init__(self, *args, fetch_list={}, **kwargs):
        super().__init__(*args, **kwargs, side_effect=self._fetch)
        self.fetch_list = fetch_list

    def _fetch(self, key):
        f = self.fetch_list[key]
        f.close()
        return h5py.File(f.name, 'r')


class TestTestAndTime(unittest.TestCase):
    def setUp(self):
        self.grid = GridMDP(agents=2,
                            grid_radius=1,
                            grid_steps=3,
                            b_steps=4,
                            d_steps=3,
                            theta_steps=4,
                            avg_steps_off=10,
                            avg_steps_on=3)
        self.S = self.grid.state_space
        self.A = self.grid.action_space

    def test_single_test_time(self):
        learner = IndepQLearners(self.grid,
                                 alpha=hyp_decay(start=0.8, exp=0.6,
                                                 flat=3000),
                                 explore_policy=make_softmax(0.7, 0.1),
                                 gamma=0.6,
                                 update_method='RUQL')
        learner.learn(500)
        learner.test_time_counter.keep_all = True
        start_data = {'learner': learner.pack()}
        result = test_and_time(start_data, test_iters=500)
        self.assertEqual(result['tot_iters'], 500)
        self.assertIn('avgr', result)
        self.assertIn('learner', result)
        result_learner = Learner.unpack(result['learner'])
        self.assertIs(type(result_learner), IndepQLearners)
        self.assertTrue(same_Qs(learner, result_learner))
        times = result_learner.test_time_counter.samples
        self.assertEqual(len(times), 500)

    @mock.patch('hrl.utils.aws.upload_file', new_callable=MockUpload)
    def test_single_history(self, mock_upload):
        learner = IndepQLearners(self.grid,
                                 alpha=hyp_decay(start=0.8, exp=0.6,
                                                 flat=3000),
                                 explore_policy=make_softmax(0.7, 0.1),
                                 gamma=0.6,
                                 update_method='RUQL')
        learner.learn(500)
        learner.test_time_counter.keep_all = True
        start_data = {'learner': learner.pack()}
        result = test_and_time(start_data, test_iters=500, hist_label='test')
        self.assertEqual(result['tot_iters'], 500)
        self.assertIn('avgr', result)
        self.assertIn('learner', result)
        self.assertIn('hist_list', result)

        self.assertEqual(len(result['hist_list']), 1)
        histkey = result['hist_list'][0]
        self.assertIn('test', histkey)

        histfile = mock_upload.uploaded_files[0]
        histfile.close()
        histfile = h5py.File(histfile.name, 'r')

        for comp in ['states', 'actions', 'rewards', 'new_states']:
            self.assertIn(comp, histfile)
            self.assertEqual(histfile[comp].shape, (500,))

    @mock.patch('hrl.utils.aws.upload_file', new_callable=MockUpload)
    def test_chain_aggregate(self, mock_upload):
        learner = IndepQLearners(self.grid,
                                 alpha=hyp_decay(start=0.8, exp=0.6,
                                                 flat=3000),
                                 explore_policy=make_softmax(0.7, 0.1),
                                 gamma=0.6,
                                 update_method='RUQL')
        learner.learn(500)
        learner.test_time_counter.keep_all = True
        start_data = {'learner': learner.pack()}
        result1 = test_and_time(start_data, test_iters=500,
                                hist_label='hist')
        result2 = test_and_time(copy.deepcopy(result1), test_iters=500,
                                hist_label='hist')
        result3 = test_and_time(copy.deepcopy(result2), test_iters=500,
                                hist_label='hist')

        # Aggregate iteration count
        self.assertEqual(result1['tot_iters'], 500)
        self.assertEqual(result2['tot_iters'], 1000)
        self.assertEqual(result3['tot_iters'], 1500)

        # Aggregate rewards
        all_rewards = np.empty((1500, self.grid.agents))
        for j, r in enumerate((result1, result2, result3)):
            histfile = mock_upload.uploaded_files[j]
            histfile.close()
            histfile = h5py.File(histfile.name, 'r')

            for i, ag in enumerate(self.grid.agent_keys):
                all_rewards[500*j:500*(j+1), i] = histfile['rewards'][ag]
            histfile.close()

        mean_avgr = np.mean(all_rewards)
        self.assertAlmostEqual(mean_avgr, result3['avgr'])

        # Times
        for j, r in enumerate((result1, result2, result3)):
            learner = Learner.unpack(r['learner'])
            times = learner.test_time_counter.samples
            self.assertEqual(len(times), 500*(j+1))

        # History list
        uploaded_keys = [c[0][2] for c in mock_upload.call_args_list]
        self.assertSequenceEqual(result3['hist_list'], uploaded_keys)

        # Histories linked by the last state
        # 1 -> 2
        histfile = h5py.File(mock_upload.uploaded_files[0].name, 'r')
        last_state = self.S.unpack(histfile['new_states'][-1])
        histfile = h5py.File(mock_upload.uploaded_files[1].name, 'r')
        first_state = self.S.unpack(histfile['states'][0])
        self.assertTrue(self.S.items_eq(last_state, first_state))
        # 2 -> 3
        histfile = h5py.File(mock_upload.uploaded_files[1].name, 'r')
        last_state = self.S.unpack(histfile['new_states'][-1])
        histfile = h5py.File(mock_upload.uploaded_files[2].name, 'r')
        first_state = self.S.unpack(histfile['states'][0])
        self.assertTrue(self.S.items_eq(last_state, first_state))

    @mock.patch('hrl.utils.aws.aws_fetch_data', new_callable=MockFetch)
    @mock.patch('hrl.utils.aws.upload_file', new_callable=MockUpload)
    def test_auto_aggregate(self, mock_upload, mock_fetch):
        learner = IndepQLearners(self.grid,
                                 alpha=hyp_decay(start=0.8, exp=0.6, flat=3000),
                                 explore_policy=make_softmax(0.7, 0.1),
                                 gamma=0.6,
                                 update_method='RUQL')
        learner.learn(500)
        learner.test_time_counter.keep_all = True
        start_data = {'learner': learner.pack()}
        result11 = test_and_time(start_data, test_iters=500)
        result12 = test_and_time(copy.deepcopy(result11), test_iters=500)
        result13 = test_and_time(copy.deepcopy(result12), test_iters=500)

        result21 = test_and_time(start_data, test_iters=500,
                                 hist_label='hist')
        result22 = test_and_time(copy.deepcopy(result21), test_iters=500,
                                 hist_label='hist')
        result23 = test_and_time(copy.deepcopy(result22), test_iters=500,
                                 hist_label='hist')

        mock_fetch.fetch_list = mock_upload.uploaded_as_dict()
        key = aggregate_test_and_time(result13, result23,
                                      upload_label='test')
        final_key = mock_upload.call_args[0][2]
        self.assertEqual(key, final_key)
        final_data = mock_upload.uploaded_files[-1]
        final_data.close()
        final_data = h5py.File(final_data.name, 'r')
        self.assertEqual(final_data['plot_data']['test_times'].shape, (3000,))
        self.assertEqual(final_data['plot_data']['avg_rewards'].shape, (2,))
        self.assertEqual(final_data['plot_data']['avg_rewards'][0],
                         result13['avgr'])
        self.assertEqual(final_data['plot_data']['avg_rewards'][1],
                         result23['avgr'])
        for j, r in enumerate((result13, result23)):
            lr = Learner.unpack(r['learner'])
            times = np.array([s[0] for s in lr.test_time_counter.samples])
            aggr_times = final_data['plot_data']['test_times']
            self.assertTrue(np.all(times == aggr_times[j*1500:(j+1)*1500]))

        self.assertIn('hist_1', final_data)
        hist_kws = dict(final_data['hist_1'].items())
        full_hist = hrl.utils.pack.unpack_history(learner.mdp, **hist_kws)
        self.assertEqual(len(full_hist), 1500)


if __name__ == '__main__':
    unittest.main()
