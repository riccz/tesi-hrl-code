import logging
import unittest

logging.disable(logging.INFO)


def _discover_tests():
    """Used by setup.py to discover the test modules"""
    return unittest.defaultTestLoader.discover('tests', pattern='test_*.py')
