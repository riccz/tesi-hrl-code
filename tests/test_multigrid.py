import copy
import itertools
import math
import os
import random
import sys
import unittest

import numpy as np

sys.path.insert(1, os.path.abspath(os.path.join(
    os.path.dirname(__file__), os.pardir)
))
from abstr_tests import TestInvSeqInterface
#from hrl.multigrid import *

# @unittest.skip
# class TestMultiGrid(TestMDPInterface, unittest.TestCase):
#     def setUp(self):
#         # random.seed(4031292763)
#         # np.random.seed(4031292763)

#         self._mdp = MultiGridMDP(agents=4,
#                                  x_steps=3,x_min=-1,x_max=1,
#                                  y_steps=3,y_min=-1,y_max=1,
#                                  b_steps=5,
#                                  r_epsilon=1e-3,
#                                  r_phi_up=1.05, r_phi_down=0.8)

#     @property
#     def mdp(self):
#         return self._mdp

#     def test_starting_state(self):
#         s = self.mdp.starting_state
#         self.assertEqual(len(s.variables), 4)
#         for ag in self.mdp.agent_keys:
#             s_ag = s[ag]
#             self.assertEqual(s_ag.pos.x, 0)
#             self.assertEqual(s_ag.pos.y, 0)
#             self.assertEqual(s_ag.b, 1)

#     def test_stay_at_start(self):
#         action_kw = {ag: 'stay' for ag in self.mdp.agent_keys}
#         action = self.A(**action_kw)
#         (s, r, ns) = self.mdp.do_action(action)
#         self.assertEqual(s, self.mdp.starting_state)
#         self.assertEqual(ns, self.mdp.starting_state)
#         self.assertEqual(self.mdp.state, self.mdp.starting_state)

#         self.assertDictEqual(r, {ag: 0.0 for ag in self.mdp.agent_keys})

#         self.assertEqual(self.mdp.time, 1)

# @unittest.skip
# class TestMultiGrid2Agents(TestMDPInterface, unittest.TestCase):
#     def setUp(self):
#         # random.seed(4031292763)
#         # np.random.seed(4031292763)

#         self._mdp = MultiGridMDP(agents=2,
#                                  x_steps=3,x_min=-1,x_max=1,
#                                  y_steps=3,y_min=-1,y_max=1,
#                                  b_steps=10,
#                                  r_epsilon=1e-3,
#                                  r_phi_up=1.05, r_phi_down=0.8)

#         self.A0 = self.mdp.action_space.ag0
#         self.A1 = self.mdp.action_space.ag1

#         self.S0 = self.mdp.state_space.ag0
#         self.S1 = self.mdp.state_space.ag1

#     @property
#     def mdp(self):
#         return self._mdp

#     def test_valid_actions_at_border(self):
#         s = self.mdp.state_space(ag0={
#             'pos':{'x': 1, 'y':-1},
#             'b': 0.75
#         }, ag1={
#             'pos':{'x': -1, 'y':0},
#             'b': 1
#         })

#         actions = self.mdp.valid_actions(s)
#         self.assertEqual(len(actions), 2)
#         actions0 = actions['ag0']
#         actions1 = actions['ag1']

#         for a in actions0:
#             self.assertIn(a, self.A0)
#         for a in actions1:
#             self.assertIn(a, self.A1)

#         acts0str = [str(a) for a in actions0]
#         acts1str = [str(a) for a in actions1]
#         self.assertSequenceEqual(sorted(acts0str),
#                                  sorted(['stay', 'up', 'left']))
#         self.assertSequenceEqual(sorted(acts1str),
#                                  sorted(['stay', 'down', 'up', 'right']))

#     def test_valid_actions_collision_diagonal(self):
#         s = self.mdp.state_space(ag0={
#             'pos':{'x': 1, 'y':-1},
#             'b': 0.75
#         }, ag1={
#             'pos':{'x': 0, 'y':0},
#             'b': 1
#         })

#         actions = self.mdp.valid_actions(s)
#         self.assertEqual(len(actions), 2)
#         actions0 = actions['ag0']
#         actions1 = actions['ag1']

#         for a in actions0:
#             self.assertIn(a, self.A0)
#         for a in actions1:
#             self.assertIn(a, self.A1)

#         acts0str = [str(a) for a in actions0]
#         acts1str = [str(a) for a in actions1]
#         self.assertSequenceEqual(acts0str,['stay'])
#         self.assertSequenceEqual(sorted(acts1str),
#                                  sorted(['stay', 'left', 'up']))

#     def test_valid_actions_collision_aligned(self):
#         s = self.mdp.state_space(ag0={
#             'pos':{'x': -1, 'y':1},
#             'b': 0.75
#         }, ag1={
#             'pos':{'x': 1, 'y':1},
#             'b': 1
#         })

#         actions = self.mdp.valid_actions(s)
#         self.assertEqual(len(actions), 2)
#         actions0 = actions['ag0']
#         actions1 = actions['ag1']

#         for a in actions0:
#             self.assertIn(a, self.A0)
#         for a in actions1:
#             self.assertIn(a, self.A1)

#         acts0str = [str(a) for a in actions0]
#         acts1str = [str(a) for a in actions1]
#         self.assertSequenceEqual(sorted(acts0str),
#                                  sorted(['stay', 'down']))
#         self.assertSequenceEqual(sorted(acts1str),
#                                  sorted(['stay', 'down']))

#     def test_valid_actions_empty_battery(self):
#         s = self.mdp.state_space(ag0={
#             'pos':{'x': -1, 'y':1},
#             'b': 0
#         }, ag1={
#             'pos':{'x': 0, 'y':1},
#             'b': 1
#         })

#         actions = self.mdp.valid_actions(s)
#         self.assertEqual(len(actions), 2)
#         actions0 = actions['ag0']
#         actions1 = actions['ag1']
#         self.assertEqual(len(actions0), 1)
#         a1str = [str(a) for a in actions1]
#         self.assertSequenceEqual(sorted(a1str),
#                                  sorted(['stay', 'down', 'right', 'left']))

#     @unittest.skip("not implemented")
#     def test_transition_probabilities(self):
#         s = self.S(ag0={
#             'pos':{'x': -1, 'y':1},
#             'b': 0.75
#         }, ag1={
#             'pos':{'x': 0, 'y':1},
#             'b': 1
#         })

#         a = self.A(ag0='down', ag1='right')

#         new_s = self.S(ag0={
#             'pos':{'x': -1, 'y':0},
#             'b': 0.55
#         }, ag1={
#             'pos':{'x': 1, 'y':1},
#             'b': 0.75
#         })

#         self.assertEqual(self.mdp.transition_prob(s=s, a=a, new_s=new_s), 1.0)
#         other_states = [s for s in self.S if s != new_s]
#         for os in other_states:
#             self.assertEqual(self.mdp.transition_prob(s=s, a=a, new_s=os), 0.0)

#     def test_reward_full(self):
#         s = self.S(ag0={
#             'pos':{'x': -1, 'y':1},
#             'b': 0.75
#         }, ag1={
#             'pos':{'x': 0, 'y':1},
#             'b': 1
#         })

#         a = self.A(ag0='down', ag1='right')

#         new_s = self.S(ag0={
#             'pos':{'x': -1, 'y':0},
#             'b': 0.55
#         }, ag1={
#             'pos':{'x': 1, 'y':1},
#             'b': 0.75
#         })

#         rs = self.mdp.reward(s=s, a=a, new_s=new_s)
#         self.assertDictEqual(rs, {'ag0': 1.0, 'ag1': 1.0})

#     def test_reward_multistep(self):
#         s0 = self.S(ag0={
#             'pos':{'x': -1, 'y':1},
#             'b': 0.75
#         }, ag1={
#             'pos':{'x': 0, 'y':1},
#             'b': 1
#         })
#         a0 = self.A(ag0='stay', ag1='stay')
#         s1 = self.S(ag0={
#             'pos':{'x': -1, 'y':1},
#             'b': 0.65
#         }, ag1={
#             'pos':{'x': 0, 'y':1},
#             'b': 0.85
#         })

#         rs1 = self.mdp.reward(s=s0, a=a0, new_s=s1)
#         self.assertDictEqual(rs1, {'ag0': 1.0, 'ag1': 1.0})

#         self.mdp._reward.update(s1)
#         a1 = self.A(ag0='stay', ag1='stay')
#         s2 = self.S(ag0={
#             'pos':{'x': -1, 'y':1},
#             'b': 0.55
#         }, ag1={
#             'pos':{'x': 0, 'y':1},
#             'b': 0.75
#         })

#         rs2 = self.mdp.reward(s=s1, a=a1, new_s=s2)
#         self.assertDictEqual(rs2, {'ag0': 0.8, 'ag1': 0.8})

#         self.mdp._reward.update(s2)
#         a2 = self.A(ag0='stay', ag1='right')
#         s3 = self.S(ag0={
#             'pos':{'x': -1, 'y':1},
#             'b': 0.45
#         }, ag1={
#             'pos':{'x': 1, 'y':1},
#             'b': 0.55
#         })

#         rs3 = self.mdp.reward(s=s2, a=a2, new_s=s3)
#         self.assertDictEqual(rs3, {'ag0': 0.8 ** 2, 'ag1': 1.0})


#     @unittest.skip("implementation changed")
#     def test_move_one_step(self):
#         actions = self.mdp.valid_actions()
#         actions['ag0'].remove('stay')
#         actions['ag1'].remove('stay')
#         a0 = actions['ag0'][0]
#         a1 = actions['ag1'][0]
#         (s, r, ns) = self.mdp.do_action(self.A(ag0=a0, ag1=a1))
#         self.assertEqual(self.mdp.state, ns)

#         bdiffs = [ns[ag].b - s[ag].b for ag in ['ag0', 'ag1']]
#         xdiffs = [ns[ag].pos.x - s[ag].pos.x for ag in ['ag0', 'ag1']]
#         ydiffs = [ns[ag].pos.y - s[ag].pos.y for ag in ['ag0', 'ag1']]

#         self.assertAlmostEqual(bdiffs[0], -0.2)
#         self.assertAlmostEqual(bdiffs[1], -0.2)

#         self.assertAlmostEqual(abs(xdiffs[0]) + abs(ydiffs[0]), 2/3)
#         self.assertAlmostEqual(abs(xdiffs[1]) + abs(ydiffs[1]), 2/3)

#         self.assertDictEqual(r, {'ag0': 1, 'ag1': 1})
#         self.assertEqual(self.mdp.time, 1)

#         next_rs = self.mdp.reward(s=ns, a=self.A(ag0='stay', ag1='stay'),
#                                   new_s=ns)
#         self.assertAlmostEqual(next_rs['ag0'], 0.8)
#         self.assertAlmostEqual(next_rs['ag1'], 0.8)

#     def test_return_step_count(self):
#         self.mdp.state = self.S(ag0={
#             'pos':{'x': -1, 'y':1},
#             'b': 0.0
#         }, ag1={
#             'pos':{'x': 1, 'y':1},
#             'b': 0.0
#         })
#         dist = 2
#         for t in range(dist):
#             a = self.mdp.valid_actions()
#             a0 = a['ag0']
#             a1 = a['ag1']
#             self.assertGreater(len(a0), 0)
#             self.assertGreater(len(a1), 0)
#             (s,r,ns) = self.mdp.do_action(self.A(ag0=random.choice(a0),
#                                                  ag1=random.choice(a1)))
#             self.assertDictEqual(r, {'ag0':0, 'ag1':0})
#         self.assertEqual(self.mdp.state, self.mdp.starting_state)

# @unittest.skip
# class TestReducedMultiGrid(TestInvSeqInterface, unittest.TestCase):
#     def setUp(self):
#         random.seed(1720200902)
#         np.random.seed(1720200902)

#         self.full = MultiGridSpace(agents=2,
#                                    x_min=-100, x_max=100,
#                                    y_min=-100, y_max=100,
#                                    x_steps=6,
#                                    y_steps=6,
#                                    b_steps=4)

#         self.reduced0 = ReducedMultiGridSpace(self.full, 'ag0',
#                                               x_steps=2,
#                                               y_steps=2,
#                                               b_steps=2)

#         self.reduced = ReducedMultiGridSpace.make_each_agent(self.full,
#                                                              x_steps=2,
#                                                              y_steps=2,
#                                                              b_steps=2)

#         self.reduced_empty_batt = ReducedMultiGridSpace.make_each_agent(self.full,
#                                                                         x_steps=2,
#                                                                         y_steps=2,
#                                                                         b_empty=True)

#     @property
#     def invseq(self):
#         return self.reduced0

#     def test_same_range(self):
#         for red in self.reduced.values():
#             for ag in self.full.agent_keys:
#                 self.assertEqual(red.spaces[ag].x_min, self.full.x_min)
#                 self.assertEqual(red.spaces[ag].x_max, self.full.x_max)
#                 self.assertEqual(red.spaces[ag].y_min, self.full.y_min)
#                 self.assertEqual(red.spaces[ag].y_max, self.full.y_max)
#                 self.assertEqual(red.spaces[ag].b.a, self.full.ag0.b.a)
#                 self.assertEqual(red.spaces[ag].b.b, self.full.ag0.b.b)

#     def test_keep_same_steps(self):
#         for ag, red in self.reduced.items():
#             self.assertEqual(red.spaces[ag].x_steps, self.full.x_steps)
#             self.assertEqual(red.spaces[ag].y_steps, self.full.y_steps)
#             self.assertEqual(red.spaces[ag].b_steps, self.full.b_steps)

#     def test_reduce_same_agent_state(self):
#         fullstates = [self.full.random() for _ in range(200)]
#         for fullstate in fullstates:
#             with self.subTest(fullstate=fullstate):
#                 for ag, s in self.reduced.items():
#                     red = s.reduce(fullstate)
#                     self.assertEqual(self.full.spaces[ag].index(fullstate[ag]),
#                                      s.spaces[ag].index(red[ag]))
#                     self.assertEqual(float(red[ag]['b']),
#                                      float(fullstate[ag]['b']))
#                     self.assertEqual(float(red[ag]['pos']['x']),
#                                      float(fullstate[ag]['pos']['x']))
#                     self.assertEqual(float(red[ag]['pos']['y']),
#                                      float(fullstate[ag]['pos']['y']))
#     def test_reduce_other_states(self):
#         fullstates = [self.full.random() for _ in range(200)]
#         for fullstate in fullstates:
#             with self.subTest(fullstate=fullstate):
#                 for ag, s in self.reduced.items():
#                     red = s.reduce(fullstate)
#                     for otherag in filter(lambda oag: oag != ag,
#                                           self.reduced.keys()):
#                         x = float(red[otherag]['pos']['x'])
#                         y = float(red[otherag]['pos']['y'])
#                         b = float(red[otherag]['b'])

#                         xi = lambda r: s.spaces[otherag].pos.x.index(r)
#                         yi = lambda r: s.spaces[otherag].pos.y.index(r)
#                         bi = lambda r: s.spaces[otherag].b.index(r)

#                         if x < 0:
#                             self.assertEqual(xi(red[otherag]['pos']['x']), 0)
#                         elif x > 0:
#                             self.assertEqual(xi(red[otherag]['pos']['x']), 1)

#                         if y < 0:
#                             self.assertEqual(yi(red[otherag]['pos']['y']), 0)
#                         elif y > 0:
#                             self.assertEqual(yi(red[otherag]['pos']['y']), 1)

#                         if b < 0.5:
#                             self.assertEqual(bi(red[otherag]['b']), 0)
#                         elif b > 0.5:
#                             self.assertEqual(bi(red[otherag]['b']), 1)

#     def test_reduce_other_states_empty_battery(self):
#         fullstates = [self.full.random() for _ in range(200)]
#         for fullstate in fullstates:
#             with self.subTest(fullstate=fullstate):
#                 for ag, s in self.reduced_empty_batt.items():
#                     red = s.reduce(fullstate)
#                     for otherag in filter(lambda oag: oag != ag,
#                                           self.reduced_empty_batt.keys()):
#                         x = float(red[otherag]['pos']['x'])
#                         y = float(red[otherag]['pos']['y'])
#                         b = float(red[otherag]['b'])

#                         xi = lambda r: s.spaces[otherag].pos.x.index(r)
#                         yi = lambda r: s.spaces[otherag].pos.y.index(r)
#                         bi = lambda r: s.spaces[otherag].b.index(r)

#                         if x < 0:
#                             self.assertEqual(xi(red[otherag]['pos']['x']), 0)
#                         elif x > 0:
#                             self.assertEqual(xi(red[otherag]['pos']['x']), 1)

#                         if y < 0:
#                             self.assertEqual(yi(red[otherag]['pos']['y']), 0)
#                         elif y > 0:
#                             self.assertEqual(yi(red[otherag]['pos']['y']), 1)

#                         if fullstate[otherag]['b'] == 0:
#                             self.assertEqual(bi(red[otherag]['b']), 0)
#                         else:
#                             self.assertEqual(bi(red[otherag]['b']), 1)
