import itertools
import os
import random
import sys
import unittest

import numpy as np

sys.path.insert(1, os.path.abspath(os.path.join(
    os.path.dirname(__file__), os.pardir)
))
from hrl.battery import *

class TestZeroFinding(unittest.TestCase):
    def assertSequenceAlmostEqual(self, first, second, places=7, delta=None):
        self.assertEqual(len(first), len(second))
        for i in range(len(first)):
            self.assertAlmostEqual(first[i], second[i], places=places, delta=delta)

    def setUp(self):
        random.seed(2664382435)
        np.random.seed(2664382435)

        self.quad_factory = lambda a,b,c: lambda x: a*x**2 + b*x + c
        self.quad = self.quad_factory(-1,0,4)

    def test_two_zeros(self):
        zeros = all_zeros_concave(self.quad, -10, 10)
        self.assertSequenceAlmostEqual(sorted(zeros), [-2, 2])

    def test_single_zeros(self):
        zeros_l = all_zeros_concave(self.quad, -10, 0)
        self.assertSequenceAlmostEqual(zeros_l, [-2])
        zeros_r = all_zeros_concave(self.quad, 0, 10)
        self.assertSequenceAlmostEqual(zeros_r, [2])

    def test_no_zeros(self):
        zeros = all_zeros_concave(self.quad, -1.5, 1.5)
        self.assertSequenceEqual(zeros, [])

    def test_random_quads(self):
        N = 10
        randa = [random.uniform(-10, -1e-3) for _ in range(N)]
        randb = [random.uniform(-10, 10) for _ in range(N)]
        randc = [random.uniform(-10, 10) for _ in range(N)]

        for a, b, c in itertools.product(randa, randb, randc):
            with self.subTest(a=a, b=b, c=c):
                quad = self.quad_factory(a,b,c)
                zeros = all_zeros_concave(quad, -10, 10)
                np_zeros = np.roots((a,b,c))
                np_zeros = np_zeros[np.isreal(np_zeros)]
                np_zeros = np_zeros[np_zeros >= -10]
                np_zeros = np_zeros[np_zeros <= 10]
                self.assertSequenceAlmostEqual(sorted(zeros), sorted(np_zeros))
