import copy
import os
import sys
import unittest

import numpy as np

sys.path.insert(1, os.path.abspath(os.path.join(
    os.path.dirname(__file__), os.pardir)
))
from hrl.basic import QuantizedSpace
from hrl.product import *
from hrl.utils import IMMUTABLE_TYPES
import abstr_tests as test_abstr


class TestRavelReplacement(unittest.TestCase):
    def test_unravel_same_as_numpy(self):
        xs = np.random.randint(0, 100000, 10000)
        shape = (10, 5, 4, 5, 100)
        for x in xs:
            with self.subTest(x=x):
                npur = np.unravel_index(x, shape)
                ur = unravel_index(x, shape)
                self.assertEqual(ur, npur)

    def test_unravel_big_int(self):
        x = 2**128 - 1
        shape = (2**32,) * 4
        ur = unravel_index(x, shape)
        self.assertGreater(x, sys.maxsize)
        self.assertEqual(ur, (2**32 - 1, ) * 4)

    def test_ravel_same_as_numpy(self):
        shape = (10, 5, 4, 5, 100)
        for _ in range(10000):
            mx = tuple(np.random.randint(0, s) for s in shape)
            with self.subTest(mx=mx):
                npr = np.ravel_multi_index(mx, shape)
                r = ravel_multi_index(mx, shape)
                self.assertEqual(r, npr)

    def test_ravel_big_int(self):
        shape = (2**32,) * 4
        mx = (2**32 - 1, ) * 4
        r = ravel_multi_index(mx, shape)
        self.assertGreater(r, sys.maxsize)
        self.assertEqual(r, 2**128 - 1)


class TestProductSpace(test_abstr.TestInvSeqInterface):
    def setUp(self):
        self.x_sp = QuantizedSpace(a=-10,b=-8,steps=5)
        self.y_sp = QuantizedSpace(a=-1,b=1,steps=6)
        self.z_sp = QuantizedSpace(a=7,b=9,steps=4)
        self.space = ProductSpace(x=self.x_sp, y=self.y_sp, z=self.z_sp)

    @property
    def invseq(self):
        return self.space

    def test_subspace_keys(self):
        keys = sorted(self.invseq.subkeys)
        self.assertSequenceEqual(keys, list(self.invseq.subkeys))
        self.assertSequenceEqual(keys, ['x', 'y', 'z'])

    def test_access_sub_spaces(self):
        self.assertIs(self.invseq.sub('x'), self.x_sp)
        self.assertIs(self.invseq.sub('y'), self.y_sp)
        self.assertIs(self.invseq.sub('z'), self.z_sp)

    def test_access_all_subspaces(self):
        subs = list(self.invseq.subspaces)
        self.assertIs(subs[0], self.x_sp)
        self.assertIs(subs[1], self.y_sp)
        self.assertIs(subs[2], self.z_sp)

    def test_shallow_copy_subspaces(self):
        cp = copy.copy(self.invseq)
        self.assertIs(self.invseq.spaces['x'], cp.spaces['x'])
        self.assertIs(self.invseq.spaces['y'], cp.spaces['y'])
        self.assertIs(self.invseq.spaces['z'], cp.spaces['z'])

    def test_deep_copy_subspaces(self):
        cp = copy.deepcopy(self.invseq)
        self.assertIsNot(self.invseq.spaces['x'], cp.spaces['x'])
        self.assertEqual(self.invseq.spaces['x'], cp.spaces['x'])
        self.assertIsNot(self.invseq.spaces['y'], cp.spaces['y'])
        self.assertEqual(self.invseq.spaces['y'], cp.spaces['y'])
        self.assertIsNot(self.invseq.spaces['z'], cp.spaces['z'])
        self.assertEqual(self.invseq.spaces['z'], cp.spaces['z'])

    def test_length(self):
        self.assertEqual(len(self.invseq),
                         len(self.x_sp) * len(self.y_sp) * len(self.z_sp))

    def test_shape_values(self):
        shape = self.invseq.shape
        self.assertEqual(len(shape), 3)
        self.assertEqual(shape[0]*shape[1]*shape[2], len(self.invseq))
        for i, s in enumerate(self.invseq.spaces.values()):
            self.assertEqual(shape[i], len(s))

    def test_make_item(self):
        s = self.invseq.make_item(x=-9, y=0, z=8)
        self.assertIn(s['x'], self.x_sp)
        self.assertTrue(self.x_sp.items_eq(s['x'], -9))
        self.assertIn(s['y'], self.y_sp)
        self.assertTrue(self.y_sp.items_eq(s['y'], 0))
        self.assertIn(s['z'], self.z_sp)
        self.assertTrue(self.z_sp.items_eq(s['z'], 8))

    def test_make_item_tuple(self):
        s = self.invseq.make_item(x=(-9,), y=(0,), z=(8,))
        self.assertIn(s['x'], self.x_sp)
        self.assertEqual(s['x'], -9)
        self.assertIn(s['y'], self.y_sp)
        self.assertEqual(s['y'], 0)
        self.assertIn(s['z'], self.z_sp)
        self.assertEqual(s['z'], 8)

    def test_make_item_existing(self):
        x = self.x_sp.make_item(-9)
        y = self.y_sp.make_item(0)
        z = self.z_sp.make_item(8)

        s = self.invseq.make_item(x=x, y=y, z=z)
        self.assertEqual(s['x'], x)
        self.assertEqual(s['y'], y)
        self.assertEqual(s['z'], z)

    def test_make_item_mixed(self):
        x = self.x_sp.make_item(-9)

        s = self.invseq.make_item(x=x, y=(0,), z=8)
        self.assertEqual(s['x'], x)
        self.assertEqual(s['y'], 0)
        self.assertIn(s['y'], self.y_sp)
        self.assertEqual(s['z'], 8)
        self.assertIn(s['z'], self.z_sp)

    def test_make_item_few_args(self):
        with self.assertRaises(ValueError):
            self.invseq.make_item(x=-9, y=0)

    def test_make_item_extra_args(self):
        with self.assertRaises(ValueError):
            self.invseq.make_item(x=-9, y=0, z=8, foo=42)

    def test_make_item_wrong_names(self):
        with self.assertRaises(ValueError):
            self.invseq.make_item(x=-9, y=0, foo=42)

    def test_multi_indexing(self):
        shape = self.invseq.shape
        items = []
        for i,j,k in itertools.product(*[range(sh) for sh in shape]):
            item = self.invseq[i,j,k]

            self.assertEqual(self.x_sp.index(item['x']), i)
            self.assertEqual(self.y_sp.index(item['y']), j)
            self.assertEqual(self.z_sp.index(item['z']), k)

            self.assertFalse(item in items)
            self.assertTrue(item in self.invseq)
            items.append(item)
        self.assertEqual(len(self.invseq), len(items))

    def test_multi_slice_indexing(self):
        items = self.invseq[0,:,0]
        self.assertEqual(items.shape, (1, len(self.y_sp), 1))
        for i in range(items.shape[1]):
            self.assertEqual(self.x_sp.index(items[0,i,0]['x']), 0)
            self.assertEqual(self.y_sp.index(items[0,i,0]['y']), i)
            self.assertEqual(self.z_sp.index(items[0,i,0]['z']), 0)

        items = self.invseq[0:2,0:2,0:2]
        self.assertEqual(items.shape, (2, 2, 2))
        for i,j,k in itertools.product(range(2), range(2), range(2)):
            self.assertEqual(self.x_sp.index(items[i,j,k]['x']), i)
            self.assertEqual(self.y_sp.index(items[i,j,k]['y']), j)
            self.assertEqual(self.z_sp.index(items[i,j,k]['z']), k)

    def test_multi_iterable_indexing(self):
        items = self.invseq[0,range(len(self.y_sp)),0]
        self.assertEqual(items.shape, (1, len(self.y_sp), 1))
        for i in range(items.shape[1]):
            self.assertEqual(self.x_sp.index(items[0,i,0]['x']), 0)
            self.assertEqual(self.y_sp.index(items[0,i,0]['y']), i)
            self.assertEqual(self.z_sp.index(items[0,i,0]['z']), 0)

        items = self.invseq[range(2),range(2),range(2)]
        self.assertEqual(items.shape, (2, 2, 2))
        for i,j,k in itertools.product(range(2), range(2), range(2)):
            self.assertEqual(self.x_sp.index(items[i,j,k]['x']), i)
            self.assertEqual(self.y_sp.index(items[i,j,k]['y']), j)
            self.assertEqual(self.z_sp.index(items[i,j,k]['z']), k)

        items = self.invseq[list(range(2)),list(range(2)),list(range(2))]
        self.assertEqual(items.shape, (2, 2, 2))
        for i,j,k in itertools.product(range(2), range(2), range(2)):
            self.assertEqual(self.x_sp.index(items[i,j,k]['x']), i)
            self.assertEqual(self.y_sp.index(items[i,j,k]['y']), j)
            self.assertEqual(self.z_sp.index(items[i,j,k]['z']), k)

    def test_subitems_shallow_copy(self):
        if self.invseq.sub('x').item_type in IMMUTABLE_TYPES:
            self.skipTest("Immutable types keep the same id")

        item = self.invseq.random()
        cp = copy.copy(item)
        self.assertIs(item['x'], cp['x'])
        self.assertIs(item['y'], cp['y'])
        self.assertIs(item['z'], cp['z'])

    def test_subitems_deep_copy(self):
        if self.invseq.sub('x').item_type in IMMUTABLE_TYPES:
            self.skipTest("Immutable types keep the same id")

        item = self.invseq.random()
        cp = copy.deepcopy(item)
        self.assertIsNot(item['x'], cp['x'])
        self.assertIsNot(item['y'], cp['y'])
        self.assertIsNot(item['z'], cp['z'])
        self.assertTrue(self.x_sp.items_eq(item['x'], cp['x']))
        self.assertTrue(self.y_sp.items_eq(item['y'], cp['y']))
        self.assertTrue(self.z_sp.items_eq(item['z'], cp['z']))

    def test_item_modification(self):
        item = self.invseq.make_item(x=-9, y=0, z=8)
        oldx = item['x']
        old_index = self.invseq.index(item)

        item['x'] = self.x_sp.make_item(-8.5)
        self.assertIsNot(item['x'], oldx)
        self.assertIn(item, self.invseq)

        item_index = self.invseq.index(item)
        self.assertNotEqual(old_index, item_index)
