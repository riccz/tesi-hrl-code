import unittest
import sys
import os


sys.path.insert(1, os.path.abspath(os.path.join(
    os.path.dirname(__file__), os.pardir)
))
from abstr_tests import TestInvSeqInterface
from hrl.basic import QuantizedSpace
from hrl.union import *

class TestUnionSpace(TestInvSeqInterface):
    def setUp(self):
        self.X = QuantizedSpace(a=-10, b=-5, steps=11)
        self.Y = QuantizedSpace(a=-4, b=4, steps=21)
        self.Z = QuantizedSpace(a=0, b=10, steps=51)

        self._unionspace = UnionSpace(X=self.X, Y=self.Y, Z=self.Z)

    @property
    def invseq(self):
        return self._unionspace

    def test_access_sub_spaces(self):
        self.assertIs(self.invseq.sub('X'), self.X)
        self.assertIs(self.invseq.sub('Y'), self.Y)
        self.assertIs(self.invseq.sub('Z'), self.Z)

    def test_length(self):
        self.assertEqual(len(self.invseq),
                         len(self.X) + len(self.Y) + len(self.Z))
