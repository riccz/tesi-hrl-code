from unittest import mock
import functools
import os
import random
import sys
import timeit
import unittest
import warnings

from scipy import sparse
import numpy as np

sys.path.insert(1, os.path.abspath(os.path.join(
    os.path.dirname(__file__), os.pardir)
))
from hrl import utils
from hrl.grid import GridMDP
from hrl.qlearn import *
from hrl.simple import SimpleMDP, SimpleMultiMDP

with warnings.catch_warnings():
    warnings.simplefilter("ignore", FutureWarning)
    import h5py


def all_qls(test_method):
    @functools.wraps(test_method)
    def test_wrapper(self):
        for ql, p in zip(self.qls, self.ql_params):
            with self.subTest(p=p):
                test_method(self, ql)
    return test_wrapper


class TestSingleQLearner(unittest.TestCase):
    def setUp(self):
        random.seed(291893077)
        np.random.seed(291893077)

        self.mdp = SimpleMDP()

        def make_eps_greedy(start, exp):
            return EpsilonGreedyPolicy(hyp_decay(start=start, exp=exp))

        def make_softmax(start, exp):
            return SoftmaxPolicy(hyp_decay(start=start, exp=exp))

        self.ql_params = [
            {'alpha': hyp_decay(start=0.99, exp=0.6, flat=3000),
             'explore_policy': make_eps_greedy(0.9, 0.1),
             'gamma': 1},
            {'alpha': hyp_decay(start=0.99, exp=0.6, flat=3000),
             'explore_policy': make_softmax(0.7, 0.1),
             'gamma': 1},
            {'alpha': hyp_decay(start=0.8, exp=0.6, flat=3000),
             'explore_policy': make_softmax(0.7, 0.1),
             'gamma': 1,
             'update_method': 'RUQL'},
            {'alpha': hyp_decay(start=0.9, exp=0.6, flat=3000),
             'explore_policy': make_softmax(0.7, 0.1),
             'gamma': 1,
             'update_method': 'FAQL',
             'faql_beta': hyp_decay(start=0.9, exp=0.6, flat=3000)},
        ]

        self.qls = [IndepQLearners(self.mdp, **p) for p in self.ql_params]

    @all_qls
    def test_make_policy(self, ql):
        ql.learn(500)
        p = ql.policy()
        Q = ql.Qs['ag0']
        S = self.mdp.state_space
        A = self.mdp.action_space.sub('ag0')
        for s in S:
            a_p = p(s)['ag0']
            a_ql = ql._greedy_action(ql.mdp, ql.Qs, s)['ag0']
            self.assertAlmostEqual(Q[S.index(s), A.index(a_p)],
                                   Q[S.index(s), A.index(a_ql)])

    @all_qls
    def test_optimal_policy(self, ql):
        def S(n):
            return self.mdp.state_space.make_item(ag0=n)

        for _ in range(2000):
            ql.learn(7)
            self.mdp.reset()
        p = ql.policy()

        self.assertEqual(p(S('A'))['ag0'], 'right')
        self.assertEqual(p(S('B'))['ag0'], 'right')
        self.assertEqual(p(S('C'))['ag0'], 'right')
        self.assertEqual(p(S('D'))['ag0'], 'stay')

    @all_qls
    def test_optimal_policy_deterministic_mdp(self, ql):
        def S(n):
            return self.mdp.state_space.make_item(ag0=n)

        self.mdp.epsilon = 0

        for _ in range(100):
            ql.learn(100)
            self.mdp.reset()
        p = ql.policy()

        self.assertEqual(p(S('A'))['ag0'], 'right')
        self.assertEqual(p(S('B'))['ag0'], 'right')
        self.assertEqual(p(S('C'))['ag0'], 'right')
        self.assertEqual(p(S('D'))['ag0'], 'stay')

    @all_qls
    def test_optimal_q_deterministic_mdp(self, ql):
        def Si(n):
            return self.mdp.state_space.index({'ag0': n})

        def Ai(n):
            return self.mdp.action_space.sub('ag0').index(n)

        self.mdp.epsilon = 0

        for _ in range(5000):
            # oldQ = self.ql.Qs['ag0'].copy()
            ql.learn(7)
            self.mdp.reset()
        Q = ql.Qs['ag0']

        self.assertAlmostEqual(Q[Si('A'), Ai('stay')], -4, places=1)
        self.assertAlmostEqual(Q[Si('A'), Ai('right')], -3, places=1)
        self.assertAlmostEqual(Q[Si('B'), Ai('left')], -4, places=1)
        self.assertAlmostEqual(Q[Si('B'), Ai('stay')], -3, places=1)
        self.assertAlmostEqual(Q[Si('B'), Ai('right')], -2, places=1)
        self.assertAlmostEqual(Q[Si('C'), Ai('left')], -3, places=1)
        self.assertAlmostEqual(Q[Si('C'), Ai('stay')], -2, places=1)
        self.assertAlmostEqual(Q[Si('C'), Ai('right')], -1, places=1)
        self.assertAlmostEqual(Q[Si('D'), Ai('stay')], 0, places=1)
        self.assertAlmostEqual(Q[Si('D'), Ai('left')], -2, places=1)

    @all_qls
    def test_one_update(self, ql):
        a = ql.greedy_action(self.mdp.state)
        self.assertIn(a['ag0'], self.mdp.valid_actions()['ag0'])
        (s, r, ns) = self.mdp.do_action(a)
        self.assertAlmostEqual(r['ag0'], -1)

        oldQ = ql.Qs['ag0'].copy() + ql.max_r_inf

        if self.mdp.max_reward <= 0:
            max_tot_r = 0
        else:
            self.assertGreaterEqual(ql.gamma, 0)
            self.assertLessEqual(ql.gamma, 1)
            max_tot_r = self.mdp.max_reward / (1 - ql.gamma)

        self.assertTrue(np.all(oldQ >= max_tot_r))

        ql.update(s=s, a=a, r=r, new_s=ns)
        self.assertEqual(ql.time, 1)

        Sidx = lambda s: self.mdp.state_space.index(s)
        Aidx = lambda a: self.mdp.action_space.sub('ag0').index(a)

        self.assertLessEqual(ql.Qs['ag0'][Sidx(s), Aidx(a['ag0'])],
                             oldQ[Sidx(s), Aidx(a['ag0'])])
        self.assertLessEqual(len(np.where(oldQ < ql.Qs['ag0'])[0]), 1)


class TestIndepQLearners(unittest.TestCase):
    def setUp(self):
        random.seed(4144366782)
        np.random.seed(4144366782)

        self.mdp = SimpleMultiMDP()
        alpha = hyp_decay(start=0.99, exp=0.51, flat=10000)
        expl = EpsilonGreedyPolicy(ConstCallable(0.1))
        self.iql = IndepQLearners(self.mdp, alpha=alpha, gamma=1,
                                  explore_policy=expl)

    def test_make_policy(self):
        self.iql.learn(100)
        p = self.iql.policy()

        Sidx = lambda s: self.mdp.state_space.index(s)
        A0idx = lambda a: self.mdp.action_space.sub('ag0').index(a)
        A1idx = lambda a: self.mdp.action_space.sub('ag1').index(a)
        for s in self.mdp.state_space:
            a_p = p(s)
            a0_ql = self.iql.greedy_action(s)['ag0']
            a1_ql = self.iql.greedy_action(s)['ag1']
            self.assertAlmostEqual(self.iql.Qs['ag0'][Sidx(s), A0idx(a_p['ag0'])],
                                   self.iql.Qs['ag0'][Sidx(s), A0idx(a0_ql)])
            self.assertAlmostEqual(self.iql.Qs['ag1'][Sidx(s), A1idx(a_p['ag1'])],
                                   self.iql.Qs['ag1'][Sidx(s), A1idx(a1_ql)])

    def test_optimal_policy_deterministic_mdp(self):
        for _ in range(3000):
            self.iql.learn(10)
            self.mdp.reset()
        p = self.iql.policy()

        S = self.mdp.state_space
        A = self.mdp.action_space

        self.assertEqual(p(S(ag0='A', ag1='C')), A(ag0='right', ag1='left'))
        self.assertEqual(p(S(ag0='A', ag1='D')), A(ag0='right', ag1='left'))
        self.assertEqual(p(S(ag0='A', ag1='E')), A(ag0='right', ag1='stay'))
        self.assertEqual(p(S(ag0='B', ag1='C')), A(ag0='right', ag1='left'))
        self.assertEqual(p(S(ag0='B', ag1='D')), A(ag0='right', ag1='left'))
        self.assertEqual(p(S(ag0='B', ag1='E')), A(ag0='right', ag1='stay'))
        self.assertEqual(p(S(ag0='E', ag1='C')), A(ag0='stay', ag1='left'))
        self.assertEqual(p(S(ag0='E', ag1='D')), A(ag0='stay', ag1='left'))
        self.assertEqual(p(S(ag0='E', ag1='E')), A(ag0='stay', ag1='stay'))

    def test_optimal_q_deterministic_mdp(self):
        for _ in range(6000):
            self.iql.learn(5)
            self.mdp.reset()
        p = self.iql.policy()

        S = self.mdp.state_space
        A = self.mdp.action_space
        Q0 = self.iql.Qs['ag0']
        Q1 = self.iql.Qs['ag1']
        Sidx = lambda **kwargs: S.index(S(**kwargs))
        A0idx = lambda a: A.sub('ag0').index(A.sub('ag0')(a))
        A1idx = lambda a: A.sub('ag1').index(A.sub('ag1')(a))

        self.assertAlmostEqual(Q0[Sidx(ag0='A', ag1='C'),
                                  A0idx('stay')], -3, places=1)
        self.assertAlmostEqual(Q0[Sidx(ag0='A', ag1='C'),
                                  A0idx('right')], -2, places=1)
        self.assertAlmostEqual(Q0[Sidx(ag0='A', ag1='D'),
                                  A0idx('stay')], -3, places=1)
        self.assertAlmostEqual(Q0[Sidx(ag0='A', ag1='D'),
                                  A0idx('right')], -2, places=1)
        self.assertAlmostEqual(Q0[Sidx(ag0='A', ag1='E'),
                                  A0idx('stay')], -3, places=1)
        self.assertAlmostEqual(Q0[Sidx(ag0='A', ag1='E'),
                                  A0idx('right')], -2, places=1)

        self.assertAlmostEqual(Q0[Sidx(ag0='B', ag1='C'),
                                  A0idx('left')], -3, places=1)
        self.assertAlmostEqual(Q0[Sidx(ag0='B', ag1='C'),
                                  A0idx('stay')], -2, places=1)
        self.assertAlmostEqual(Q0[Sidx(ag0='B', ag1='C'),
                                  A0idx('right')], -1, places=1)
        self.assertAlmostEqual(Q0[Sidx(ag0='B', ag1='D'),
                                  A0idx('left')], -3, places=1)
        self.assertAlmostEqual(Q0[Sidx(ag0='B', ag1='D'),
                                  A0idx('stay')], -2, places=1)
        self.assertAlmostEqual(Q0[Sidx(ag0='B', ag1='D'),
                                  A0idx('right')], -1, places=1)
        self.assertAlmostEqual(Q0[Sidx(ag0='B', ag1='E'),
                                  A0idx('left')], -3, places=1)
        self.assertAlmostEqual(Q0[Sidx(ag0='B', ag1='E'),
                                  A0idx('stay')], -2, places=1)
        self.assertAlmostEqual(Q0[Sidx(ag0='B', ag1='E'),
                                  A0idx('right')], -1, places=1)

        self.assertAlmostEqual(Q0[Sidx(ag0='E', ag1='C'),
                                  A0idx('stay')], 0, places=1)
        self.assertAlmostEqual(Q0[Sidx(ag0='E', ag1='D'),
                                  A0idx('stay')], 0, places=1)
        self.assertAlmostEqual(Q0[Sidx(ag0='E', ag1='E'),
                                  A0idx('stay')], 0, places=1)

        self.assertAlmostEqual(Q1[Sidx(ag0='A', ag1='D'),
                                  A1idx('stay')], -3, places=1)
        self.assertAlmostEqual(Q1[Sidx(ag0='A', ag1='D'),
                                  A1idx('left')], -2, places=1)
        self.assertAlmostEqual(Q1[Sidx(ag0='B', ag1='D'),
                                  A1idx('stay')], -3, places=1)
        self.assertAlmostEqual(Q1[Sidx(ag0='B', ag1='D'),
                                  A1idx('left')], -2, places=1)
        self.assertAlmostEqual(Q1[Sidx(ag0='E', ag1='D'),
                                  A1idx('stay')], -3, places=1)
        self.assertAlmostEqual(Q1[Sidx(ag0='E', ag1='D'),
                                  A1idx('left')], -2, places=1)

        self.assertAlmostEqual(Q1[Sidx(ag0='A', ag1='C'),
                                  A1idx('right')], -3, places=1)
        self.assertAlmostEqual(Q1[Sidx(ag0='A', ag1='C'),
                                  A1idx('stay')], -2, places=1)
        self.assertAlmostEqual(Q1[Sidx(ag0='A', ag1='C'),
                                  A1idx('left')], -1, places=1)
        self.assertAlmostEqual(Q1[Sidx(ag0='B', ag1='C'),
                                  A1idx('right')], -3, places=1)
        self.assertAlmostEqual(Q1[Sidx(ag0='B', ag1='C'),
                                  A1idx('stay')], -2, places=1)
        self.assertAlmostEqual(Q1[Sidx(ag0='B', ag1='C'),
                                  A1idx('left')], -1, places=1)
        self.assertAlmostEqual(Q1[Sidx(ag0='E', ag1='C'),
                                  A1idx('right')], -3, places=1)
        self.assertAlmostEqual(Q1[Sidx(ag0='E', ag1='C'),
                                  A1idx('stay')], -2, places=1)
        self.assertAlmostEqual(Q1[Sidx(ag0='E', ag1='C'),
                                  A1idx('left')], -1, places=1)

        self.assertAlmostEqual(Q1[Sidx(ag0='A', ag1='E'),
                                  A1idx('stay')], 0, places=1)
        self.assertAlmostEqual(Q1[Sidx(ag0='B', ag1='E'),
                                  A1idx('stay')], 0, places=1)
        self.assertAlmostEqual(Q1[Sidx(ag0='E', ag1='E'),
                                  A1idx('stay')], 0, places=1)

    def test_testing_method(self):
        self.iql.learn(50)
        mdp_time = self.mdp.time
        mdp_state = copy.deepcopy(self.mdp.state)
        ql_time = self.iql.time
        Qs = copy.deepcopy(self.iql.Qs)

        avg = self.iql.test(100)

        self.assertEqual(mdp_time, self.mdp.time)
        self.assertEqual(ql_time, self.iql.time)
        self.assertTrue(self.mdp.state_space.items_eq(mdp_state, self.mdp.state))
        for ag in self.mdp.agent_keys:
            self.assertTrue(np.all(Qs[ag] == self.iql.Qs[ag]))


class TestPacking(unittest.TestCase):
    def setUp(self):
        mdp = SimpleMultiMDP()
        alpha = hyp_decay(start=0.99, exp=0.6)
        expl = EpsilonGreedyPolicy(ConstCallable(0.1))
        self.ql = IndepQLearners(mdp, alpha=alpha,
                                 explore_policy=expl,
                                 gamma=1,
                                 keep_time_samples=True)
        self.ql.learn(100)

    @unittest.skip("Changed")
    def test_pack_unpack(self):
        with mock.patch('hrl.utils.aws.upload_file') as mock_upload:
            packed = self.ql.pack()
            self.assertEqual(mock_upload.call_count, 1)
            (args, kwargs) = mock_upload.call_args
            (fname, bucket, s3key) = args
            self.assertEqual(bucket, 'hrl.zanol.eu')
            self.assertEqual(s3key.split('/')[0], 'tmp')
            # self.assertEqual(packed, s3key)
        with mock.patch('hrl.utils.aws.aws_fetch_data') as mock_fetch:
            h5f = h5py.File(fname, 'r')
            mock_fetch.return_value = h5f
            unpacked = Learner.unpack(packed)
            self.assertEqual(mock_fetch.call_count, 1)
            fetch_s3key = mock_fetch.call_args[0][0]
            self.assertEqual(fetch_s3key, s3key)

        for ag in self.ql.mdp.agent_keys:
            self.assertTrue(np.all(self.ql.Qs[ag] == unpacked.Qs[ag]))
        self.assertEqual(unpacked.gamma, self.ql.gamma)
        self.assertEqual(unpacked.time, self.ql.time)
        self.assertListEqual(unpacked.train_time_counter.samples,
                             self.ql.train_time_counter.samples)
        self.assertListEqual(unpacked.test_time_counter.samples,
                             self.ql.test_time_counter.samples)


class TestSoftmax(unittest.TestCase):
    def setUp(self):
        self.bigtau = ConstCallable(1e300)
        self.smalltau = ConstCallable(1e-300)

    def test_no_underflow(self):
        smp = SoftmaxPolicy(self.smalltau)

        xs = np.random.random(100) * 100 - 50
        argsorted_xs = np.argsort(xs)

        probs = smp.choice_prob(xs, t=0)
        self.assertTrue(all(np.isfinite(p) for p in probs))
        self.assertTrue(all(p >= 0 for p in probs))
        self.assertTrue(all(p <= 1 for p in probs))
        self.assertAlmostEqual(sum(probs), 1)

        argsorted_probs = np.argsort(probs)
        self.assertEqual(argsorted_probs[-1], argsorted_xs[-1])

    def test_no_overflow(self):
        smp = SoftmaxPolicy(self.bigtau)

        xs = np.random.random(100) * 100 - 50

        probs = smp.choice_prob(xs, t=0)
        self.assertTrue(all(np.isfinite(p) for p in probs))
        self.assertTrue(all(p >= 0 for p in probs))
        self.assertTrue(all(p <= 1 for p in probs))
        self.assertAlmostEqual(sum(probs), 1)

        def almost_eq(a, b, places=7):
            return round(a - b, places) == 0

        self.assertTrue(all(almost_eq(p, 1/len(xs)) for p in probs))

    def test_xs_variance(self):
        smp = SoftmaxPolicy(ConstCallable(1))

        xs = np.random.random(100) * 1e300 - 0.5e300
        argsorted_xs = np.argsort(xs)

        probs = smp.choice_prob(xs, t=0)
        self.assertTrue(all(np.isfinite(p) for p in probs))
        self.assertTrue(all(p >= 0 for p in probs))
        self.assertTrue(all(p <= 1 for p in probs))
        self.assertAlmostEqual(sum(probs), 1)

        argsorted_probs = np.argsort(probs)
        self.assertEqual(argsorted_probs[-1], argsorted_xs[-1])


if __name__ == "__main__":
    unittest.main()
