from collections.abc import Mapping
import collections
import copy
import itertools
import operator
import os
import pickle
import random
import sys
import unittest

import numpy as np

sys.path.insert(1, os.path.abspath(os.path.join(
    os.path.dirname(__file__), os.pardir)
))
from hrl.abstr import *
from hrl.utils import IMMUTABLE_TYPES


class TestInvSeqInterface(unittest.TestCase):
    """Mix-in class to test the abstract InvSeq interface

    Add this as a superclass of a TestCase for a concrete
    implemetation of InvSeq

    """

    @property
    def invseq(self):
        self.skipTest("Abstract test class")

    def items_eq(self, lhs, rhs):
        return lhs == rhs

    def items_ne(self, lhs, rhs):
        return not self.items_eq(lhs, rhs)

    def assertItemSequenceEqual(self, seq1, seq2):
        i1 = iter(seq1)
        i2 = iter(seq2)
        self.assertTrue(all(self.items_eq(a, b) for a, b in zip(i1, i2)))
        self.assertRaises(StopIteration, next, i1)
        self.assertRaises(StopIteration, next, i2)

    def test_is_sequence(self):
        self.assertIsInstance(self.invseq, collections.abc.Sequence)

    def test_is_comparable(self):
        self.assertTrue(hasattr(self.invseq, '__eq__'))
        self.assertTrue(hasattr(self.invseq, '__ne__'))

        # Should be tested more specifically in subclasses
        self.assertTrue(self.invseq == self.invseq)
        self.assertFalse(self.invseq != self.invseq)

    def test_is_copyable(self):
        c = copy.copy(self.invseq)
        self.assertIsNot(c, self.invseq)
        self.assertEqual(c, self.invseq)

        # Should be tested more specifically in subclasses
        dc = copy.deepcopy(self.invseq)
        self.assertIsNot(dc, self.invseq)
        self.assertEqual(dc, self.invseq)

    def test_nested_copying(self):
        t = (self.invseq, self.invseq)
        t_cp = copy.copy(t)
        self.assertIs(t_cp[0], t_cp[1])
        self.assertIs(t_cp[0], self.invseq)
        self.assertIs(t_cp[1], self.invseq)

        t_dcp = copy.deepcopy(t)
        self.assertIs(t_dcp[0], t_dcp[1])
        self.assertIsNot(t_dcp[0], self.invseq)
        self.assertIsNot(t_dcp[1], self.invseq)

    def test_is_picklable(self):
        s = pickle.dumps(self.invseq)
        invseq2 = pickle.loads(s)
        self.assertIsNot(self.invseq, invseq2)
        self.assertEqual(self.invseq, invseq2)

    def test_nested_pickling(self):
        t = (self.invseq, self.invseq)
        s = pickle.dumps(t)
        t2 = pickle.loads(s)
        self.assertIsNot(t2[0], t[0])
        self.assertIsNot(t2[1], t[1])
        self.assertEqual(t2[0], t[0])
        self.assertEqual(t2[1], t[1])
        self.assertIs(t2[0], t2[1])

    def test_full_key_range(self):
        items = [self.invseq[k] for k in range(len(self.invseq))]

    def test_items_same_type(self):
        ItemType = self.invseq.item_type
        items = [self.invseq[k] for k in range(len(self.invseq))]
        self.assertTrue(all(type(i) is ItemType for i in items))

    def test_iteration_full_range(self):
        items = [i for i in self.invseq]
        items_manual = [self.invseq[k] for k in range(len(self.invseq))]
        self.assertItemSequenceEqual(items, items_manual)

    def test_overflow_key(self):
        self.invseq[len(self.invseq) - 1]
        with self.assertRaises(IndexError):
            self.invseq[len(self.invseq)]
        with self.assertRaises(IndexError):
            self.invseq[len(self.invseq)+1]

    def test_negative_key(self):
        self.assertTrue(self.items_eq(self.invseq[-1], self.invseq[len(self.invseq) - 1]))
        reverse = [self.invseq[i] for i in range(len(self.invseq)-1, -1, -1)]
        reverse.reverse()
        forward = [self.invseq[i] for i in range(len(self.invseq))]
        self.assertItemSequenceEqual(forward, reverse)

    def test_negative_overflow_key(self):
        self.invseq[-len(self.invseq)]
        with self.assertRaises(IndexError):
            self.invseq[-len(self.invseq)-1]

    def test_same_key_same_item(self):
        items1 = [self.invseq[k] for k in range(len(self.invseq))]
        items2 = [self.invseq[k] for k in range(len(self.invseq))]
        self.assertItemSequenceEqual(items1, items2)

    def test_unique_items(self):
        items = [i for i in self.invseq]

        all_nonsame_pairs = itertools.combinations(items, 2)
        is_pair_ne = itertools.starmap(self.items_ne, all_nonsame_pairs)
        self.assertTrue(all(is_pair_ne))

    def test_inversion(self):
        ks = range(len(self.invseq))
        items = [self.invseq[k] for k in ks]
        ks_rev = [self.invseq.index(i) for i in items]
        self.assertSequenceEqual(ks, ks_rev)

    def test_slice_indexing(self):
        items = [i for i in self.invseq]

        items1 = self.invseq[:]
        self.assertItemSequenceEqual(items1, items[:])

        items2 = self.invseq[3:]
        self.assertItemSequenceEqual(items2, items[3:])

        items3 = self.invseq[1:3]
        self.assertItemSequenceEqual(items3, items[1:3])

        items4 = self.invseq[1:-1]
        self.assertItemSequenceEqual(items4, items[1:-1])

        items5 = self.invseq[1:-1:3]
        self.assertItemSequenceEqual(items5, items[1:-1:3])

        items6 = self.invseq[-1:1:-3]
        self.assertItemSequenceEqual(items6, items[-1:1:-3])

    def test_list_indexing(self):
        keys = list(range(len(self.invseq))[0:13:2])
        random.shuffle(keys)

        items_manual = [self.invseq[k] for k in keys]
        items = self.invseq[keys]
        self.assertItemSequenceEqual(items, items_manual)

    def test_ndarray_indexing(self):
        keys = np.array(range(len(self.invseq))[0:13:2])
        random.shuffle(keys)

        items_manual = [self.invseq[k] for k in keys]
        items = self.invseq[keys]
        self.assertItemSequenceEqual(items, items_manual)

    # def test_islice_indexing(self):
    #     items = [i for i in self.invseq]

    #     items1 = self.invseq.islice()
    #     self.assertSequenceEqual(list(items1), items[:])

    #     items2 = self.invseq.islice(3)
    #     self.assertSequenceEqual(list(items2), items[3:])

    #     items3 = self.invseq.islice(1, 3)
    #     self.assertSequenceEqual(list(items3), items[1:3])

    #     items4 = self.invseq.islice(1, -1)
    #     self.assertSequenceEqual(list(items4), items[1:-1])

    #     items5 = self.invseq.islice(1, -1, 3)
    #     self.assertSequenceEqual(list(items5), items[1:-1:3])

    #     items6 = self.invseq.islice(-1, 1, -3)
    #     self.assertSequenceEqual(list(items6), items[-1:1:-3])

    def test_inverse_list_indexing(self):
        keys = list(range(len(self.invseq))[0:13:2])
        random.shuffle(keys)
        items = [self.invseq[k] for k in keys]

        keys_rev = self.invseq.index(items)
        self.assertSequenceEqual(keys, keys_rev)

    def test_k2i_only_single_keys(self):
        with self.assertRaises(TypeError):
            self.invseq.k2i([1, 2])
        with self.assertRaises(TypeError):
            self.invseq.k2i((1, 2))
        with self.assertRaises(TypeError):
            self.invseq.k2i(np.array([1, 2]))

    def test_k2i_out_of_range(self):
        with self.assertRaises(IndexError):
            self.invseq.k2i(len(self.invseq))
        with self.assertRaises(IndexError):
            self.invseq.k2i(-1)

    def test_i2k_only_single_items(self):
        with self.assertRaises(TypeError):
            self.invseq.i2k(list(self.invseq[:2]))
        with self.assertRaises(TypeError):
            self.invseq.i2k(tuple(self.invseq[:2]))
        with self.assertRaises(TypeError):
            self.invseq.i2k(np.array(self.invseq[:2]))

    def test_i2k_wrong_items(self):
        with self.assertRaises(TypeError):
            self.invseq.k2i(object())

    def test_count_items(self):
        items = self.invseq[:]
        for i in items:
            self.assertEqual(self.invseq.count(i), 1)
        self.assertEqual(self.invseq.count(object()), 0)

    def test_contains_items(self):
        items = self.invseq[:]
        for i in items:
            self.assertTrue(i in self.invseq)
        self.assertFalse(object() in self.invseq)

    def test_random_item(self):
        items = [self.invseq.random() for _ in range(30)]
        for i in items:
            self.assertTrue(i in self.invseq)

    def test_items_equality(self):
        items1 = self.invseq[:]
        items2 = self.invseq[:]
        for i1, i2 in itertools.zip_longest(items1, items2):
            self.assertTrue(self.invseq.items_eq(i1, i2))
            self.assertFalse(self.invseq.items_ne(i1, i2))

    def test_items_inequality(self):
        items1 = self.invseq[:]
        items2 = list(self.invseq[1:]) + [self.invseq[0]]
        for i1, i2 in itertools.zip_longest(items1, items2):
            self.assertFalse(self.invseq.items_eq(i1, i2))
            self.assertTrue(self.invseq.items_ne(i1, i2))

    def test_items_are_copyable(self):
        if self.invseq.item_type in IMMUTABLE_TYPES:
            self.skipTest("Immutable types keep the same id")

        item = self.invseq.random()
        c = copy.copy(item)
        self.assertIsNot(c, item)
        self.assertTrue(self.invseq.items_eq(item, c))
        self.assertTrue(self.invseq.items_eq(c, item))

        # Should be tested more specifically in subclasses
        dc = copy.deepcopy(item)
        self.assertIsNot(dc, item)
        self.assertTrue(self.invseq.items_eq(item, dc))
        self.assertTrue(self.invseq.items_eq(dc, item))

    def test_items_nested_copying(self):
        if self.invseq.item_type in IMMUTABLE_TYPES:
            self.skipTest("Immutable types keep the same id")

        item = self.invseq.random()
        t = (item, item)
        t_cp = copy.copy(t)
        self.assertIs(t_cp[0], t_cp[1])
        self.assertIs(t_cp[0], item)
        self.assertIs(t_cp[1], item)

        t_dcp = copy.deepcopy(t)
        self.assertIs(t_dcp[0], t_dcp[1])
        self.assertIsNot(t_dcp[0], item)
        self.assertIsNot(t_dcp[1], item)
        self.assertTrue(self.invseq.items_eq(t_dcp[0], t[0]))
        self.assertTrue(self.invseq.items_eq(t_dcp[1], t[1]))

    def test_items_are_picklable(self):
        item = self.invseq.random()
        s = pickle.dumps(item, protocol=4)
        item2 = pickle.loads(s)
        self.assertTrue(self.invseq.items_eq(item, item2))
        self.assertTrue(self.invseq.items_eq(item2, item))

    def test_items_nested_pickling(self):
        if self.invseq.item_type in IMMUTABLE_TYPES:
            self.skipTest("Immutable types keep the same id")

        item = self.invseq.random()
        t = (item, item)
        s = pickle.dumps(t, protocol=4)
        t2 = pickle.loads(s)
        self.assertIsNot(t2[0], t[0])
        self.assertIsNot(t2[1], t[1])
        self.assertIs(t2[0], t2[1])
        self.assertTrue(self.invseq.items_eq(t2[0], t[0]))
        self.assertTrue(self.invseq.items_eq(t2[1], t[1]))

    def test_packing(self):
        self.assertTrue(hasattr(self.invseq, 'pack'))
        self.assertTrue(hasattr(self.invseq, 'unpack'))

        item = self.invseq.random()
        packed = self.invseq.pack(item)
        self.assertTrue(isinstance(packed, np.ndarray))
        self.assertEqual(packed.ndim, 0)

        unpacked = self.invseq.unpack(packed)
        self.assertTrue(self.invseq.items_eq(unpacked, item))
        self.assertTrue(self.items_eq(unpacked, item))
