import copy
import os
import pickle
import sys
import unittest

import numpy as np

sys.path.insert(1, os.path.abspath(os.path.join(
    os.path.dirname(__file__), os.pardir)
))
from hrl.basic import *
from hrl.utils import sliding_window
import abstr_tests as test_abstr

class TestQuantizedSpace(test_abstr.TestInvSeqInterface):
    def setUp(self):
        self._invseq = QuantizedSpace(a=-1, b=1, steps=50)

    @property
    def invseq(self):
        return self._invseq

    def test_space_properties(self):
        self.assertEqual(self.invseq.a, -1)
        self.assertEqual(self.invseq.b, 1)
        self.assertEqual(self.invseq.steps, 50)
        interval = self.invseq.stepsize * self.invseq.steps
        self.assertEqual(interval, 2)

    def test_correct_length(self):
        self.assertEqual(self.invseq.steps, len(self.invseq))

    def test_sorted_items(self):
        items = [i for i in self.invseq]
        s_items = sorted(items)
        self.assertSequenceEqual(items, s_items)

    def test_min_max_items(self):
        items = [i for i in self.invseq]
        m = self.invseq.min
        M = self.invseq.max
        self.assertEqual(m, min(items))
        self.assertEqual(M, max(items))

    def test_partial_ordering(self):
        x1 = self.invseq.make_item(-0.5)
        x2 = self.invseq.make_item(0)
        x3 = self.invseq.make_item(0.5)
        x4 = self.invseq.make_item(0.51)

        eq = lambda lhs, rhs: self.invseq.items_eq(lhs, rhs)
        ne = lambda lhs, rhs: self.invseq.items_ne(lhs, rhs)
        lt = lambda lhs, rhs: self.invseq.items_lt(lhs, rhs)
        le = lambda lhs, rhs: self.invseq.items_le(lhs, rhs)
        gt = lambda lhs, rhs: self.invseq.items_gt(lhs, rhs)
        ge = lambda lhs, rhs: self.invseq.items_ge(lhs, rhs)

        self.assertTrue(eq(x3, x4))

        self.assertTrue(lt(x1, x2))
        self.assertTrue(lt(x2, x3))
        self.assertTrue(gt(x3, x2))
        self.assertTrue(gt(x2, x1))
        self.assertTrue(le(x3, x4))
        self.assertTrue(ge(x3, x4))
        self.assertTrue(ge(x4, x2))
        self.assertTrue(le(x1, x3))

        self.assertFalse(ge(x1, x2))
        self.assertFalse(gt(x1, x2))
        self.assertFalse(le(x3, x2))
        self.assertFalse(lt(x3, x2))
        self.assertFalse(lt(x3, x4))
        self.assertFalse(gt(x3, x4))

        other_invseq = copy.deepcopy(self.invseq)
        y1 = other_invseq.make_item(-0.5)
        y2 = other_invseq.make_item(0)
        y3 = other_invseq.make_item(0.5)

        self.assertTrue(eq(x1, y1))
        self.assertTrue(eq(x2, y2))
        self.assertTrue(eq(x3, y3))
        self.assertTrue(eq(x4, y3))

    def test_quantized_equality(self):
        zero = self.invseq.make_item(0.02)
        self.assertAlmostEqual(zero, self.invseq[self.invseq.index(zero)])

        xs = np.linspace(0.02, 0.02 + self.invseq.stepsize/2, 100)
        for x in xs:
            self.assertTrue(self.invseq.items_eq(zero, self.invseq.make_item(x)))

        notzero = self.invseq.make_item(0.02 + self.invseq.stepsize/2 * 1.25)
        self.assertTrue(self.invseq.items_ne(zero, notzero))

    def test_make_preserves_values(self):
        xs = np.linspace(self.invseq.a, self.invseq.b, 100)
        items = [self.invseq.make_item(x) for x in xs]
        for i, it in enumerate(items):
            self.assertEqual(xs[i], float(it))


class TestNonuniformQuantizedSpace(test_abstr.TestInvSeqInterface):
    def setUp(self):
        self.bins = np.logspace(0, 2, 50+1)[1:]
        self._invseq = QuantizedSpace(a=0, b=100, bins=self.bins)

    @property
    def invseq(self):
        return self._invseq

    def test_space_properties(self):
        self.assertEqual(self.invseq.a, 0)
        self.assertEqual(self.invseq.b, 100)
        self.assertEqual(self.invseq.steps, 50)
        intervals = sliding_window([self.invseq.a] + list(self.invseq.bins))
        intsum = sum(j - i for i,j in intervals)
        self.assertEqual(intsum, self.invseq.b - self.invseq.a)

    def test_make_item_whole_range(self):
        xs = np.linspace(self.invseq.a, self.invseq.b, 1024)
        items = [self.invseq.make_item(x) for x in xs]
        keys = [self.invseq.i2k(i) for i in items]
        uniq_keys = sorted(set(keys))
        self.assertSequenceEqual(uniq_keys, range(len(self.invseq)))

    def test_quantized_equality(self):
        ten = self.invseq.make_item(10.48)
        ten_index = self.invseq.index(ten)
        self.assertAlmostEqual(float(ten), float(self.invseq[ten_index]),
                               places=2)

        step_up = self.invseq.bins[ten_index] * 0.99
        step_down = self.invseq.bins[ten_index - 1] * 1.01

        xs = np.linspace(step_down, step_up, 100)
        for x in xs:
            ix = self.invseq.make_item(x)
            self.assertTrue(self.invseq.items_eq(ix, ten))

        not_ten = self.invseq.make_item(self.invseq.bins[ten_index] * 1.01)
        self.assertFalse(self.invseq.items_eq(not_ten, ten))

    def test_make_preserves_values(self):
        xs = np.linspace(self.invseq.a, self.invseq.b, 100)
        items = [self.invseq.make_item(x) for x in xs]
        for i, it in enumerate(items):
            self.assertEqual(xs[i], float(it))

class TestNamedSpace(test_abstr.TestInvSeqInterface):
    def setUp(self):
        self._invseq = NamedSpace('A', 'B', 'C', 'D')

    @property
    def invseq(self):
        return self._invseq

    def test_make_item(self):
        for n in ['A', 'B', 'C', 'D']:
            x = self.invseq.make_item(n)
            self.assertIn(x, self.invseq)

    def test_make_item_wrong(self):
        with self.assertRaises(ValueError):
            self.invseq.make_item('a')
