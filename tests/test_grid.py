import copy
import itertools
import os
import random
import sys
import unittest

from scipy.special import binom
import numpy as np

sys.path.insert(1, os.path.abspath(os.path.join(
    os.path.dirname(__file__), os.pardir)
))
from hrl import common
from hrl import utils
from hrl.basic import QuantizedSpace
from hrl.grid import *
from hrl.utils import flatten_dicts
import abstr_tests as test_abstr


class TestRewardTransitionProb(unittest.TestCase):
    def setUp(self):
        X = QuantizedSpace(a=-1, b=1, steps=3)
        Y = X
        self.Rspace_unif = GridRewardSpace(X, Y, pon=0.1, poff=0.3)
        self.x0i = X.index(0.0)
        self.y0i = Y.index(0.0)
        self.Rones = np.ones(self.Rspace_unif.state_shape, dtype=bool)
        self.Rones[self.x0i, self.y0i] = False
        self.Rones = self.Rspace_unif.make_item(self.Rones)

        self.Rspace_random = GridRewardSpace(X, Y,
                                             pon=np.random.random((3,3)),
                                             poff=np.random.random((3,3)))

    def test_all_ones(self):
        tr_probs = list(reward_transition_prob_on(self.Rones, self.Rspace_unif))
        self.assertEqual(len(tr_probs), 1)
        self.assertEqual(tr_probs[0][1], 1)
        self.assertEqual(tr_probs[0][0], self.Rspace_unif.index(self.Rones))

    def test_single_off(self):
        R = self.Rones.copy()
        R[0, 1] = False
        tr_probs = list(reward_transition_prob_on(R, self.Rspace_unif))
        self.assertEqual(len(tr_probs), 2)
        (R0i, p0) = tr_probs[0]
        (R1i, p1) = tr_probs[1]
        R0 = self.Rspace_unif[R0i]
        R1 = self.Rspace_unif[R1i]
        if R0[0, 1]:
            self.assertFalse(R1[0, 1])
            self.assertEqual(p0, 0.1)
            self.assertEqual(p1, 1 - 0.1)
        else:
            self.assertTrue(R1[0, 1])
            self.assertEqual(p0, 1 - 0.1)
            self.assertEqual(p1, 0.1)

    def test_many_off(self):
        R = self.Rones.copy()
        off_coords = [
            (0, 0),
            (0, 1),
            (0, 2),
            (1, 2)
        ]
        for xi, yi in off_coords:
            R[xi, yi] = False

        tr_probs = list(reward_transition_prob_on(R, self.Rspace_unif))
        self.assertEqual(len(tr_probs), 2**4)
        self.assertAlmostEqual(sum(p for _, p in tr_probs), 1)

        for newRi, p in tr_probs:
            newR = self.Rspace_unif[newRi]
            for xi, yi in itertools.product(range(3), range(3)):
                if (xi, yi) in off_coords:
                    continue
                self.assertEqual(R[xi, yi], newR[xi, yi])

            on_count = sum(newR[xi, yi] for xi, yi in off_coords)
            expected_p = ((0.1) ** on_count *
                          (1 - 0.1) ** (4 - on_count))
            self.assertAlmostEqual(p, expected_p)

    def test_nonuniform_pon(self):
        R = np.ones(self.Rspace_random.state_shape, dtype=bool)
        R[self.x0i, self.y0i] = False
        R = self.Rspace_random.make_item(R)
        off_coords = [
            (0, 0),
            (0, 1),
            (0, 2),
            (1, 2)
        ]
        for xi, yi in off_coords:
            R[xi, yi] = False

        tr_probs = list(reward_transition_prob_on(R, self.Rspace_random))
        self.assertEqual(len(tr_probs), 2**4)
        self.assertAlmostEqual(sum(p for _, p in tr_probs), 1)

        for newRi, p in tr_probs:
            newR = self.Rspace_unif[newRi]
            for xi, yi in itertools.product(range(3), range(3)):
                if (xi, yi) in off_coords:
                    continue
                self.assertEqual(R[xi, yi], newR[xi, yi])

            expected_p = 1
            for xi, yi in off_coords:
                if newR[xi, yi]:
                    expected_p *= self.Rspace_random.pon[xi, yi]
                else:
                    expected_p *= 1 - self.Rspace_random.pon[xi, yi]
            self.assertAlmostEqual(p, expected_p)

    def test_single_agent(self):
        agent_pos = ([0], [1])
        R = self.Rones.copy()

        tr_probs = list(reward_transition_prob_off(R, self.Rspace_unif, agent_pos))
        self.assertEqual(len(tr_probs), 2)
        self.assertAlmostEqual(sum(p for _, p in tr_probs), 1)

        (R0i, p0) = tr_probs[0]
        (R1i, p1) = tr_probs[1]
        R0 = self.Rspace_unif[R0i]
        R1 = self.Rspace_unif[R1i]
        if R0[0, 1]:
            self.assertFalse(R1[0, 1])
            self.assertEqual(p0, 1 - 0.3)
            self.assertEqual(p1, 0.3)
        else:
            self.assertTrue(R1[0, 1])
            self.assertEqual(p0, 0.3)
            self.assertEqual(p1, 1 - 0.3)

    def test_ignore_off(self):
        agent_pos = ([0, 1], [1, 0])
        R = self.Rones.copy()
        R[0, 1] = False

        tr_probs = list(reward_transition_prob_off(R, self.Rspace_unif, agent_pos))
        self.assertEqual(len(tr_probs), 2)
        self.assertAlmostEqual(sum(p for _, p in tr_probs), 1)

        (R0i, p0) = tr_probs[0]
        (R1i, p1) = tr_probs[1]
        R0 = self.Rspace_unif[R0i]
        R1 = self.Rspace_unif[R1i]
        self.assertFalse(R0[0, 1])
        self.assertFalse(R1[0, 1])
        if R0[1, 0]:
            self.assertFalse(R1[1, 0])
            self.assertEqual(p0, 1 - 0.3)
            self.assertEqual(p1, 0.3)
        else:
            self.assertTrue(R1[1, 0])
            self.assertEqual(p0, 0.3)
            self.assertEqual(p1, 1 - 0.3)

    def test_many_agents(self):
        R = self.Rones.copy()
        agents_coords = ([0, 0, 0, 1], [0, 1, 2, 2])
        ac_aspairs = list(zip(*agents_coords))

        tr_probs = list(reward_transition_prob_off(R, self.Rspace_unif, agents_coords))
        self.assertEqual(len(tr_probs), 2**4)
        self.assertAlmostEqual(sum(p for _, p in tr_probs), 1)

        for newRi, p in tr_probs:
            newR = self.Rspace_unif[newRi]
            for xi, yi in itertools.product(range(3), range(3)):
                if (xi, yi) in ac_aspairs:
                    continue
                self.assertEqual(R[xi, yi], newR[xi, yi])

            on_count = sum(newR[xi, yi] for xi, yi in ac_aspairs)
            expected_p = ((1 - 0.3) ** on_count *
                          (0.3) ** (4 - on_count))
            self.assertAlmostEqual(p, expected_p)

    def test_nonuniform_poff(self):
        R = np.ones(self.Rspace_random.state_shape, dtype=bool)
        R[self.x0i, self.y0i] = False
        R = self.Rspace_random.make_item(R)
        agents_coords = [
            (0, 0),
            (0, 1),
            (0, 2),
            (1, 2)
        ]
        agent_coords_split = ([x for x, _ in agents_coords],
                              [y for _, y in agents_coords])

        tr_probs = list(reward_transition_prob_off(R, self.Rspace_random,
                                                   agent_coords_split))
        self.assertEqual(len(tr_probs), 2**4)
        self.assertAlmostEqual(sum(p for _, p in tr_probs), 1)

        for newRi, p in tr_probs:
            newR = self.Rspace_unif[newRi]
            for xi, yi in itertools.product(range(3), range(3)):
                if (xi, yi) in agents_coords:
                    continue
                self.assertEqual(R[xi, yi], newR[xi, yi])

            expected_p = 1
            for xi, yi in agents_coords:
                if newR[xi, yi]:
                    expected_p *= 1 - self.Rspace_random.poff[xi, yi]
                else:
                    expected_p *= self.Rspace_random.poff[xi, yi]
            self.assertAlmostEqual(p, expected_p)

    def test_full_transitions(self):
        agent_pos = ([0, 1], [1, 0])
        R = self.Rones.copy()
        R[1, 2] = False
        R[2, 2] = False

        tr_probs = list(reward_transition_prob(R, self.Rspace_random, agent_pos))
        self.assertEqual(len(tr_probs), 2**4)
        self.assertAlmostEqual(sum(p for _, p in tr_probs), 1)

        for newRi, p in tr_probs:
            newR = self.Rspace_random[newRi]
            for xi, yi in itertools.product(range(3), range(3)):
                if (xi, yi) in list(zip(*agent_pos)):
                    continue
                if not R[xi, yi] and (xi != self.x0i or yi != self.y0i):
                    continue
                self.assertEqual(R[xi, yi], newR[xi, yi])

            expected_p_off = 1
            for xi, yi in zip(*agent_pos):
                if newR[xi, yi]:
                    expected_p_off *= 1 - self.Rspace_random.poff[xi, yi]
                else:
                    expected_p_off *= self.Rspace_random.poff[xi, yi]

            expected_p_on = 1
            for xi, yi in [(1, 2), (2, 2)]:
                if newR[xi, yi]:
                    expected_p_on *= self.Rspace_random.pon[xi, yi]
                else:
                    expected_p_on *= 1 - self.Rspace_random.pon[xi, yi]

            self.assertAlmostEqual(p, expected_p_on * expected_p_off)


class TestGridActionSpace(test_abstr.TestInvSeqInterface):
    def setUp(self):
        self.space = GridActionSpace()

    @property
    def invseq(self):
        return self.space


class TestGridActionSpaceMultiAgent(test_abstr.TestInvSeqInterface):
    def setUp(self):
        self.space = GridActionSpace(agents=4)

    @property
    def invseq(self):
        return self.space


class TestGridSpace(test_abstr.TestInvSeqInterface):
    def setUp(self):
        self.space = GridSpace(x_min=-100, x_max=100, x_steps=2,
                               y_min=-100, y_max=100, y_steps=2,
                               ret_steps=3,
                               b_steps=2, pon=0.1, poff=1/3)

    @property
    def invseq(self):
        return self.space

    def items_eq(self, lhs, rhs):
        ag_states_eq = all(lhs[ag] == rhs[ag]
                           for ag in self.invseq.agent_keys)
        rewards_eq = np.all(lhs['R'] == rhs['R'])
        return ag_states_eq and rewards_eq


class TestObservationSpace(test_abstr.TestInvSeqInterface):
    def setUp(self):
        grid = GridSpace(x_min=-100, x_max=100, x_steps=3,
                         y_min=-100, y_max=100, y_steps=3,
                         ret_steps=2,
                         b_steps=2, pon=0.1, poff=1/3, agents=4)
        self.space = GridObservationSpace(grid, agent='ag0',
                                          d_steps=3, theta_steps=4)

    @property
    def invseq(self):
        return self.space


class TestGridSpaceNonUniformB(test_abstr.TestInvSeqInterface):
    def setUp(self):
        self.b_steps = 3
        self.b_bins = ((np.geomspace(1, 10, self.b_steps+1) - 1) / 9)[1:]
        self.space = GridSpace(x_min=-100, x_max=100, x_steps=2,
                               y_min=-100, y_max=100, y_steps=2,
                               ret_steps=2,
                               pon=0.05, poff=0.3,
                               b_bins=self.b_bins)

    @property
    def invseq(self):
        return self.space

    def items_eq(self, lhs, rhs):
        ag_states_eq = all(lhs[ag] == rhs[ag]
                           for ag in self.invseq.agent_keys)
        rewards_eq = np.all(lhs['R'] == rhs['R'])
        return ag_states_eq and rewards_eq


class TestGridSpaceMultiAgent(test_abstr.TestInvSeqInterface):
    def setUp(self):
        self.b_steps = 1
        self.b_bins = ((np.geomspace(1, 10, self.b_steps+1) - 1) / 9)[1:]
        self.space = GridSpace(x_min=-100, x_max=100, x_steps=2,
                               y_min=-100, y_max=100, y_steps=2,
                               ret_steps=2,
                               b_bins=self.b_bins,
                               pon=0.05, poff=0.3,
                               agents=2)

    @property
    def invseq(self):
        return self.space

    def test_all_agents_same_space(self):
        ag0 = self.invseq.sub('ag0')
        for ag in self.invseq.agent_keys:
            self.assertIs(self.invseq.sub(ag), ag0)

    def items_eq(self, lhs, rhs):
        ag_states_eq = all(lhs[ag] == rhs[ag]
                           for ag in self.invseq.agent_keys)
        rewards_eq = np.all(lhs['R'] == rhs['R'])
        return ag_states_eq and rewards_eq


class TestGridMDP(unittest.TestCase):
    def setUp(self):
        random.seed(4031292763)
        np.random.seed(4031292763)

        self.mdp = GridMDP(agents=2,
                           grid_radius=1,
                           grid_steps=3,
                           b_steps=4,
                           d_steps=3,
                           theta_steps=4,
                           avg_steps_off=10,
                           avg_steps_on=3)
        self.S = self.mdp.state_space
        self.A = self.mdp.action_space

    def test_state_tracking(self):
        for t in range(300):
            a = self.mdp.random_action()
            prev_s = copy.deepcopy(self.mdp.state)
            (s, r, ns) = self.mdp.do_action(a)
            self.assertTrue(self.S.items_eq(s, prev_s))
            self.assertTrue(self.S.items_eq(ns, self.mdp.state))

    def test_starting_state_remains_same(self):
        s = self.mdp.starting_state
        sc = copy.deepcopy(self.mdp.starting_state)
        for _ in range(10):
            a = self.mdp.random_action()
            self.mdp.do_action(a)
        self.assertTrue(self.S.items_eq(self.mdp.starting_state, sc))
        self.assertIs(self.mdp.starting_state, s)

    def test_time_tracking(self):
        for t in range(100):
            a = self.mdp.random_action()
            self.assertEqual(self.mdp.time, t)
            self.mdp.do_action(a)
        self.assertEqual(self.mdp.time, 100)

        self.mdp.reset()
        self.assertEqual(self.mdp.time, 0)

    def test_valid_actions_output(self):
        for _ in range(10):
            actions = self.mdp.valid_actions()

            self.assertEqual(len(actions), self.mdp.agents)
            for ag in self.mdp.agent_keys:
                acts_ag = actions[ag]
                for a in acts_ag:
                    self.assertIn(a, self.A.spaces[ag])

            a = self.mdp.random_action()
            self.mdp.do_action(a)

    def test_valid_actions_at_start(self):
        actions = self.mdp.valid_actions(self.mdp.starting_state)
        seen_nonstay_count = 0
        seen_nonstay = set()
        for ag in self.mdp.agent_keys:
            acts_ag = actions[ag]
            A_ag = self.A.spaces[ag]

            self.assertIn(A_ag('stay'), acts_ag)

            acts_str = [str(a) for a in acts_ag if a != 'stay']
            seen_nonstay_count += len(acts_str)
            seen_nonstay.update(acts_str)
        self.assertEqual(len(seen_nonstay), seen_nonstay_count)
        self.assertEqual(len(seen_nonstay), 4)

    def test_random_walk_no_overlapping(self):
        for t in range(1000):
            a = self.mdp.random_action()
            (s, r, ns) = self.mdp.do_action(a)
            for ag in self.mdp.agent_keys:
                if 'grid' not in ns[ag]:
                    continue

                x = ns[ag]['grid']['x']
                y = ns[ag]['grid']['y']
                start_g = self.mdp.starting_state[ag]['grid']

                if (self.S.x.items_eq(x, start_g['x']) and
                    self.S.y.items_eq(y, start_g['y'])):
                    continue

                otherags = filter(lambda oag: oag != ag, self.mdp.agent_keys)
                for otherag in otherags:
                    if 'grid' not in ns[otherag]:
                        continue

                    otherx = ns[otherag]['grid']['x']
                    othery = ns[otherag]['grid']['y']

                    self.assertFalse(self.S.x.items_eq(x, otherx) and
                                     self.S.y.items_eq(y, othery))

    def test_stay_at_start(self):
        self.assertTrue(self.S.items_eq(self.mdp.state,
                                        self.mdp.starting_state))
        self.assertEqual(self.mdp.time, 0)
        stay = self.A.make_item(**{ag: 'stay' for ag in self.mdp.agent_keys})
        (s, r, ns) = self.mdp.do_action(stay)
        new_state_old_actions = copy.deepcopy(self.mdp.state)
        for ag in self.mdp.agent_keys:
            new_state_old_actions[ag]['grid']['prev_a'] = 'return'
        self.assertTrue(self.S.items_eq(new_state_old_actions,
                                        self.mdp.starting_state))
        self.assertTrue(self.S.items_eq(self.mdp.state, ns))
        self.assertTrue(self.S.items_eq(self.mdp.starting_state, s))
        self.assertDictEqual(r, {ag: 0 for ag in self.mdp.agent_keys})
        self.assertEqual(self.mdp.time, 1)

    def test_move_one_step(self):
        actions = {ag: 'stay' for ag in self.mdp.agent_keys}
        actions['ag0'] = 'down'
        a = self.A.make_item(**actions)
        (s, r, ns) = self.mdp.do_action(a)
        G = self.S.single.sub('grid')
        expected = G.make_item(x=0.0, y=-2/3, b=1.0, prev_a='down')
        self.assertTrue(self.S.single.items_eq(self.mdp.state['ag0'],
                                               {'grid': expected}))
        self.assertTrue(self.S.items_eq(self.mdp.state, ns))

        self.assertEqual(r['ag0'], 0)
        self.assertEqual(self.mdp.time, 1)

        actions['ag0'] = 'stay'
        next_a = self.A.make_item(**actions)
        next_r = self.mdp.reward(s=ns, a=a, new_s=ns)
        self.assertAlmostEqual(next_r['ag0'], 0)

    # def test_transition_prob_single(self):
    #     s = self.S.make_item(pos={'x':-1, 'y':0}, b=0.45)
    #     a = self.A.make_item('stay')
    #     ns = self.S.make_item(pos={'x':-1, 'y':0}, b=0.35)
    #     p = self.mdp.transition_prob(s=s, a=a, new_s=ns)
    #     self.assertEqual(p, 1)

    #     ns = self.S.make_item(pos={'x':-1, 'y':0}, b=0.45)
    #     p = self.mdp.transition_prob(s=s, a=a, new_s=ns)
    #     self.assertEqual(p, 0)

    # def test_transition_prob_vector(self):
    #     s = self.S.make_item(pos={'x':-1, 'y':0}, b=0.45)
    #     a = self.A.make_item('stay')
    #     ps = self.mdp.transition_prob(s=s, a=a)
    #     ns = self.S.make_item(pos={'x':-1, 'y':0}, b=0.35)
    #     self.assertEqual(len(ps), len(self.S))
    #     self.assertTrue(all(p == 0 for p in ps[:ns.index]))
    #     self.assertTrue(all(p == 0 for p in ps[ns.index+1:]))
    #     self.assertEqual(ps[ns.index], 1)

    # def test_transition_prob_invalid(self):
    #     with self.assertRaises(ValueError):
    #         self.mdp.transition_prob(s=self.S.make_item(pos={'x':-1, 'y':0}, b=0.45),
    #                                  a=self.A.make_item('left'),
    #                                  new_s=self.S.make_item(pos={'x':-1, 'y':0}, b=0.45))

    #     with self.assertRaises(ValueError):
    #         self.mdp.transition_prob(s=self.S.make_item(pos={'x':-1, 'y':0}, b=0.45),
    #                                  a=self.A.make_item('left'))

@unittest.skip("Implementation needs edits")
class TestGridMDPMultistep(unittest.TestCase):
    def setUp(self):
        # random.seed(4031292763)
        # np.random.seed(4031292763)

        self.mdp = GridMDP(agents=1,
                           grid_radius=1,
                           grid_steps=3,
                           b_steps=4,
                           reward_epsilon=1e-3,
                           reward_phi_up=1.05,
                           reward_phi_down=0.985,
                           reward_phi_down_slow=0.998,
                           reward_shift_level=0.4,
                           gamma=0.5,
                           stay_steps=6)
        self.S = self.mdp.state_space
        self.A = self.mdp.action_space

    def test_multistep_stay(self):
        self.mdp.do_action(self.A(ag0='up'))
        self.assertEqual(self.mdp.time, 1)
        before = copy.deepcopy(self.mdp.state)
        (s, r, ns) = self.mdp.do_action(self.A(ag0='stay'))

        self.assertEqual(self.mdp.time, 7)

        self.assertTrue(self.S.items_eq(before, s))

        self.assertTrue(self.S.x.items_eq(ns['ag0']['x'],
                                          s['ag0']['x']))
        self.assertTrue(self.S.y.items_eq(ns['ag0']['y'],
                                          s['ag0']['y']))

        b_delta = s['ag0']['b'] - ns['ag0']['b']
        self.assertAlmostEqual(b_delta, self.mdp.battery_usage('stay') * 6)

        exp_raw_rewards = 0.985 ** np.arange(6)
        exp_disc_reward = sum(0.5 ** k * r
                              for k, r in enumerate(exp_raw_rewards))
        self.assertAlmostEqual(r['ag0'], exp_disc_reward)

    def test_multistep_empty_battery(self):
        self.mdp.do_action(self.A(ag0='up'))
        # Fall just below the empty threshold in 3 steps
        self.mdp.state['ag0']['b'] = (0.25 +
                                      2.99 * self.mdp.battery_usage('stay'))
        (s, r, ns) = self.mdp.do_action(self.A(ag0='stay'))
        self.assertEqual(self.mdp.time, 4)
        self.assertTrue(self.S.b.items_eq(ns['ag0']['b'], 0))

        exp_raw_rewards = 0.985 ** np.arange(3)
        exp_disc_reward = sum(0.5 ** k * r
                              for k, r in enumerate(exp_raw_rewards))
        self.assertAlmostEqual(r['ag0'], exp_disc_reward)

class TestRewardFunction(unittest.TestCase):
    def setUp(self):
        # random.seed(4031292763)
        # np.random.seed(4031292763)

        self.mdp = GridMDP(agents=2,
                           grid_radius=3/2,
                           grid_steps=3,
                           b_steps=4,
                           d_steps=3,
                           theta_steps=4,
                           avg_steps_off=10,
                           avg_steps_on=3)

    def test_stay_over_off(self):
        S = self.mdp.state_space
        self.mdp.state['ag0']['grid']['x'] = 1.0
        S.R.set(self.mdp.state['R'], 1.0, 0.0, False)
        a = {'ag0': 'stay', 'ag1': 'stay'}
        reward = self.mdp.reward(self.mdp.state, a, self.mdp.state)
        self.assertDictEqual(reward, {'ag0': 0, 'ag1': 0})

    def test_stay_over_on(self):
        S = self.mdp.state_space
        self.mdp.state['ag0']['grid']['x'] = 1.0
        self.mdp.state['ag1']['grid']['y'] = 1.0
        a = {'ag0': 'stay', 'ag1': 'stay'}
        reward = self.mdp.reward(self.mdp.state, a, self.mdp.state)
        self.assertDictEqual(reward, {'ag0': 1, 'ag1': 1})

class TestIndepTransitionProbs(unittest.TestCase):
    def setUp(self):
        self.mdp = GridMDP(agents=4,
                           grid_radius=1,
                           grid_steps=3,
                           b_steps=4,
                           d_steps=3,
                           theta_steps=4,
                           avg_steps_off=10,
                           avg_steps_on=3)
        self.S = self.mdp.state_space
        self.A = self.mdp.action_space

        self.states = []
        self.actions = []
        for _ in range(100):
            s = self.mdp.state
            self.states.append(s)
            a = self.mdp.random_action(s)
            self.actions.append(a)
            self.mdp.do_action(a)

    def _output_shape_subtest(self, s, a, ag):
        probs = self.mdp.indep_transition_prob(ag, s, a[ag])

        self.assertTrue(hasattr(probs, '__iter__'))
        probs = list(probs)
        self.assertLessEqual(len(probs), 5**3 * 2)
        self.assertAlmostEqual(sum(p for _, p in probs), 1)
        self.assertTrue(all(s in self.mdp.state_space for s, _ in probs))
        self.assertTrue(all(p > 0 for _, p in probs))

    def test_output_shape(self):
        for s, a in zip(self.states, self.actions):
            for ag in self.mdp.agent_keys:
                with self.subTest(s=s, a=a, ag=ag):
                    self._output_shape_subtest(s, a, ag)

    def _one_R_change_subtest(self, s, a, ag):
        probs = list(self.mdp.indep_transition_prob(ag, s, a[ag]))
        if a == 'stay':
            xi = self.mdp.state_space.x.index(s[ag]['grid']['x'])
            yi = self.mdp.state_space.y.index(s[ag]['grid']['y'])
            oldR = s['R']
            for ns, _ in probs:
                newR = ns['R'].copy()
                newR[xi, yi] = oldR[xi, yi]
                self.assertTrue(np.all(oldR == newR))
        else:
            oldR = s['R']
            for ns, _ in probs:
                newR = ns['R'].copy()
                if 'grid' in ns[ag]:
                    xi = self.mdp.state_space.x.index(ns[ag]['grid']['x'])
                    yi = self.mdp.state_space.y.index(ns[ag]['grid']['y'])
                    newR[xi, yi] = oldR[xi, yi]
                    self.assertTrue(np.all(oldR == newR))
                else:
                    self.assertTrue(np.all(oldR == newR))

    def test_only_one_R_change(self):
        for s, a in zip(self.states, self.actions):
            for ag in self.mdp.agent_keys:
                with self.subTest(s=s, a=a, ag=ag):
                    self._one_R_change_subtest(s, a, ag)

    def _avg_indep_rew_subtest(self, s, a, ag):
        probs = list(self.mdp.indep_transition_prob(ag, s, a[ag]))
        nss = [ns for ns, _ in probs]
        ps = [p for _, p in probs]
        rs = [self.mdp.indep_reward(ag, s, a, ns) for ns in nss]
        avgr1 = np.vdot(rs, ps)
        avgr2 = self.mdp.average_indep_reward(ag, s, a)
        self.assertAlmostEqual(avgr1, avgr2)

    def test_average_indep_reward(self):
        if not hasattr(self.mdp, 'average_indep_reward'):
            self.skipTest("Method not implemented")
        for s, a in zip(self.states, self.actions):
            for ag in self.mdp.agent_keys:
                with self.subTest(s=s, a=a, ag=ag):
                    self._avg_indep_rew_subtest(s, a, ag)

    def test_random_walk_vs_probs(self):
        nsteps = 1000
        for _ in range(nsteps):
            a = self.mdp.random_action()
            probs = list(self.mdp.transition_prob(self.mdp.state, a))
            (s, r, ns) = self.mdp.do_action(a)
            newstates = [ns for ns, _ in probs]
            ps = [p for _, p in probs]
            eq_to_ns = functools.partial(self.mdp.state_space.items_eq, ns)
            whichnew = list(filter(eq_to_ns, newstates))
            self.assertEqual(len(whichnew), 1)


class TestBatteryTransProb(unittest.TestCase):
    def setUp(self):
        batt_bins = battery_bins(10, 0.016)
        self.mdp = GridMDP(agents=2,
                           grid_radius=300/2,
                           grid_steps=3,
                           b_bins=batt_bins,
                           d_steps=3,
                           theta_steps=4,
                           avg_steps_off=10,
                           avg_steps_on=3)

    def test_return_idx_prob_pairs(self):
        s_batt = self.mdp.state_space.b.make_item(1)
        probs = list(self.mdp._battery_trans_prob(
            s_batt, 'stay', prob_thresh=0))
        idxs = [i for i, _ in probs]
        ps = [p for _, p in probs]
        self.assertEqual(len(probs), self.mdp.state_space.b_steps)
        self.assertAlmostEqual(sum(ps), 1)
        self.assertSequenceEqual(
            sorted(idxs), range(self.mdp.state_space.b_steps))

    def test_monotone_probs(self):
        s_batt = self.mdp.state_space.b.make_item(1)
        probs = list(self.mdp._battery_trans_prob(
            s_batt, 'stay', prob_thresh=0))
        probs = sorted(probs, key=operator.itemgetter(0))
        ps = [p for _, p in probs]
        self.assertSequenceEqual(sorted(ps), ps)

    def test_lower_start(self):
        s_batt = self.mdp.state_space.b[3]
        probs = list(self.mdp._battery_trans_prob(
            s_batt, 'right', prob_thresh=0))
        ps = [p for _, p in probs]
        idxs = [i for i, _ in probs]
        self.assertEqual(len(probs), 4)
        self.assertAlmostEqual(sum(ps), 1)
        self.assertSequenceEqual(sorted(idxs), range(4))

    def test_zero_into_itself(self):
        s_batt = self.mdp.state_space.b[0]
        probs = list(self.mdp._battery_trans_prob(
            s_batt, 'left', prob_thresh=0))
        ps = [p for _, p in probs]
        idxs = [i for i, _ in probs]
        self.assertEqual(len(probs), 1)
        self.assertAlmostEqual(ps[0], 1)
        self.assertEqual(idxs[0], 0)

    def test_prob_thresholding(self):
        s_batts = self.mdp.state_space.b[:]
        for s_batt in s_batts:
            with self.subTest(s_batt=s_batt):
                probs = list(self.mdp._battery_trans_prob(s_batt, 'left',
                                                          prob_thresh=0))
                probs_thr = list(self.mdp._battery_trans_prob(s_batt, 'left',
                                                              prob_thresh=1e-2))
                for i, p in probs_thr:
                    self.assertIn((i, p), probs)
                    self.assertGreaterEqual(p, 1e-2)

    def test_continuous_start_value(self):
        s_batts = np.linspace(0, 1, 4096)
        for s_batt in s_batts:
            s_idx = self.mdp.state_space.b.index(s_batt)
            probs = list(self.mdp._battery_trans_prob(s_batt, 'left',
                                                      prob_thresh=0))
            ps = [p for _, p in probs]
            idxs = [i for i, _ in probs]
            self.assertEqual(len(probs), s_idx + 1)
            self.assertAlmostEqual(sum(ps), 1)
            self.assertSequenceEqual(sorted(idxs), range(s_idx + 1))

class TestJointBatteryTransProb(unittest.TestCase):
    def setUp(self):
        batt_bins = battery_bins(5, 0.016)
        self.mdp = GridMDP(agents=4,
                           grid_radius=300/2,
                           grid_steps=3,
                           b_bins=batt_bins,
                           d_steps=3,
                           theta_steps=4,
                           avg_steps_off=10,
                           avg_steps_on=3)

        self.mdp.state['ag0']['grid']['x'] = -100
        self.mdp.state['ag0']['grid']['y'] = 100
        self.mdp.state['ag1']['grid']['x'] = 100
        self.mdp.state['ag1']['grid']['y'] = 100
        self.mdp.state['ag2']['grid']['x'] = 100
        self.mdp.state['ag2']['grid']['y'] = -100
        self.mdp.state['ag3']['grid']['x'] = -100
        self.mdp.state['ag3']['grid']['y'] = -100

    def test_return_jb_prob_pairs(self):
        ja = {ag: 'stay' for ag in self.mdp.agent_keys}
        jps = list(self.mdp._joint_battery_trans_prob(
            self.mdp.state, ja, prob_thresh=0))
        self.assertEqual(len(jps), self.mdp.state_space.b_steps**4)
        jb_idxs = [i for i, _ in jps]
        ps = [p for _, p in jps]

        self.assertAlmostEqual(sum(ps), 1)
        for jb in jb_idxs:
            self.assertSequenceEqual(sorted(jb.keys()), self.mdp.agent_keys)
        for ag in self.mdp.agent_keys:
            oneagent = [jb[ag] for jb in jb_idxs]
            self.assertSequenceEqual(sorted(set(oneagent)),
                                     range(self.mdp.state_space.b_steps))

    def test_lower_start(self):
        B = self.mdp.state_space.b
        self.mdp.state['ag0']['grid']['b'] = B[0]
        self.mdp.state['ag1']['grid']['b'] = B[1]
        self.mdp.state['ag2']['grid']['b'] = B[2]
        self.mdp.state['ag3']['grid']['b'] = B[3]

        ja = {ag: 'stay' for ag in self.mdp.agent_keys}
        jps = list(self.mdp._joint_battery_trans_prob(
            self.mdp.state, ja, prob_thresh=0))
        self.assertEqual(len(jps), 4*3*2*1)
        jb_idxs = [i for i, _ in jps]
        ps = [p for _, p in jps]

        self.assertAlmostEqual(sum(ps), 1)
        for jb in jb_idxs:
            self.assertSequenceEqual(sorted(jb.keys()), self.mdp.agent_keys)

        oneagent0 = [jb['ag0'] for jb in jb_idxs]
        self.assertSequenceEqual(sorted(set(oneagent0)), range(1))
        oneagent1 = [jb['ag1'] for jb in jb_idxs]
        self.assertSequenceEqual(sorted(set(oneagent1)), range(2))
        oneagent2 = [jb['ag2'] for jb in jb_idxs]
        self.assertSequenceEqual(sorted(set(oneagent2)), range(3))
        oneagent3 = [jb['ag3'] for jb in jb_idxs]
        self.assertSequenceEqual(sorted(set(oneagent3)), range(4))

    def test_prob_thresholding(self):
        B = self.mdp.state_space.b
        for b0, b1, b2, b3 in itertools.product(B[:], repeat=4):
            self.mdp.state['ag0']['grid']['b'] = b0
            self.mdp.state['ag1']['grid']['b'] = b1
            self.mdp.state['ag2']['grid']['b'] = b2
            self.mdp.state['ag3']['grid']['b'] = b3

            ja = {ag: 'stay' for ag in self.mdp.agent_keys}
            jps = list(self.mdp._joint_battery_trans_prob(
                       self.mdp.state, ja, prob_thresh=0))
            jps_thr = list(self.mdp._joint_battery_trans_prob(
                           self.mdp.state, ja, prob_thresh=1e-2))
            for jb, p in jps_thr:
                    self.assertIn((jb, p), jps)
                    self.assertGreaterEqual(p, 1e-2)

    def test_starting_state(self):
        self.mdp.reset()
        ja = {'ag0': 'stay', 'ag1': 'left', 'ag2': 'right', 'ag3': 'stay'}
        self.mdp._check_valid_action(ja)
        jps = list(self.mdp._joint_battery_trans_prob(
                       self.mdp.state, ja, prob_thresh=0))
        self.assertEqual(len(jps), self.mdp.state_space.b_steps**2)
        jb_idxs = [i for i, _ in jps]
        ps = [p for _, p in jps]

        self.assertAlmostEqual(sum(ps), 1)
        for jb in jb_idxs:
            self.assertSequenceEqual(sorted(jb.keys()), self.mdp.agent_keys)

        oneagent0 = [jb['ag0'] for jb in jb_idxs]
        self.assertSequenceEqual(sorted(set(oneagent0)), [self.mdp.state_space.b_steps-1])
        oneagent3 = [jb['ag3'] for jb in jb_idxs]
        self.assertSequenceEqual(sorted(set(oneagent3)), [self.mdp.state_space.b_steps-1])
        oneagent1 = [jb['ag1'] for jb in jb_idxs]
        self.assertSequenceEqual(sorted(set(oneagent1)), range(self.mdp.state_space.b_steps))
        oneagent2 = [jb['ag2'] for jb in jb_idxs]
        self.assertSequenceEqual(sorted(set(oneagent2)), range(self.mdp.state_space.b_steps))

    def test_returning(self):
        self.assertGreater(self.mdp.state_space.ret_steps, 1)
        self.mdp.state['ag0'] = {'ret': 0}
        self.mdp.state['ag1'] = {'ret': 1}
        ja = {'ag0': 'return', 'ag1': 'return', 'ag2': 'up', 'ag3': 'up'}
        jps = list(self.mdp._joint_battery_trans_prob(
                       self.mdp.state, ja, prob_thresh=0))
        self.assertEqual(len(jps), self.mdp.state_space.b_steps**2)
        jb_idxs = [i for i, _ in jps]
        ps = [p for _, p in jps]

        self.assertAlmostEqual(sum(ps), 1)

        oneagent0 = [jb['ag0'] for jb in jb_idxs]
        self.assertSequenceEqual(sorted(set(oneagent0)), [self.mdp.state_space.b_steps - 1])
        oneagent1 = [jb['ag1'] for jb in jb_idxs]
        self.assertSequenceEqual(sorted(set(oneagent1)), [0])
        oneagent2 = [jb['ag2'] for jb in jb_idxs]
        self.assertSequenceEqual(sorted(set(oneagent2)), range(self.mdp.state_space.b_steps))
        oneagent3 = [jb['ag3'] for jb in jb_idxs]
        self.assertSequenceEqual(sorted(set(oneagent3)), range(self.mdp.state_space.b_steps))

    def test_approx(self):
        B = self.mdp.state_space.b
        for b0, b1, b2, b3 in itertools.product(B[:], repeat=4):
            for main_agent in self.mdp.agent_keys:
                self.mdp.state['ag0']['grid']['b'] = b0
                self.mdp.state['ag1']['grid']['b'] = b1
                self.mdp.state['ag2']['grid']['b'] = b2
                self.mdp.state['ag3']['grid']['b'] = b3

                ja = {ag: 'stay' for ag in self.mdp.agent_keys}
                jps = list(self.mdp._joint_battery_trans_prob(
                        self.mdp.state, ja, prob_thresh=0, approx=False))
                jps_approx = list(self.mdp._joint_battery_trans_prob(
                            self.mdp.state, ja, prob_thresh=0,
                            approx=True, main_agent=main_agent))
                self.assertAlmostEqual(sum(p for _, p in jps_approx), 1)
                jb_idxs = [jb for jb, _ in jps_approx]

                for ag in self.mdp.agent_keys:
                    oneagent = set(jb[ag] for jb in jb_idxs)
                    if ag != main_agent:
                        self.assertEqual(len(oneagent), 1)

class TestCommonGridBattery(unittest.TestCase):
    def setUp(self):
        self.grid = copy.deepcopy(common.grid)
        self.multigrid = copy.deepcopy(common.multigrid)

    def test_max_battery_trans_fanout(self):
        for mdp in [self.grid, self.multigrid]:
            morethan1 = 0
            for s_batt in np.linspace(0, 1, 4096):
                probs = list(mdp._battery_trans_prob(s_batt, 'left',
                                                     prob_thresh=1e-2))
                self.assertLessEqual(len(probs), 3)
                if len(probs) > 1:
                    morethan1 += 1
            self.assertLess(morethan1 / 4096, 0.2)


    def test_max_joint_battery_trans_fanout(self):
        for mdp in [self.grid, self.multigrid]:
            morethan1 = 0
            for s_batt in np.linspace(0, 1, 4096):
                mdp.state['ag0']['grid']['b'] = s_batt
                probs = list(mdp._joint_battery_trans_prob(
                             mdp.state,
                             {ag: 'stay' for ag in mdp.agent_keys},
                             prob_thresh=1e-2,
                             approx=True,
                             main_agent='ag0'))
                self.assertLessEqual(len(probs), 3)
                if len(probs) > 1:
                    morethan1 += 1
            self.assertLess(morethan1 / 4096, 0.2)


if __name__ == '__main__':
    unittest.main()
