import concurrent.futures as cf
import copy
import math
import warnings

import numpy as np

from hrl import utils
from hrl.grid import GridMDP
from hrl.qlearn import LookaheadQLearners

with warnings.catch_warnings():
    warnings.simplefilter("ignore", FutureWarning)
    import h5py

grid_stepsize = 30
grid_steps = 10
grid = GridMDP(agents=1,
               grid_radius=grid_stepsize*grid_steps/2, grid_steps=grid_steps,
               b_steps=10,
               reward_epsilon=1e-4,
               reward_phi_up=1+1e-4,
               reward_phi_down=0.9875,
               reward_phi_down_slow=0.998,
               reward_shift_level=0.4)

testcount = 32
testiters = 10000
itercount = 120000

alphas = np.empty(testcount)
gammas = np.empty(testcount)
is_softmax = np.empty(testcount, dtype=bool)
taus = np.empty(testcount)
taus[...] = math.nan
epsilons = np.empty(testcount)
epsilons[...] = math.nan
is_target = np.empty(testcount, dtype=bool)
lookahead_steps = np.empty(testcount, dtype=int)

def parallel_run(ql):
    def avg_reward_score(history):
        (avgs, _) = ql.default_score(history)
        return np.fromiter(avgs.values(), dtype=float).mean()

    ql.learn(itercount)
    return ql.test(testiters, score=avg_reward_score)

score_futures = np.empty(testcount, dtype=object)
with cf.ProcessPoolExecutor() as executor:
    for ep in range(testcount):
        grid_copy = copy.deepcopy(grid)
        alphas[ep] = np.random.uniform(0, 1)
        gammas[ep] = np.random.uniform(0, 1)
        is_softmax[ep] = np.random.random() >= 0.5
        if is_softmax[ep]:
            taus[ep] = np.random.uniform(0, 1e3)
            tau = taus[ep]
            epsilon = None
        else:
            epsilons[ep] = np.random.uniform(0, 1)
            epsilon = epsilons[ep]
            tau = None
        is_target[ep] = np.random.random() >= 0.5
        lookahead_steps[ep] = np.random.choice([1, 2])

        ql = LookaheadQLearners(grid_copy, alpha=alphas[ep], gamma=gammas[ep], epsilon=epsilon, tau=tau,
                                lookahead_target=is_target[ep], lookahead_steps=lookahead_steps[ep])
        score_futures[ep] = executor.submit(parallel_run, ql)
scores = np.array([f.result() for f in score_futures])

with h5py.File('random_single_lookahead.hdf5', 'w') as f:
    g = f.create_group('random_grid_data')
    g.attrs['params'] = utils.pack_objects(test_iterations=testiters,
                                           test_points=testcount,
                                           learn_iterations=itercount,
                                           grid=grid,
                                           git_sha1=utils.git_sha1())

    ds = g.create_dataset('alphas', (testcount,), dtype=float, compression='gzip')
    ds[:] = alphas
    ds = g.create_dataset('gammas', (testcount,), dtype=float, compression='gzip')
    ds[:] = gammas
    ds = g.create_dataset('is_softmax', (testcount,), dtype=bool, compression='gzip')
    ds[:] = is_softmax
    ds = g.create_dataset('taus', (testcount,), dtype=float, compression='gzip')
    ds[:] = taus
    ds = g.create_dataset('epsilons', (testcount,), dtype=float, compression='gzip')
    ds[:] = epsilons
    ds = g.create_dataset('scores', (testcount,), dtype=float, compression='gzip')
    ds[:] = scores
    ds = g.create_dataset('is_target', (testcount,), dtype=bool, compression='gzip')
    ds[:] = is_target
    ds = g.create_dataset('lookahead_steps', (testcount,), dtype=int, compression='gzip')
    ds[:] = lookahead_steps
key = utils.aws_upload_data('random_single_lookahead.hdf5', 'random_single_lookahead')
print("Uploaded with key {!s}".format(key))
