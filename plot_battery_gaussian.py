import math
import itertools

from scipy import optimize
import matplotlib.pyplot as plt
import numpy as np
import numpy.polynomial.polynomial as poly

from hrl.battery import TremblayBattery

### Battery parameters ###

Q = 5.5 # [Ah]
R = 4.75e-3
Efull = 16.2
Eexp = 14.15
A = Efull - Eexp
B = 3/4
Enom = 14
Qnom = 4.5
K = (Efull - Enom + A*(math.exp(-B * Qnom) - 1)) * (Q - Qnom) / Qnom
plot_i = 20*Q
E0 = Efull + K + R*plot_i - A
minV = 12

TB = TremblayBattery(E0=E0, R=R, K=K, A=A, B=B, Q=Q, minV=minV)

def random_power_generator(pmove, mu_h, std_h, mu_m, std_m):
    while True:
        if np.random.rand() > pmove:
            mu = mu_h
            std = std_h
        else:
            mu = mu_m
            std = std_m
        power = np.random.normal(mu, std)
        while power < 0:
            print("Negative power")
            power = np.random.normal(mu, std)
        yield power

mu_h = 338  # Goss hover loaded
std_h = 19.79e-3 * mu_h  # Scaled from Dietrich
mu_m = 848  # Goss 8 m/s loaded
std_m = 76.92e-3 * mu_m  # Scaled from Dietrich

plt.close()

### Constant power discharge plot ###

(ts, tstep) = np.linspace(0, 15*60, 4096, retstep=True)
TB.reset()
Vs = []
Is = []
for t in ts:
    try:
        (v, i) = TB.draw_power(mu_h, tstep)
    except ValueError:
        break
    Vs.append(v)
    Is.append(i)
# Pad to length of ts
Vs = np.pad(np.asarray(Vs, dtype=float),
    (0, len(ts) - len(Vs)),
    mode='constant', constant_values=math.nan)
Is = np.pad(np.asarray(Is, dtype=float),
    (0, len(ts) - len(Is)),
    mode='constant', constant_values=math.nan)

fig, ax1 = plt.subplots()
ax1.set_xlabel("Time [minutes]")
ax1.set_ylabel('I_out [A]', color='C0')
plt.plot(ts / 60, Is, color='C0')

ax2 = ax1.twinx()
ax2.set_ylabel('V_out [V]', color='C1')
plt.plot(ts / 60, Vs, color='C1')

fig.tight_layout()

# print("Power MSE = {:e}".format(np.nanmean(((Vs * Is) - mu_h)**2)))

### Gaussian power SOC plots with long T ###

T = 100
pmoves = [0.2, 0.5, 0.8]
repcount = 100000

allVs = []
allIs = []
allPs = []
allsocs = []
for pmove in pmoves:
    # allVs.append([])
    # allIs.append([])
    # allPs.append([])
    allsocs.append([])
    for rep in range(repcount):
        Vs = []
        Is = []
        Ps = []
        socs = []

        TB.reset()
        Pgen = random_power_generator(pmove, mu_h, std_h, mu_m, std_m)
        while True:
            P = next(Pgen)
            Ps.append(P)
            start_soc = TB.soc
            try:
                (v, i) = TB.draw_power(P, T)
            except ValueError:
                break
            Vs.append(v)
            Is.append(i)
            socs.append(start_soc)

        # print("Final SOC = {:f}".format(TB.soc))
        # allVs[-1].append(Vs)
        # allIs[-1].append(Is)
        # allPs[-1].append(Ps)
        allsocs[-1].append(socs)

maxlength = max(max(len(socs) for socs in socss) for socss in allsocs)
for i in range(len(allsocs)):
    for j in range(len(allsocs[i])):
        socs = np.asarray(allsocs[i][j], dtype=float)
        socs = np.pad(socs, (0, maxlength - len(socs)),
            mode='constant', constant_values=0)
        allsocs[i][j] = socs

allsocs = np.asarray(allsocs, dtype=float)

for i, pmove in enumerate(pmoves):
    plt.figure()
    plt.xlabel("Time [minutes]")
    plt.ylabel("SOC")

    avg_socs = np.mean(allsocs[i], axis=0)
    std_socs = np.std(allsocs[i], axis=0)
    ts = np.arange(len(socs)) * T
    (line,) = plt.plot(ts / 60, avg_socs, label="pmove = {:.1f}".format(pmove),
        color="C{:d}".format(i))
    plt.plot(ts / 60, avg_socs - 1.96 * std_socs, color=line.get_color(), linestyle='dotted')
    plt.plot(ts / 60, avg_socs + 1.96 * std_socs, color=line.get_color(), linestyle='dotted')
    plt.legend()
    plt.ylim(-0.2, 1)
    plt.xlim(0, 14)
    plt.grid()
    plt.savefig("plots/battery_gaussian_Tlong_{:.1f}.pdf".format(pmove), format='pdf')

### Gaussian power SOC plot with short T

T = 10
pmoves = [0.2, 0.5, 0.8]
repcount = 100000

allVs = []
allIs = []
allPs = []
allsocs = []
for pmove in pmoves:
    # allVs.append([])
    # allIs.append([])
    # allPs.append([])
    allsocs.append([])
    for rep in range(repcount):
        Vs = []
        Is = []
        Ps = []
        socs = []

        TB.reset()
        Pgen = random_power_generator(pmove, mu_h, std_h, mu_m, std_m)
        while True:
            P = next(Pgen)
            Ps.append(P)
            start_soc = TB.soc
            try:
                (v, i) = TB.draw_power(P, T)
            except ValueError:
                break
            Vs.append(v)
            Is.append(i)
            socs.append(start_soc)

        # print("Final SOC = {:f}".format(TB.soc))
        # allVs[-1].append(Vs)
        # allIs[-1].append(Is)
        # allPs[-1].append(Ps)
        allsocs[-1].append(socs)

maxlength = max(max(len(socs) for socs in socss) for socss in allsocs)
for i in range(len(allsocs)):
    for j in range(len(allsocs[i])):
        socs = np.asarray(allsocs[i][j], dtype=float)
        socs = np.pad(socs, (0, maxlength - len(socs)),
            mode='constant', constant_values=0)
        allsocs[i][j] = socs

allsocs = np.asarray(allsocs, dtype=float)

plt.figure()
plt.xlabel("Time [minutes]")
plt.ylabel("SOC")
for i, pmove in enumerate(pmoves):
    avg_socs = np.mean(allsocs[i], axis=0)
    std_socs = np.std(allsocs[i], axis=0)
    ts = np.arange(len(socs)) * T
    (line,) = plt.plot(ts / 60, avg_socs, label="pmove = {:.1f}".format(pmove),
        color="C{:d}".format(i))
    plt.plot(ts / 60, avg_socs - 1.96 * std_socs, color=line.get_color(), linestyle='dotted')
    plt.plot(ts / 60, avg_socs + 1.96 * std_socs, color=line.get_color(), linestyle='dotted')
plt.legend()
plt.ylim(-0.2, 1)
plt.xlim(0, 14)
plt.grid()
plt.savefig("plots/battery_gaussian_Tshort.pdf", format='pdf')

plt.show()
