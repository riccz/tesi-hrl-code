import math
import itertools

from scipy import optimize
import matplotlib.pyplot as plt
import numpy as np

from hrl.battery import TremblayBattery, tremblay_v_out

Q = 0.5 # [Ah]
R = 17e-3
Efull = 4.1
Eexp = 3.658
A = Efull - Eexp
B = 3/291.7e-3
Enom = 3.545
Qnom = 453e-3
K = (Efull - Enom + A*(math.exp(-B * Qnom) - 1)) * (Q - Qnom) / Qnom
plot_i = 10*Q
E0 = Efull + K + R*plot_i - A
minV = 3

TB = TremblayBattery(E0=E0, R=R, K=K, A=A, B=B, Q=Q, minV=minV)

fig = plt.figure()
origax = fig.add_subplot(1,1,1)
plotax = fig.add_axes(origax.get_position(), frameon=False)

orig_plot = plt.imread("batt_disch.png")
origax.imshow(orig_plot)
origax.tick_params(which='both', bottom='off', top='off', left='off', right='off',
                   labelbottom='off', labeltop='off',
                   labelleft='off', labelright='off')

plotax.set_xlim(0, 530)
plotax.set_ylim(2.8, 4.4)
plotax.grid()
plotax.set_xticks(np.arange(0, 530+1, 53))
plotax.set_yticks(np.arange(2.8, 4.4+0.1, 0.2))
# plotax.minorticks_on()

plotax.set_prop_cycle(linestyle=['dashed']*4,
                      color=['black', 'red', 'blue', 'teal'])

Is = np.array([1, 10, 20, 25]) * Q
Qs = np.linspace(0, 530e-3, 4096)
all_Vs = np.empty((len(Is), len(Qs)))
for i, I in enumerate(Is):
    TB.reset()
    Vs = []
    last_q = 0
    for q in Qs[1:]:
        Vs.append(TB.output_voltage(I))
        qdiff = q - last_q
        try:
            TB.draw_charge(qdiff)
        except ValueError:
            pass
        last_q = q
    Vs.append(TB.output_voltage(I))
    all_Vs[i,:] = Vs
    plotax.plot(Qs * 1000, Vs, label="{:.1f}C".format(I/Q))

plotax.legend()

# plt.xlim(0, 1.1*Q)
# plt.ylim(minV * 0.95, Efull * 1.05)
# plt.grid()

plotax.set_xlabel('Used charge [mAh]')
plotax.set_ylabel('V_out [V]')

plt.show()
