import pickle
import copy

from matplotlib import animation
import matplotlib.pyplot as plt
import numpy as np

from hrl.utils import load_vars

plt.close()

data = load_vars('single_agent.pickle.xz')
hist = data['hist']
lahist = data['lahist']
rfunc = data['rfunc']

mdp = rfunc.mdp

policy = data['lapolicy']
pol_path = []
mdp.reset()
for _ in range(1000):
    s = mdp.state
    pol_path.append(copy.deepcopy(s))
    a = policy(s)
    mdp.do_action(a)

rfunc.reset()


path = [h[0] for h in lahist]
#nss = [h[3] for h in hist]
xs = np.fromiter((s.pos.x for s in path), float)
ys = np.fromiter((s.pos.y for s in path), float)

all_xs = np.fromiter(mdp.state_space.pos.x, float)
all_ys = np.fromiter(mdp.state_space.pos.y, float)

(plot_x, plot_y) = np.meshgrid(all_xs, all_ys)

# plt.figure()
# for ns in nss[:100]:
#     rfunc.update(ns)
# plt.contourf(plot_x, plot_y, rfunc._reward_matrix, 100)




pathfig = plt.figure()
pathax = plt.axes()
#(pathline,) = pathax.plot([], [], 'r', alpha=0.33, zorder=2)
#pathpoints = pathax.scatter([], [], c='r', zorder=3)

# def pathfig_init():
#     pathline.set_data([], [])
#     pathpoints.set_offsets(np.array([[],[]]))
#     #pathpoints._sizes = np.array([])
#     #pathpoints.set_array(np.array([]))
#     rfunc.reset()
#     contour = pathax.contourf(plot_x, plot_y, rfunc._reward_matrix.transpose(), 32, zorder=1, alpha=0.5)
#     return (pathline, pathpoints, contour)

def pathfig_anim(i):
    pathax.clear()
    pathax.set_xlim(-1, 1)
    pathax.set_ylim(-1, 1)

    (pathline,) = pathax.plot(xs[:i+1], ys[:i+1], 'r', alpha=0.33, zorder=2)
    pathpoints = pathax.scatter(xs[i], ys[i], c='r', zorder=3)

    # pathline.set_data(xs[:i], ys[:i])
    # pathpoints.set_offsets(np.array([xs[i-1], ys[i-1]]).transpose())
    #pathpoints._sizes = np.array([4])
    #pathpoints.set_array(np.array('r'))
    rfunc.update(path[i])
    contour = pathax.contourf(plot_x, plot_y, rfunc._reward_matrix.transpose(), 128, zorder=1)
    return (pathline, pathpoints, contour)
pathanim = animation.FuncAnimation(pathfig, pathfig_anim,
                                   frames=len(path), interval=250)


plt.show()
