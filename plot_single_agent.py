import argparse
import copy
import itertools
import operator
import pathlib
import warnings

from matplotlib import animation
from matplotlib import pyplot as plt
import numpy as np

from hrl import utils
from hrl.common import plot_opts_parser, algo_color, algo_order

with warnings.catch_warnings():
    warnings.simplefilter("ignore", FutureWarning)
    import h5py


def plot_avg_reward(key, label, rw_key=None, la_key=None, axes=None,
                    la_label='LA'):
    if axes is None:
        (fig, ax) = plt.subplots()
    else:
        ax = axes
        fig = axes.get_figure()

    with utils.aws.aws_fetch_data(key) as f:
        params = utils.unpack_objects(f['plot_data'].attrs['params'])
        train_iters = params['training_iterations']
        testpoints = params['test_points']

        train_steps = np.arange(1, testpoints + 1) * train_iters / 1e6
        average_rewards = np.asarray(f['plot_data']['avg_rewards'])
        plot_avg = np.mean(average_rewards, axis=0)
        plot_std = np.std(average_rewards, axis=0)
        print("{!s} final avg.rew. = {:f},"
              " std.dev. = {:f}".format(label, plot_avg[-1], plot_std[-1]))
        (line,) = ax.plot(train_steps, plot_avg, label=label,
                           color=algo_color[label])
        ax.plot(train_steps, plot_avg + 1.96 * plot_std,
                 linestyle='dotted', color=line.get_color())
        ax.plot(train_steps, plot_avg - 1.96 * plot_std,
                 linestyle='dotted', color=line.get_color())

    if rw_key:
        with utils.aws.aws_fetch_data(rw_key) as f_rw:
            rw_average_rewards = np.asarray(f_rw['plot_data']['avg_rewards'])

            rwavg = rw_average_rewards.mean()
            rwstd = rw_average_rewards.std()
            print("RW final avg.rew. = {:f},"
                  " std.dev. = {:f}".format(rwavg, rwstd))
            rwline = plt.axhline(rwavg, linestyle='solid',
                                 label='RW', color=algo_color['RW'])
            ax.axhline(rwavg + 1.96 * rwstd,
                        linestyle='dotted', color=rwline.get_color())
            ax.axhline(rwavg - 1.96 * rwstd,
                        linestyle='dotted', color=rwline.get_color())

    if la_key:
        with utils.aws.aws_fetch_data(la_key) as f_la:
            la_average_rewards = np.asarray(f_la['plot_data']['avg_rewards'])

            laavg = la_average_rewards.mean()
            lastd = la_average_rewards.std()
            print("{!s} final avg.rew. = {:f},"
                  " std.dev. = {:f}".format(la_label, laavg, lastd))
            laline = plt.axhline(laavg, linestyle='solid',
                                 label=la_label, color=algo_color[la_label])
            ax.axhline(laavg + 1.96 * lastd,
                        linestyle='dotted', color=laline.get_color())
            ax.axhline(laavg - 1.96 * lastd,
                        linestyle='dotted', color=laline.get_color())

    ax.grid(True)
    # plt.legend(loc='lower right')
    ax.legend()
    ax.set_xlabel('Training steps $[\cdot 10^6]$')
    ax.set_ylabel('Normalized cumulative test reward')
    ax.set_ylim(0.1, 0.65)

    return fig


def plot_test_reward(**keys):
    fig = plt.figure()
    sorted_kwargs = sorted(keys.items(), key=lambda kv: algo_order[kv[0]])
    for label, key in sorted_kwargs:
        with utils.aws.aws_fetch_data(key) as f:
            params = utils.unpack_objects(f['plot_data'].attrs['params'])
            grid = params['grid']
            test_iters = params['test_iterations']

            hist_label = 'hist_last' if 'hist_last' in f else 'hist'

            test_steps = np.arange(1, test_iters + 1)
            rewards = np.empty((grid.agents, test_iters))
            for i, ag in enumerate(grid.agent_keys):
                rewards[i, :] = f[hist_label]['rewards'][ag]

            avg_reward = np.mean(rewards, axis=0)
            timeavg_reward = np.fromiter(utils.iter.avg_over_time(avg_reward),
                                         dtype=float)
            plt.plot(test_steps, timeavg_reward, label=label,
                     color=algo_color[label])

    plt.grid()
    plt.legend()
    plt.xlabel('Test steps')
    plt.ylabel('Normalized cumulative test reward')
    plt.ylim(0.1, 0.65)

    return fig


def avg_train_time(key):
    with utils.aws.aws_fetch_data(key) as f:
        avg_train_times = np.asarray(f['plot_data']['avg_train_times'])
        std_train_times = np.asarray(f['plot_data']['std_train_times'])

        global_avg_train_time = np.mean(avg_train_times)
        pow_train_times = std_train_times ** 2 + avg_train_times ** 2
        global_std_train_time = np.sqrt(np.mean(pow_train_times) -
                                        global_avg_train_time ** 2)

        return (global_avg_train_time, global_std_train_time)


def avg_test_time(key):
    with utils.aws.aws_fetch_data(key) as f:
        avg_test_times = np.asarray(f['plot_data']['avg_test_times'])
        std_test_times = np.asarray(f['plot_data']['std_test_times'])

        global_avg_test_time = np.mean(avg_test_times)
        pow_test_times = std_test_times ** 2 + avg_test_times ** 2
        global_std_test_time = np.sqrt(np.mean(pow_test_times) -
                                       global_avg_test_time ** 2)

        return (global_avg_test_time, global_std_test_time)

if __name__ == '__main__':
    plt.close()

    # Final data
    faql_key = 'data/single_agent/48824467/90hrs7orUo3JBbwC'
    la_key = 'data/single_lookahead/b6c8f04c/wvUAofeulItjiDjy'  # 3 steps
    ql_key = 'data/single_agent/23efa7d2/sRtlEFYVozLW5JEE'
    ruql_key = 'data/single_agent/23efa7d2/WJNGDSEXLK7xR86B'
    rw_key = 'data/random_walk/b6c8f04c/eWvG1UzlMlUwyzgy'

    # Different key for FAQL test
    faql_test_key = 'data/single_agent/aadb59d3/T8ten2nmCOKF0lAu'

    outdir = pathlib.Path('plots/single')
    if not outdir.exists():
        outdir.mkdir(parents=True)

    plot_avg_reward(ruql_key, 'RUQL', rw_key=rw_key, la_key=la_key)
    plt.savefig(str(outdir / 'avg_reward_ruql.pdf'), format='pdf')
    plot_avg_reward(ql_key, 'QL', rw_key=rw_key, la_key=la_key)
    plt.savefig(str(outdir / 'avg_reward_ql.pdf'), format='pdf')
    plot_avg_reward(faql_key, 'FAQL', rw_key=rw_key, la_key=la_key)
    plt.savefig(str(outdir / 'avg_reward_faql.pdf'), format='pdf')

    (allfig, allax) = plt.subplots()
    plot_avg_reward(ql_key, 'QL', rw_key=rw_key, la_key=la_key, axes=allax)
    plot_avg_reward(faql_key, 'FAQL', axes=allax)
    plot_avg_reward(ruql_key, 'RUQL', axes=allax)
    allfig.savefig(str(outdir / 'avg_reward_all.pdf'), format='pdf')

    (ruql_train_avg, ruql_train_std) = avg_train_time(ruql_key)
    (ruql_test_avg, ruql_test_std) = avg_test_time(ruql_key)
    (ql_train_avg, ql_train_std) = avg_train_time(ql_key)
    (ql_test_avg, ql_test_std) = avg_test_time(ql_key)
    (faql_train_avg, faql_train_std) = avg_train_time(faql_key)
    (faql_test_avg, faql_test_std) = avg_test_time(faql_key)

    (la_test_avg, la_test_std) = avg_test_time(la_key)
    (rw_test_avg, rw_test_std) = avg_test_time(rw_key)

    plt.figure()
    train_yvec = np.array([ruql_train_avg,
                        ql_train_avg,
                        faql_train_avg]) * 1000
    train_errvec = np.array([ruql_train_std,
                            ql_train_std,
                            faql_train_std]) * 1.96 * 1000
    plt.errorbar(np.arange(3), y=train_yvec, yerr=train_errvec,
                fmt=' o', capsize=12, capthick=2.5, elinewidth=2.5)
    plt.grid()
    # plt.ylim(-1.5, 10)
    plt.ylabel('Average training step time [ms]')
    plt.xticks(np.arange(3), ['RUQL', 'QL', 'FAQL'])
    plt.savefig(str(outdir / 'avg_train_times.pdf'), format='pdf')

    plt.figure()
    test_yvec = np.array([rw_test_avg,
                        la_test_avg,
                        ruql_test_avg,
                        ql_test_avg,
                        faql_test_avg]) * 1000
    test_errvec = np.array([rw_test_std,
                            la_test_std,
                            ruql_test_std,
                            ql_test_std,
                            faql_test_std]) * 1.96 * 1000
    plt.errorbar(np.arange(5), y=test_yvec, yerr=test_errvec,
                fmt=' o', capsize=12, capthick=2.5, elinewidth=2.5)
    plt.grid()
    # plt.ylim(-1.5, 10)
    plt.ylabel('Average testing step time [ms]')
    plt.xticks(np.arange(5), ['RW', 'LA', 'RUQL', 'QL', 'FAQL'])
    plt.savefig(str(outdir / 'avg_test_times.pdf'), format='pdf')

    plot_test_reward(QL=ql_key, FAQL=faql_test_key, RUQL=ruql_key, RW=rw_key, LA=la_key)
    plt.savefig(str(outdir / 'test_rewards.pdf'), format='pdf')

    plt.show()
