import itertools
import operator
import time

from matplotlib import animation
import matplotlib.pyplot as plt
import numpy as np

from hrl.multigrid import MultiGridMDP
from hrl.qlearn import QLearner, LookaheadQLearner, IndepQLearners, RandomWalker
from hrl.utils import save_vars, avg_over_time, ProgressCounter

b_steps = 10
b_bins = (np.geomspace(1, 10, b_steps+1) - 1) / 9
b_bins = b_bins[1:]
        
grid = MultiGridMDP(agents=2,
                    x_min=-50, x_max=50,
                    y_min=-50, y_max=50,
                    reduced_x_steps=2,
                    reduced_y_steps=2,
                    reduced_b_steps=2,
                    reduced_b_empty=True,
                    x_steps=10, y_steps=10,
                    b_steps=b_steps, b_bins=b_bins,
                    r_epsilon=1e-4, r_phi_up=1.0125, r_phi_down=0.75)

assert(grid.state_space.ag0.x.stepsize == 10)
assert(grid.state_space.ag0.y.stepsize == 10)

ql = IndepQLearners(grid, epsilon=0.05, alpha=0.9, gamma=0.9,
                    flat_alpha=3000)

epcount = 10
itercount = 3000

rw = RandomWalker(grid)
pc = ProgressCounter(epcount*itercount)
random_hist = []
for _ in range(epcount):
    grid.reset()
    h = rw.learn(itercount, after=pc.show_progress) # -> (s,a,r,ns)
    random_hist += h

pc = ProgressCounter(epcount*itercount)
hist = []
for _ in range(epcount):
    grid.reset()
    h = ql.learn(itercount, after=pc.show_progress) # -> (s,a,r,ns)
    hist += h
# policy = ql.policy



save_vars('multiagent.pickle.xz',
          epcount=epcount,
          itercount=itercount,
          hist=hist,
          # policy=policy,
          grid=grid,
          #ql=ql,
          random_hist=random_hist)
