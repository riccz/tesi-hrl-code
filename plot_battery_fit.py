import math
import itertools
import sys

from scipy import optimize
import matplotlib.pyplot as plt
import numpy as np

from hrl.battery import TremblayBattery

plt.close()

fig = plt.figure()
origax = fig.add_subplot(1,1,1)
plotax = fig.add_axes(origax.get_position(), frameon=False)

orig_plot = plt.imread("batt_disch.png")
origax.imshow(orig_plot)
origax.tick_params(which='both', bottom='off', top='off', left='off', right='off',
                   labelbottom='off', labeltop='off',
                   labelleft='off', labelright='off')

plotax.set_xlim(0, 6000)
plotax.set_ylim(11.2, 17.6)
plotax.grid()
plotax.set_xticks(np.arange(0, 6000+500, 1000))
plotax.set_yticks(np.arange(11.2, 17.6+0.4, 0.8))

plotax.set_prop_cycle(linestyle=['dashed']*3,
                      color=['blue', 'magenta', 'purple'])

fig.set_size_inches(7.65, 6)

Q = 5.5 # [Ah]
R = 4.75e-3
Efull = 16.2
Eexp = 14.15
A = Efull - Eexp
B = 3/4
Enom = 14
Qnom = 4.5
K = (Efull - Enom + A*(math.exp(-B * Qnom) - 1)) * (Q - Qnom) / Qnom
plot_i = 20*Q
E0 = Efull + K + R*plot_i - A
minV = 12

TB = TremblayBattery(E0=E0, R=R, K=K, A=A, B=B, Q=Q, minV=minV)

plotax.grid()

Is = np.array([20, 40, 50]) * Q
Qs = np.linspace(0, 1.1*Q, 4096)
all_Vs = np.empty((len(Is), len(Qs)))
for i, I in enumerate(Is):
    TB.reset()
    Vs = []
    last_q = 0
    for q in Qs[1:]:
        Vs.append(TB.output_voltage(I))
        qdiff = q - last_q
        try:
            TB.draw_charge(qdiff)
        except ValueError:
            pass
        last_q = q
    Vs.append(TB.output_voltage(I))
    all_Vs[i,:] = Vs
    plotax.plot(Qs * 1000, Vs, label="I_batt = {:.0f}C".format(I/Q))

plotax.legend()

plotax.set_xlabel('Used charge [mAh]')
plotax.set_ylabel('V_batt [V]')

plt.show()
