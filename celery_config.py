from os import path
import configparser
import pathlib
import ssl
import urllib

from hrl import GIT_SHA1

here = path.abspath(path.dirname(__file__))

config = configparser.ConfigParser()
configpath = pathlib.Path.home() / '.hrl/secrets.conf'
if configpath.exists():
    config.read(str(configpath))
else:
    config.add_section('celery')

if 'password' in config['celery'] and 'redis_password' in config['celery']:
    redis_password = config['celery']['redis_password']
    server_cert = str(pathlib.Path(here) / 'letsencrypt_chain.crt')

    broker_url = ('redis://:{!s}'
                  '@redis.dyn.zanol.eu:443/0').format(redis_password)
    redis_backend_use_ssl = {
        'ssl_cert_reqs': ssl.CERT_REQUIRED,
        'ssl_ca_certs': server_cert,
    }
    broker_use_ssl = redis_backend_use_ssl

    url_args = {
        'ssl_cert_reqs': 'CERT_REQUIRED',
        'ssl_ca_certs': server_cert,
    }
    result_backend = ('rediss://:{!s}'
                      '@redis.dyn.zanol.eu:443/1'
                      '?{!s}').format(redis_password,
                                      urllib.parse.urlencode(url_args))

task_serializer = 'pickle'
result_serializer = 'pickle'
task_compression = 'gzip'
result_compression = 'gzip'

accept_content = ['pickle', 'json']

worker_prefetch_multiplier = 1
task_reject_on_worker_lost = True

task_default_queue = "hrl_{!s}".format(GIT_SHA1[:8])
task_create_missing_queues = True

enable_utc = True
timezone = 'UTC'
