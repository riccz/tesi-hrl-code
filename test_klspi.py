import argparse
import copy
import functools
import logging
import random
import warnings

from celery import Celery
import numpy as np

import hrl
from hrl import utils
from hrl import klspi
from hrl.common import grid, kern, preprocess

with warnings.catch_warnings():
    warnings.simplefilter("ignore", FutureWarning)
    import h5py

logger = logging.getLogger(__name__)
git_sha1 = hrl.GIT_SHA1
logger.info("Running with commit {!s}".format(git_sha1))

klspi_app = Celery('test_klspi', config_source='celery_config')


@klspi_app.task(acks_late=True)
def run_test(learner, test_iters):
    random.seed()
    np.random.seed()

    logger.debug("Starting test")
    avgr = learner.test(test_iters, reset=True)
    logger.debug("Test finished")
    return avgr


@klspi_app.task(acks_late=True)
def run_training(learner, train_iters):
    random.seed()
    np.random.seed()

    learner.learn(steps=train_iters)
    return learner


if __name__ == '__main__':
    parser_descr = 'Test KLSPI'
    parser = argparse.ArgumentParser(description=parser_descr,
                                     allow_abbrev=False)
    parser.add_argument("--ald-thresh", type=float, default=1e-3)
    parser.add_argument("--avgcount", type=int, default=30)
    parser.add_argument("--epsilon", type=float, default=0)
    parser.add_argument("--gamma", type=float, default=0.6)
    parser.add_argument("--klspi-iters", type=int, default=100)
    parser.add_argument("--sigma", type=float, default=1)
    parser.add_argument("--test-iters", type=int, default=10000)
    parser.add_argument("--train-iters", type=int, default=1000)

    parser.add_argument("--queue", type=str,
                        default=klspi_app.conf.task_default_queue)

    parser.add_argument("--no-upload", help="Don't upload the results",
                        action='store_false', dest='upload')
    parser.add_argument("--upload", help="Upload the results",
                        action='store_true', dest='upload')
    parser.set_defaults(upload=True)
    args = parser.parse_args()

    train_iters = args.train_iters
    test_iters = args.test_iters
    klspi_iters = args.klspi_iters
    avgcount = args.avgcount
    epsilon = args.epsilon
    gamma = args.gamma
    ald_thresh = args.ald_thresh
    sigma = args.sigma

    grid_cp = copy.deepcopy(grid)
    learner_kern = functools.partial(kern, sigma=sigma)
    learner = klspi.KLSPILearner(grid_cp, kern=learner_kern, gamma=gamma,
                                 ald_thresh=ald_thresh,
                                 epsilon=epsilon)
    grid_cp.reset()

    logger.debug("Starting training")
    avgrewards = np.empty((klspi_iters, avgcount))
    policies = np.empty(klspi_iters, dtype=object)
    avgrewards_results = np.empty(avgrewards.shape, dtype=object)
    for i in range(klspi_iters):
        learner = run_training.apply_async((learner, train_iters),
                                           queue=args.queue).get()
        policies[i] = learner.policy()
        for j in range(avgcount):
            avgrewards_results[i, j] = run_test.apply_async((learner, test_iters),
                                                            queue=args.queue)
        logger.debug("Training iteration {:d} done".format(i))
    logger.debug("Training finished")

    for i in range(klspi_iters):
        for j in range(avgcount):
            avgrewards[i, j] = avgrewards_results[i, j].get()
        logger.debug("Average reward after {:d}"
                     " training steps = {:f}".format(i+1, avgrewards[i,:].mean()))
    logger.info("Finished")

    with h5py.File('klspi.hdf5', 'w') as f:
            f.attrs['git_sha1'] = git_sha1

            f.attrs['params'] = utils.pack_objects(train_iters=train_iters,
                                                   test_iters=test_iters,
                                                   klspi_iters=klspi_iters,
                                                   avgcount=avgcount,
                                                   epsilon=epsilon,
                                                   gamma=gamma,
                                                   ald_thresh=ald_thresh,
                                                   sigma=sigma)

            avgrs_ds = f.create_dataset('avg_rewards', avgrewards.shape,
                                        dtype=float,
                                        compression='gzip')
            avgrs_ds[:] = avgrewards

            # policies_gr = f.create_group('policies')
            # for i in range(klspi_iters):
            #     iter_gr = policies_gr.create_group("i{:d}".format(i))
            #     alpha = policies[i].alpha
            #     alpha_ds = iter_gr.create_dataset('alpha', alpha.shape,
            #                                       dtype=float)
            #     alpha_ds[:] = alpha
            #     D = np.empty((len(policies[i].D), len(policies[i].D[0])))
            #     for j, d in enumerate(policies[i].D):
            #         D[j, :] = d
            #     D_ds = iter_gr.create_dataset('D', D.shape, dtype=float)
            #     D_ds[:] = D

    key = utils.aws_upload_data('klspi.hdf5', 'test_klspi')
    logger.info("Uploaded with key {!s}".format(key))
