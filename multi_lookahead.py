import argparse
import logging
import random
import math
import warnings

from celery import Celery, group, chain, chord
import numpy as np

from hrl import utils
from hrl.common import multigrid, output_opts_parser
from hrl.mcts import MCTS
from hrl_app import test_and_time, aggregate_test_and_time
import hrl

with warnings.catch_warnings():
    warnings.simplefilter("ignore", FutureWarning)
    import h5py

logger = logging.getLogger(__name__)

logger.info("Running with commit {!s}".format(hrl.GIT_SHA1))
parser_descr = 'Compute the average reward for multi-agent lookahead'
parser = argparse.ArgumentParser(description=parser_descr,
                                 allow_abbrev=False,
                                 parents=[output_opts_parser])
parser.add_argument("--avgcount", help="Number of runs to average",
                    type=int, default=50)
parser.add_argument("--test-iters", help="Test iterations",
                    type=int, default=10000)
parser.add_argument("--gamma", help="gamma",
                    type=float, default=0.6)
parser.add_argument("--max-depth", help="max depth",
                    type=int, default=2)
parser.add_argument("--rollout-count", help="rollout count",
                    type=int, default=10)
parser.add_argument("--max-fanout", help="max fanout",
                    type=int, default=16)
args = parser.parse_args()

base_mcts = MCTS(multigrid, gamma=args.gamma,
                 max_depth=args.max_depth,
                 rollout_count=args.rollout_count,
                 uct_c=np.sqrt(2),
                 max_fanout=args.max_fanout)
base_mcts.test_time_counter.keep_all = True
base_packed = base_mcts.pack()

chunk_iters = math.ceil(args.test_iters / 100)

# Keep one history
test0 = test_and_time.s(test_iters=chunk_iters, hist_label='hist')
test = test_and_time.s(test_iters=chunk_iters)

test0chain = chain(test0 for _ in range(100))
testchain = chain(test for _ in range(100))

full_head = [test0chain] + [testchain for _ in range(1, args.avgcount)]
full_body = aggregate_test_and_time.s(upload_label='multi_mcts')
full_task = chord(full_head, body=full_body)

full_result = full_task.apply_async(args=({'learner': base_packed},))
key = full_result.get()
logger.info("Uploaded with key {!s}".format(key))
