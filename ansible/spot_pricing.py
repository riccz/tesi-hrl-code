import pandas as pd
import matplotlib.pyplot as plt
from boto3 import client

c5types = {
    'c5.18xlarge': 72,
    'c5.2xlarge': 8,
    'c5.4xlarge': 16,
    'c5.9xlarge': 36,
    'c5.large': 2,
    'c5.xlarge': 4,
    'c5d.18xlarge': 72,
    'c5d.2xlarge': 8,
    'c5d.4xlarge': 16,
    'c5d.9xlarge': 36,
    'c5d.large': 2,
    'c5d.xlarge': 4,
}

ec2 = client('ec2')
prices = ec2.describe_spot_price_history(
    AvailabilityZone="eu-west-1b",
    ProductDescriptions=['Linux/UNIX (Amazon VPC)'],
    InstanceTypes=list(c5types.keys()),
)
df = pd.DataFrame(prices['SpotPriceHistory'])

df = df.drop(['ProductDescription', 'AvailabilityZone'], axis=1)
df.set_index("Timestamp", inplace=True)
df["SpotPrice"] = df.SpotPrice.astype(float)
df = df.sort_index()

scaled = None
for it, g in df.groupby('InstanceType'):
    h = g.copy()
    ncpus = c5types[it]
    h['PerCPUPrice'] = h['SpotPrice'] / ncpus
    h['CPUCount'] = ncpus
    if scaled is None:
        scaled = h
    else:
        scaled = scaled.append(h)
scaled = scaled.sort_index()

week_ago = pd.datetime.now() - pd.Timedelta("7 days")
past_week = scaled.loc[week_ago:]
past_week_group = past_week.groupby('InstanceType')
quant80 = past_week_group.quantile(0.8)
quant80 = quant80.sort_values('PerCPUPrice')
print(quant80)

plt.figure()
for it, g in scaled.groupby('InstanceType'):
    plt.plot(g.index, g.PerCPUPrice, label=it)
plt.legend()
plt.grid()
plt.show()



# twice_daily.plot()
# plt.show()