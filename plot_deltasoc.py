import math

import numpy as np
import matplotlib.pyplot as plt
from scipy import stats

from hrl.battery import TremblayBattery

Q = 5.5 # [Ah]
R = 4.75e-3
Efull = 16.2
Eexp = 14.15
A = Efull - Eexp
B = 3/4
Enom = 14
Qnom = 4.5
K = (Efull - Enom + A*(math.exp(-B * Qnom) - 1)) * (Q - Qnom) / Qnom
plot_i = 20*Q
E0 = Efull + K + R*plot_i - A
minV = 12

TB = TremblayBattery(E0=E0, R=R, K=K, A=A, B=B, Q=Q, minV=minV)

def muprime(mu, E):
    return E**2 / (4 * R**2) - mu / R

def sigmaprime(sigma):
    return sigma / R

def open_voltage(soc):
    TB.soc = soc 
    return TB.output_voltage(0)

def deltasoc_cdf(x, soc, T, mu, sigma):
    assert(x >= 0 and x <= soc)

    E = open_voltage(soc)

    y = E / (2*R) - Q*3600/T * x 
    z = (y**2 - muprime(mu, E)) / sigmaprime(sigma)
    cdf0 = stats.norm.cdf(-muprime(mu, E)/sigmaprime(sigma))
    return 1 - (stats.norm.cdf(z) -cdf0) / (1 - cdf0)

plt.figure()
for soc in [1, 0.75, 0.5, 0.25, 0.016]:
    T = 10
    # mu = 338  # W
    # sigma = 19.79e-3 * mu
    mu = 848  # W
    sigma = 76.92e-3 * mu

    # xs = np.linspace(0.009, 0.014, 4096)
    xs = np.linspace(0, min(1, soc), 4096)
    deltas = np.fromiter((deltasoc_cdf(x, soc, T, mu, sigma) for x in xs), dtype=float)

    plt.plot(xs * 100, deltas, label="soc={:.2f}".format(soc))
plt.grid()
plt.legend()
plt.show()

# plt.figure()
# Nstds = np.random.randn(50000)
# absNs = np.abs(Nstds)
# sqrtNs = np.sqrt(absNs)
# (cum, low, binsize, extrapoints) = stats.cumfreq(absNs, 100)
# xs = low + np.linspace(0, binsize * cum.size, cum.size)
# plt.plot(xs, cum / cum[-1])

# xs = np.linspace(0, 5, 4096)
# absNcdf = 2 * (stats.norm.cdf(xs) - 1/2)
# plt.plot(xs, absNcdf)
# plt.show()

