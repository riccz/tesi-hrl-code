import math
import itertools
import sys

import numpy as np
from matplotlib import pyplot as plt

def future_avg_reward(k, pon, poff):
    assert(k >= 0)
    assert(pon > 0 and pon < 1)
    assert(poff > 0 and poff < 1)

    if k == 0:
        return (0.0, 0.0)
    elif k == 1:
        return (1 - poff, pon)
    else:
        (next_on, next_off) = future_avg_reward(k - 1, pon, poff)
        avg_off = pon * next_on + (1 - pon) * next_off
        avg_on = poff * next_off + (1 - poff) * next_on
        return (avg_on, avg_off)

scale_x = 712
scale_y = 0.33

T = 15
pon = 1/500
poff = 1/12

plt.close()

fig = plt.figure()
origax = fig.add_subplot(1,1,1)
plotax = fig.add_axes(origax.get_position(), frameon=False)

orig_plot = plt.imread("3dpoints.png")
origax.imshow(orig_plot)
origax.tick_params(which='both', bottom='off', top='off', left='off', right='off',
                   labelbottom='off', labeltop='off',
                   labelleft='off', labelright='off')

plotax.set_xlim(0, 1400)
plotax.set_ylim(0.3, 1)
plotax.grid()
plotax.set_xticks(np.arange(0, 1400+1, 200))
plotax.set_yticks(np.arange(0.3, 1+0.05, 0.1))

plotax.set_prop_cycle(linestyle=['dashed']*4,
                      color=['blue', 'red', 'teal', 'magenta'])

invTrange = np.linspace(1400/T * 0.5, 1400/T * 1.5, 4)
for invT in invTrange:
    T = 1400/invT
    print("T = 1400/{:f} = {:f}".format(invT, T))
    ks = np.arange(math.ceil(1400 / T))
    ts = T * ks

    scale_k = math.floor(scale_x / T)

    r_down = np.asarray([future_avg_reward(k, pon, poff)[0] for k in ks])
    sum_r = np.fromiter(itertools.accumulate(r_down), dtype=float)
    normalized_sum_r = 1 - sum_r / sum_r[scale_k] * (1 - scale_y)
    plotax.step(ts, normalized_sum_r, where='post')

fig.set_size_inches(6.22, 3.725)

# plt.show()
# sys.exit(0)

# Plot to save

T = 15
pon = 1/500
poff = 1/12
maxK = math.ceil(1400 / T)
scale_k = math.floor(scale_x / T)
r_down = np.asarray([future_avg_reward(k, pon, poff)[0] for k in range(maxK)])
sum_r = np.fromiter(itertools.accumulate(r_down), dtype=float)
norm_C = sum_r[scale_k] / (1 - scale_y)
normalized_sum_r = 1 - sum_r / norm_C
print("Normalization 1/C = {:f}".format(norm_C))
plt.figure("normalized cost")
plt.step(np.arange(maxK) * T, normalized_sum_r, label="Small T", where='post')
plt.figure("reward")
plt.step(np.arange(maxK), sum_r, label="Small T", where='post')

# T = 1400/256
# pon = 1/100
# poff = 1/45
# ks = np.round(ts / T)
# r_down = np.asarray([future_avg_reward(k, pon, poff)[0] for k in ks])
# plt.plot(ts, r_down, label="Long runs (1)")


T = 100
pon = 1/45
poff = 1/2.5
maxK = math.ceil(1400 / T)
scale_k = math.floor(scale_x / T)
r_down = np.asarray([future_avg_reward(k, pon, poff)[0] for k in range(maxK)])
sum_r = np.fromiter(itertools.accumulate(r_down), dtype=float)
norm_C = sum_r[scale_k] / (1 - scale_y)
normalized_sum_r = 1 - sum_r / norm_C
print("Normalization 1/C = {:f}".format(norm_C))
plt.figure("normalized cost")
plt.step(np.arange(maxK) * T, normalized_sum_r, label="Large T", where='post')
plt.figure("reward")
plt.step(np.arange(maxK), sum_r, label="Large T", where='post')

plt.figure("normalized cost")
plt.grid()
plt.xlabel("Time [s]")
plt.ylabel("Normalized cumulative avg. cost")
plt.ylim(0.3, 1)
plt.xlim(0, 1400)
plt.gca().set_xticks(np.arange(0, 1400+1, 200))
plt.gca().set_yticks(np.arange(0.3, 1+0.05, 0.1))
plt.legend()
plt.gcf().set_size_inches(6.22, 3.725)
plt.savefig('plots/3d_model.pdf', format='pdf')

plt.figure("reward")
plt.grid()
plt.xlabel("Time step")
plt.ylabel("Cumulative avg. reward")
# plt.ylim(0.3, 1)
plt.xlim(-1, 51)
# plt.gca().set_xticks(np.arange(0, 1400+1, 200))
# plt.gca().set_yticks(np.arange(0.3, 1+0.05, 0.1))
plt.legend()
plt.gcf().set_size_inches(6.22, 3.725)
plt.savefig('plots/3d_model_raw.pdf', format='pdf')

plt.show()