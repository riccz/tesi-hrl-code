#!/bin/bash

set -euo pipefail

app="$1"
worker_count="$2"
queues="${3:-}"

runner_script() {
    if [ -n "$queues" ]; then
        queues_arg="--queues ${queues}"
    else
        queues_arg=""
    fi

    cat <<- EOM
        set -euo pipefail

        ( while true; do \
            /usr/bin/find $HOME/.cache/ -type f -mtime 0.03 \
                -exec rm '{}' \; 2>/dev/null || true;\
             sleep 1800;\
        done ) &

        while true; do
            $HOME/.local/bin/celery worker \
                --concurrency=1 \
                --app=${app} \
                -O fair \
                --loglevel=info \
                --without-gossip \
                --without-mingle \
                --without-heartbeat \
                ${queues_arg} \
                --hostname=dei\${JOB_ID} &
            celery_pid="\$!"
            sleep 6h &
            sleep_pid="\$!"
            wait -n || true
            kill -TERM "\$sleep_pid" || true
            kill -TERM "\$celery_pid" || true
            wait "\$sleep_pid" || true
            wait "\$celery_pid" || true
        done
EOM
}

for ((i = 0; i < worker_count; i++)); do
    runner_script | qsub -cwd -m ea -j y -N celery_worker
done
