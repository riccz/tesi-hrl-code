import argparse
import copy
import itertools
import operator
import pathlib
import warnings

from matplotlib import animation
from matplotlib import pyplot as plt
import numpy as np

from hrl import utils
from hrl.common import plot_opts_parser, algo_color, algo_order
from hrl.utils.iter import avg_over_time

with warnings.catch_warnings():
    warnings.simplefilter("ignore", FutureWarning)
    import h5py

from plot_single_agent import plot_avg_reward as _orig_plot_avg_reward


def plot_avg_reward(*args, **kwargs):
    return _orig_plot_avg_reward(*args, **kwargs, la_label='MCTS')


def plot_test_reward(**keys):
    figs = dict()
    sorted_kwargs = sorted(keys.items(), key=lambda kv: algo_order[kv[0]])
    for label, key in sorted_kwargs:
        (fig, ax) = plt.subplots()
        figs[label] = fig
        with utils.aws.aws_fetch_data(key) as f:
            params = utils.unpack_objects(f['plot_data'].attrs['params'])
            grid = params['grid']
            test_iters = params['test_iterations']

            if 'hist_last' in f:
                hist_label = 'hist_last'
            elif 'hist' in f:
                hist_label = 'hist'
            else:
                hist_labels = [k for k in f.keys() if k.startswith("hist_")]
                assert(len(hist_labels) == 1)
                hist_label = hist_labels[0]

            test_steps = np.arange(1, test_iters + 1)
            rewards = np.empty((grid.agents, test_iters))
            for i, ag in enumerate(grid.agent_keys):
                rewards[i, :] = f[hist_label]['rewards'][ag]
                timeavg_reward = np.fromiter(avg_over_time(rewards[i, :]),
                                             dtype=float)
                ax.plot(test_steps, timeavg_reward, label=ag,
                        color=algo_color[label])
        ax.grid(True)
        ax.legend()
        ax.set_xlabel('Test steps')
        ax.set_ylabel('Normalized cumulative test reward')
        ax.set_ylim(0.1, 0.5)
    return figs


def avg_train_time(key):
    with utils.aws.aws_fetch_data(key) as f:
        avg_train_times = np.asarray(f['plot_data']['avg_train_times'])
        std_train_times = np.asarray(f['plot_data']['std_train_times'])

        global_avg_train_time = np.mean(avg_train_times)
        pow_train_times = std_train_times ** 2 + avg_train_times ** 2
        global_std_train_time = np.sqrt(np.mean(pow_train_times) -
                                        global_avg_train_time ** 2)

        return (global_avg_train_time, global_std_train_time)


def avg_test_time(key):
    with utils.aws.aws_fetch_data(key) as f:
        avg_test_times = np.asarray(f['plot_data']['avg_test_times'])
        std_test_times = np.asarray(f['plot_data']['std_test_times'])

        global_avg_test_time = np.mean(avg_test_times)
        pow_test_times = std_test_times ** 2 + avg_test_times ** 2
        global_std_test_time = np.sqrt(np.mean(pow_test_times) -
                                       global_avg_test_time ** 2)

        return (global_avg_test_time, global_std_test_time)


if __name__ == '__main__':
    plt.close()

    # Final data
    faql_key = 'data/multi_agent/b4590ba5/ZDLMSByv8csj1pq0'
    la_key = 'data/multi_mcts/cc7393d8/bWx4DVtDDvJ7Zeeh'
    ql_key = 'data/multi_agent/b4590ba5/sThEXLvAuHTHSTST'
    ruql_key = 'data/multi_agent/b4590ba5/h0hOV7VuZsqBKeal'
    rw_key = 'data/multi_random_walk/6e57ddf1/clvW2cE0TmJTdJFB'

    outdir = pathlib.Path('plots/multi')
    if not outdir.exists():
        outdir.mkdir(parents=True)

    plot_avg_reward(ruql_key, 'RUQL', rw_key=rw_key, la_key=la_key)
    plt.ylim(0.1, 0.5)
    plt.savefig(str(outdir / 'avg_reward_ruql.pdf'), format='pdf')
    plot_avg_reward(ql_key, 'QL', rw_key=rw_key, la_key=la_key)
    plt.ylim(0.1, 0.5)
    plt.savefig(str(outdir / 'avg_reward_ql.pdf'), format='pdf')
    plot_avg_reward(faql_key, 'FAQL', rw_key=rw_key, la_key=la_key)
    plt.ylim(0.1, 0.5)
    plt.savefig(str(outdir / 'avg_reward_faql.pdf'), format='pdf')

    (allfig, allax) = plt.subplots()
    plot_avg_reward(ql_key, 'QL', rw_key=rw_key, la_key=la_key, axes=allax)
    plot_avg_reward(faql_key, 'FAQL', axes=allax)
    plot_avg_reward(ruql_key, 'RUQL', axes=allax)
    allax.set_ylim(0.35, 0.45)
    allfig.savefig(str(outdir / 'avg_reward_all.pdf'), format='pdf')

    (ruql_train_avg, ruql_train_std) = avg_train_time(ruql_key)
    (ruql_test_avg, ruql_test_std) = avg_test_time(ruql_key)
    (ql_train_avg, ql_train_std) = avg_train_time(ql_key)
    (ql_test_avg, ql_test_std) = avg_test_time(ql_key)
    (faql_train_avg, faql_train_std) = avg_train_time(faql_key)
    (faql_test_avg, faql_test_std) = avg_test_time(faql_key)

    # (la_test_avg, la_test_std) = avg_test_time(la_key)
    # (rw_test_avg, rw_test_std) = avg_test_time(rw_key)

    # plt.figure()
    # train_yvec = np.array([ruql_train_avg,
    #                        ql_train_avg,
    #                        faql_train_avg]) * 1000
    # train_errvec = np.array([ruql_train_std,
    #                          ql_train_std,
    #                          faql_train_std]) * 1.96 * 1000
    # plt.errorbar(np.arange(3), y=train_yvec, yerr=train_errvec,
    #              fmt=' o', capsize=12, capthick=2.5, elinewidth=2.5)
    # plt.grid()
    # # plt.ylim(-1.5, 10)
    # plt.ylabel('Average training step time [ms]')
    # plt.xticks(np.arange(3), ['RUQL', 'QL', 'FAQL'])
    # plt.savefig(str(outdir / 'avg_train_times.pdf'), format='pdf')

    # plt.figure()
    # test_yvec = np.array([la_test_avg,
    #                       ruql_test_avg,
    #                       ql_test_avg,
    #                       faql_test_avg]) * 1000
    # test_errvec = np.array([la_test_std,
    #                         ruql_test_std,
    #                         ql_test_std,
    #                         faql_test_std]) * 1.96 * 1000
    # plt.errorbar(np.arange(4), y=test_yvec, yerr=test_errvec,
    #              fmt=' o', capsize=12, capthick=2.5, elinewidth=2.5)
    # plt.grid()
    # # plt.ylim(-1.5, 10)
    # plt.ylabel('Average testing step time [ms]')
    # plt.xticks(np.arange(5), ['LA', 'RUQL', 'QL', 'FAQL'])
    # plt.savefig(str(outdir / 'avg_test_times.pdf'), format='pdf')

    test_figs = plot_test_reward(QL=ql_key, FAQL=faql_key, RUQL=ruql_key, RW=rw_key, MCTS=la_key)
    for label, fig in test_figs.items():
        fig.savefig(str(outdir / 'test_rewards_{!s}.pdf'.format(label)), format='pdf')

    plt.show()
