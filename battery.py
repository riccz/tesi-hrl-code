import math

from scipy import optimize
import matplotlib.pyplot as plt
import numpy as np

from hrl.battery import TremblayBattery

E0 = 3.7348
R = 0.09
K = 0.0087662
A = 0.468
B = 3.5294
Q = 1.0 # [Ah]
TB = TremblayBattery(E0=E0, R=R, K=K, A=A, B=B, Q=Q)

# V vs time
Is = [0.2*Q, Q, 2*Q]
(ts, T) = np.linspace(0, 6*3600, 4096, retstep=True)

Vs = np.empty((len(Is), len(ts)))
SoCs = np.empty(Vs.shape)
for n, I in enumerate(Is):
    TB.reset()
    for j in range(len(ts)):
        Vs[n,j] = TB.v_out(I)
        SoCs[n,j] = TB.soc
        TB.draw_i(I, T)

# TB.reset()
# Vs[-1,0] = TB.v_open()
# for j, t in enumerate(ts[1:]):
#     # I = np.random.randn() * 0.3 * 0.2*Q + 0.2*Q
#     if np.random.rand() > 0.3:
#         I = 0.2*Q
#     else:
#         I = 0
#     Vs[-1,j+1] = TB.draw_i(I)


plt.figure()

for n in range(Vs.shape[0]):
    plt.plot(ts / 60, Vs[n,:])

plt.ylim(2.5, 4.5)
plt.xlabel("t")
plt.ylabel("Vout")
plt.grid()

# SoC vs time
plt.figure()

for n in range(SoCs.shape[0]):
    plt.plot(ts / 60, SoCs[n,:])

plt.ylim(0, 1)
plt.xlabel("t")
plt.ylabel("SoC")
plt.grid()

# # V vs Q
# Qs = np.linspace(0, Q, 4096)
# Is = [0.2*Q, Q, 2*Q]

# Vs = np.empty((len(Is), len(Qs)))
# for n, I in enumerate(Is):
#     Vs[n,:] = np.fromiter((out_voltage(I, (q/I) * 3600) for q in Qs), dtype=float)

# plt.figure()

# for n, I in enumerate(Is):
#     plt.plot(Qs, Vs[n,:])

# plt.ylim(2.5, 4.5)
# plt.grid()

# # V vs I
# Is = np.linspace(0.2*Q, 2*Q, 4096)
# T = 30*60
# Vs = np.fromiter((out_voltage(i, T) for i in Is), dtype=float)

# plt.figure()
# plt.plot(Is, Vs)
# plt.grid()
# plt.ylim(2.5, 4.5)

# Power func
TB.reset()

# minV = 2.5
# maxV = TB.v_open()
# maxI = P / minV
# minI = P /maxV

# TB.draw_i(1, 3600)
Ps = np.linspace(20, 60, 8+2)[1:-1]
plt.figure()
for P in Ps:
    Is = np.linspace(0, (TB.Q - TB.curr_int) / (T / 3600), 4096+2)[1:-1]
    fs = np.fromiter(map(lambda i: TB._fixed_power_func(i, P, T), Is), dtype=float)
    (l,) = plt.plot(Is, fs)

    minI = P / TB.v_open()
    maxI = P / 2.5
    plt.axvline(x=minI, color=l.get_color(), linestyle='dashed')
    plt.axvline(x=maxI, color=l.get_color(), linestyle='dotted')

    (i0, v0) = TB.draw_power(P, T)
    TB.reset()
    plt.plot([i0], [0], marker='o', color=l.get_color())
    
plt.grid()
plt.ylim(-10, 10)

# Find solution for given P
#f = lambda i: TB.fixed_power_func(P, i)
#(i0, r) = optimize.brentq(f, minI, maxI, full_output=True)

# Try P range
# Ps = np.linspace(3.7*0.2, 3.7*2, 256)
# Is = np.empty(Ps.shape)
# for n,P in enumerate(Ps):
#     f = lambda i: TB.fixed_power_func(P, i)
#     Is[n] = optimize.brentq(f, minI, maxI, full_output=True)

# plt.figure()
# plt.plot(Ps, Is)

plt.show()
