import math


from scipy import special
from matplotlib import pyplot
import numpy as np

from hrl.battery import CounterBattery

def m(t, ps, delta_s, delta_m):
    def m_term(n):
        js = np.arange(n+1)
        inds = js * delta_s + (n - js) * delta_m <= t
        binoms = special.binom(n, js) * ps**js * (1-ps)**(n-js)
        return np.vdot(inds, binoms)

    upper_n = math.floor(t/delta_s)
    return sum(map(m_term, range(1, upper_n+1)))

space_step = 10
speed = 8
time_step = space_step / speed

delta_s = 0.122/100 * time_step
delta_m = 0.038/100 * space_step

ps = 0.6

pyplot.figure()
pyplot.axhline(y=math.floor(1/delta_s), linestyle='dashed')
pyplot.axhline(y=math.floor(1/delta_m), linestyle='dashed')
pss = np.linspace(0, 1, 128)
ms = np.fromiter((m(1, ps, delta_s, delta_m) for ps in pss), dtype=float)
pyplot.plot(pss, ms)
pyplot.grid()

pss_sim = np.linspace(0, 1, 32)
ms_sim = np.empty((len(pss_sim), 1000))
for i, ps in enumerate(pss_sim):
    for j in range(ms_sim.shape[1]):
        soc = 1
        N = 0
        while soc > 0:
            if np.random.rand() < ps:
                delta = delta_s
            else:
                delta = delta_m
            soc -= delta
            if soc >= 0: N += 1
        ms_sim[i,j] = N

ms_sim_avg = np.mean(ms_sim, axis=1)
pyplot.plot(pss_sim, ms_sim_avg)

pss = np.linspace(0, 1, 16)
avg_Ns = np.empty(pss.shape)
sigma_Ns = np.empty(pss.shape)
for i, ps in enumerate(pss):
    CB = CounterBattery(5, m(1, ps, delta_s, delta_m))
    cnts = np.empty(5000)
    for rep in range(len(cnts)):
        CB.reset()
        cnt = 0
        while CB.state > 0:
            CB.draw()
            cnt += 1
        cnts[rep] = cnt

    avg_Ns[i] = np.mean(cnts)
    sigma_Ns[i] = np.std(cnts)

(line,) = pyplot.plot(pss, avg_Ns, marker='o')
pyplot.plot(pss, avg_Ns + 1.96 * sigma_Ns, linestyle='dotted', color=line.get_color())
pyplot.plot(pss, avg_Ns - 1.96 * sigma_Ns, linestyle='dotted', color=line.get_color())

pss = np.linspace(0, 1, 16)
avg_Ns = np.empty(pss.shape)
sigma_Ns = np.empty(pss.shape)
for i, ps in enumerate(pss):
    CB = CounterBattery(5, m(1, ps, delta_s, delta_m), alpha=3)
    cnts = np.empty(5000)
    for rep in range(len(cnts)):
        CB.reset()
        cnt = 0
        while CB.state > 0:
            CB.draw()
            cnt += 1
        cnts[rep] = cnt

    avg_Ns[i] = np.mean(cnts)
    sigma_Ns[i] = np.std(cnts)

(line,) = pyplot.plot(pss, avg_Ns, marker='o')
pyplot.plot(pss, avg_Ns + 1.96 * sigma_Ns, linestyle='dotted', color=line.get_color())
pyplot.plot(pss, avg_Ns - 1.96 * sigma_Ns, linestyle='dotted', color=line.get_color())

pyplot.show()
