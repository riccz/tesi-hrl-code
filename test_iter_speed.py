import timeit
import random

def f(ks):
    return [random.randrange(k) for k in ks]

def g(ks):
    return (random.randrange(k) for k in ks)

def h(ks):
    for k in ks:
        yield random.randrange(k)
def j(ks):
    return map(random.randrange, ks)

ftime = timeit.timeit(lambda: [x for x in f(range(100, 3100))], number=1000)
gtime = timeit.timeit(lambda: [x for x in g(range(100, 3100))], number=1000)
htime = timeit.timeit(lambda: [x for x in h(range(100, 3100))], number=1000)
jtime = timeit.timeit(lambda: [x for x in j(range(100, 3100))], number=1000)

print("f: {:f}\ng: {:f}\nh: {:f}\nj: {:f}".format(ftime, gtime, htime, jtime))
