import copy
import itertools
import operator
import pickle

from matplotlib import animation
import matplotlib.pyplot as plt
import numpy as np

from hrl.utils import load_vars, avg_over_time, flatten_dicts

plt.close()

data = load_vars('multiagent.pickle.xz')
hist = data['hist']
# random_hist = data['random_hist']
# policy = data['policy']
grid = data['grid']
# ql = data['ql']
del data
states = list(map(operator.itemgetter(0), hist))
del hist

plt.figure()
batts = dict()
for ag in sorted(grid.agent_keys):
    batts[ag] = np.fromiter(map(lambda s: s[ag]['b'], states), dtype=float)
    plt.plot(range(0, len(states)), batts[ag], label=ag)

plt.grid()
plt.legend()
plt.xlim(0, 1000)
plt.ylim(0, 1)
plt.savefig('battery_vs_timestep.pdf', format='pdf')

plt.figure()
zero_runs = dict()
for ag in sorted(grid.agent_keys):
    last_zero_run = 0
    zero_runs[ag] = np.zeros(len(states))
    for t,b in enumerate(batts[ag]):
        if grid.state_space.spaces[ag].b.min == b:
            last_zero_run += 1
            zero_runs[ag][t] = last_zero_run
        else:
            last_zero_run = 0
    plt.plot(range(len(states)), zero_runs[ag], label=ag)
plt.grid()
plt.legend()

plt.figure()
for ag in sorted(grid.agent_keys):
    isnonzero = (zero_runs[ag] != 0).astype(int, casting='safe')
    avg_nonzero = np.fromiter(avg_over_time(isnonzero), dtype=float)
    plt.plot(range(len(states)), avg_nonzero, label=ag)

plt.grid()
plt.legend()
plt.xlabel('time step')
plt.ylabel('avg empty')
plt.savefig('avg_empty.pdf', format='pdf')

plt.show()
