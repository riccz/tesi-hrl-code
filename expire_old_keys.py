#!/usr/bin/env python3

from os import path
import configparser
import pathlib
import redis
import ssl

ttl_hours = 12

here = path.abspath(path.dirname(__file__))

config = configparser.ConfigParser()
configpath = pathlib.Path.home() / '.hrl/secrets.conf'
config.read(str(configpath))

redis_password = config['celery']['redis_password']
server_cert = str(pathlib.Path(here) / 'letsencrypt_chain.crt')

# Results
redis_db = redis.StrictRedis(
    host='redis.dyn.zanol.eu',
    port=443,
    db=1,
    password=redis_password,
    ssl=True,
    ssl_cert_reqs=ssl.CERT_REQUIRED,
    ssl_ca_certs=server_cert,
)

for k in redis_db.scan_iter(match='celery-task-meta-*'):
    ttl = redis_db.ttl(k)
    new_ttl = ttl_hours * 3600
    if new_ttl < ttl:
        redis_db.expire(k, new_ttl)

# Broker
# redis_db = redis.StrictRedis(
#     host='rabbitmq.dyn.zanol.eu',
#     port=443,
#     db=0,
#     password=redis_password,
#     ssl=True,
#     ssl_cert_reqs=ssl.CERT_REQUIRED,
#     ssl_ca_certs=server_cert,
# )

# for k in redis_db.scan_iter(match='celery-task-meta-*'):
#     ttl = redis_db.ttl(k)
#     new_ttl = min(ttl, 12 * 3600)
#     redis_db.expire(k, new_ttl)
