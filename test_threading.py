from celery import Celery
import time
import copy
import random
from threading import Thread




def init_thread():

    app = Celery('test_threading', config_source='celery_config')
    app.conf.task_default_queue = 'test_queue'

    @app.task(bind=True)
    def hello(self, a, b):
        time.sleep(random.random())
        self.update_state(state="PROGRESS", meta={'progress': 50})
        time.sleep(random.random())
        self.update_state(state="PROGRESS", meta={'progress': 90})
        time.sleep(random.random())
        return 'hello world: %i' % (a+b)

    return (app, hello)

(app_main, hello_main) = init_thread()

if __name__ == '__main__':
    def run():
        (app, hello) = init_thread()
        
        handle = hello.delay(1, 2)
        print(handle.get())

    threads = [Thread(target=run) for _ in range(256)]

    for t in threads:
        t.start()

    for t in threads:
        t.join()
