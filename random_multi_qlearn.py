import argparse
import logging
import warnings

from celery import chain, group
import numpy as np

from hrl import qlearn
from hrl import utils
from hrl.common import multigrid, output_opts_parser
from hrl_app import run_single_test, run_single_training, revoke_all
import hrl

with warnings.catch_warnings():
    warnings.simplefilter("ignore", FutureWarning)
    import h5py

logger = logging.getLogger(__name__)

logger.info("Running with commit {!s}".format(hrl.GIT_SHA1))
parser_descr = 'Run a random grid search for multi-agent Q-learning'
parser = argparse.ArgumentParser(description=parser_descr,
                                 allow_abbrev=False,
                                 parents=[output_opts_parser])
parser.add_argument("--test-points", help="Number of random points", type=int)
parser.add_argument("--train-iters", help="Training iterations",
                    type=int, default=2000000)
parser.add_argument("--test-iters", help="Test iterations",
                    type=int, default=10000)
parser.add_argument("--avgcount", help="How many samples for each parameter set",
                    type=int, default=10)
args = parser.parse_args()

# Parameters to search
alphas = np.random.uniform(0.5, 1, args.test_points)
alpha_exps = np.random.uniform(0.5, 1, args.test_points)
gammas = np.random.uniform(0.4, 0.9, args.test_points)

is_softmax = np.random.choice((True, False), args.test_points)
taus = np.random.uniform(0.01, 2, args.test_points)
tau_consts = np.fromiter((np.random.uniform(0, (t - 1e-3) / args.train_iters)
                          for t in taus),
                         dtype=float)
taus[np.logical_not(is_softmax)] = np.nan
tau_consts[np.logical_not(is_softmax)] = np.nan
epsilons = np.random.uniform(0.01, 0.5, args.test_points)
epsilon_consts = np.fromiter((np.random.uniform(0, (e-1e-3) / args.train_iters)
                              for e in epsilons),
                             dtype=float)
epsilons[is_softmax] = np.nan
epsilon_consts[is_softmax] = np.nan

um_names = ('QL', 'FAQL', 'RUQL')
um_maxlen = max(len(m) for m in um_names)
um_probs = (0, 1, 0)
update_methods = np.random.choice(um_names, args.test_points, p=um_probs)

faql_betas = np.random.uniform(0, 1, args.test_points)
faql_betas[update_methods != 'FAQL'] = np.nan
faql_beta_exps = np.random.uniform(0, 1, args.test_points)
faql_beta_exps *= (1 - alpha_exps)
faql_beta_exps[update_methods != 'FAQL'] = np.nan

average_rewards = np.empty((args.test_points, args.avgcount))
point_avg_tasks = []
for tp in range(args.test_points):
    alpha = qlearn.hyp_decay(start=alphas[tp], exp=alpha_exps[tp])

    if is_softmax[tp]:
        tau = qlearn.lin_decay(taus[tp], tau_consts[tp], stop=1e-3)
        explore_policy = qlearn.SoftmaxPolicy(tau)
    else:
        epsilon = qlearn.lin_decay(epsilons[tp], epsilon_consts[tp], stop=1e-3)
        explore_policy = qlearn.EpsilonGreedyPolicy(epsilon)

    if update_methods[tp] == 'FAQL':
        faql_beta = qlearn.hyp_decay(faql_betas[tp], faql_beta_exps[tp])
    else:
        faql_beta = None

    ql = qlearn.IndepQLearners(multigrid, alpha=alpha, gamma=gammas[tp],
                               explore_policy=explore_policy,
                               update_method=update_methods[tp],
                               faql_beta=faql_beta)

    train = run_single_training.si(ql.pack(), train_iters=args.train_iters)
    test = run_single_test.s(test_iters=args.test_iters)
    point_avg_tasks.append([chain(train, test) for _ in range(args.avgcount)])

point_avg_tasks = np.array(point_avg_tasks, dtype=object)


def show_avgr(task_id, value):
    (avgr, _) = value
    logger.debug("Got an avgr of {:f}".format(avgr))


flat_tasks = np.reshape(point_avg_tasks, (-1,))
flat_results = utils.sched.retry_group(flat_tasks,
                                       callback=show_avgr,
                                       timeout=4*3600)
flat_results = [v for _, v in flat_results]
flat_results = [avgr for avgr, _ in flat_results]
# results are 2-ples
average_rewards = np.reshape(flat_results, point_avg_tasks.shape)

with h5py.File(args.output, 'w') as f:
    f.attrs['git_sha1'] = hrl.GIT_SHA1

    g = f.create_group('random_grid_data')
    g.attrs['params'] = utils.pack_objects(args=args,
                                           grid=multigrid)

    utils.pack.np2hdfds(g,
                        alphas=alphas,
                        alpha_exps=alpha_exps,
                        gammas=gammas,
                        is_softmax=is_softmax,
                        taus=taus,
                        tau_consts=tau_consts,
                        epsilons=epsilons,
                        epsilon_consts=epsilon_consts,
                        update_methods=np.asarray(update_methods,
                                                  dtype=('S', um_maxlen)),
                        faql_betas=faql_betas,
                        faql_beta_exps=faql_beta_exps,
                        average_rewards=average_rewards)
if args.upload:
    key = utils.aws_upload_data(args.output, 'random_multi_qlearn')
    logger.info("Uploaded with key {!s}".format(key))
else:
    logger.warning("Running with --no-upload: the results were not uploaded")
