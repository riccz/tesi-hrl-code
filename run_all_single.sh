#!/bin/bash

set -euo pipefail
set -x

waitpid=${1-}

if [ -n "$waitpid" ]; then
    while [ -e "/proc/${waitpid}" ]; do
        sleep 30
    done
fi

# git checkout data_ruql
python3 single_agent.py --test-iters=10000 --total-training=2000000 --avgcount=200 --test-points=24 --method=RUQL

# git checkout data_ql
python3 single_agent.py --test-iters=10000 --total-training=2000000 --avgcount=200 --test-points=24 --method=QL

# git checkout data_faql
python3 single_agent.py --test-iters=10000 --total-training=2000000 --avgcount=200 --test-points=24 --method=FAQL

# git checkout data_la
python3 single_lookahead.py --avgcount=50 --test-iters=10000

# git checkout data_rw
python3 random_walk.py --avgcount=50 --test-iters=10000
