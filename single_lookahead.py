import argparse
import logging
import warnings

import numpy as np

from hrl import utils
from hrl.common import grid, output_opts_parser
from hrl.lookahead import LookaheadLearner
from hrl_app import run_single_test
import hrl

with warnings.catch_warnings():
    warnings.simplefilter("ignore", FutureWarning)
    import h5py

logger = logging.getLogger(__name__)

logger.info("Running with commit {!s}".format(hrl.GIT_SHA1))
parser_descr = 'Build an average reward plot for single-agent lookahead'
parser = argparse.ArgumentParser(description=parser_descr,
                                 allow_abbrev=False,
                                 parents=[output_opts_parser])
parser.add_argument("--avgcount", help="Number of runs to average",
                    type=int, default=50)
parser.add_argument("--test-iters", help="Test iterations",
                    type=int, default=10000)
parser.add_argument("--gamma", help="gamma",
                    type=float, default=0.6)
parser.add_argument("--la-steps", help="lookahead steps",
                    type=int, default=2)
parser.add_argument("--cache-size", help="lookahead Q cache size",
                    type=int, default=0)
args = parser.parse_args()

base_la = LookaheadLearner(grid, gamma=args.gamma, la_steps=args.la_steps,
                           cache_size=args.cache_size)
base_packed = base_la.pack()
# Keep one history
test0 = run_single_test.si(base_packed, args.test_iters,
                           hist_label='hist', measure_time=True)
test = run_single_test.si(base_packed, args.test_iters, measure_time=True)
alltests = [test0] + [test for _ in range(1, args.avgcount)]
logger.debug("Testing")
test_results = utils.sched.retry_group(alltests)
test_results = [v for _, v in test_results]
logger.debug("Testing done")

average_rewards = np.array([r['avg_reward'] for r in test_results])
average_test_times = np.array([r['avg_test_time'] for r in test_results])
stddev_test_times = np.array([r['std_test_time'] for r in test_results])

histkey = test_results[0]['history_key']

with h5py.File(args.output, 'w') as h5f:
    h5f.attrs['git_sha1'] = hrl.GIT_SHA1

    g = h5f.create_group('plot_data')
    g.attrs['params'] = utils.pack_objects(
        args=args,
        average_count=args.avgcount,
        grid=grid,
        learner=base_la,
        test_iterations=args.test_iters,
    )

    utils.pack.np2hdfds(
        g,
        avg_rewards=average_rewards,
        avg_test_times=average_test_times,
        std_test_times=stddev_test_times,
    )

    g_hist = h5f.create_group('hist')
    tmp_hist = utils.aws.aws_fetch_data(histkey)
    utils.pack.insert_hdf(tmp_hist, g_hist)
    tmp_hist.close()

if args.upload:
    key = utils.aws_upload_data(args.output, 'single_lookahead')
    logger.info("Uploaded with key {!s}".format(key))
else:
    logger.warning("Running with --no-upload:"
                   " the results were not uploaded")
