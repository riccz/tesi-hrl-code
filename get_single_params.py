import copy
import logging

import matplotlib.pyplot as plt
import numpy as np

from hrl import utils

logger = logging.getLogger(__name__)
plt.close()

good_commits = [
    '7002e420c094001cf23daa0f550ce83992585fc2',
    'b6c8f04ce99f351131b7ccdb45d26817f1f95db0',
]
files = utils.aws.fetch_data_by_commit('random_single_qlearn', good_commits)

alphas = []
alpha_exps = []
gammas = []
is_softmax = []
taus = []
tau_consts = []
epsilons = []
epsilon_consts = []
update_methods = []
faql_betas = []
faql_beta_exps = []
average_rewards = []

args = None
for f in files.values():
    g = f['random_grid_data']
    params = utils.unpack_objects(g.attrs['params'])
    if args is None:
        args = params['args'].__dict__
    else:
        args_nopoints = copy.deepcopy(args)
        del args_nopoints['test_points']
        newargs = copy.deepcopy(params['args'].__dict__)
        del newargs['test_points']
        if args_nopoints != newargs:
            logger.warning("Mismatching arguments")
            continue

    alphas += g['alphas']
    alpha_exps += g['alpha_exps']
    gammas += g['gammas']
    is_softmax += g['is_softmax']
    taus += g['taus']
    tau_consts += g['tau_consts']
    epsilons += g['epsilons']
    epsilon_consts += g['epsilon_consts']
    update_methods += [um.decode() for um in g['update_methods']]
    faql_betas += g['faql_betas']
    faql_beta_exps += g['faql_beta_exps']
    average_rewards += g['average_rewards']

    f.close()

alphas = np.asarray(alphas)
alpha_exps = np.asarray(alpha_exps)
gammas = np.asarray(gammas)
is_softmax = np.asarray(is_softmax)
taus = np.asarray(taus)
tau_consts = np.asarray(tau_consts)
epsilons = np.asarray(epsilons)
epsilon_consts = np.asarray(epsilon_consts)
update_methods = np.asarray(update_methods)
faql_betas = np.asarray(faql_betas)
faql_beta_exps = np.asarray(faql_beta_exps)
average_rewards = np.asarray(average_rewards)

avgavg_rewards = np.mean(average_rewards, axis=1)
stdavg_rewards = np.std(average_rewards, axis=1)

low_ci_rewards = avgavg_rewards - 1.96 * stdavg_rewards
high_ci_rewards = avgavg_rewards + 1.96 * stdavg_rewards

sorted_i = np.argsort(low_ci_rewards)

for um in sorted(set(update_methods)):
    um_sorted_i = np.where(update_methods[sorted_i] == um)
    um_sorted_i = sorted_i[um_sorted_i]

    max_avgr = avgavg_rewards[um_sorted_i][-1]

    max_alpha = alphas[um_sorted_i][-1]
    max_alpha_exp = alpha_exps[um_sorted_i][-1]
    max_gamma = gammas[um_sorted_i][-1]
    max_is_softmax = is_softmax[um_sorted_i][-1]
    max_tau = taus[um_sorted_i][-1]
    max_tau_const = tau_consts[um_sorted_i][-1]
    max_epsilon = epsilons[um_sorted_i][-1]
    max_epsilon_const = epsilon_consts[um_sorted_i][-1]
    max_faql_beta = faql_betas[um_sorted_i][-1]
    max_faql_beta_exp = faql_beta_exps[um_sorted_i][-1]

    print("Update method {!s}:".format(um))
    print("  Max avg. reward is {:.3f}".format(max_avgr))
    print("  with 95% CI bounds [{:.3f},"
          " {:.3f}]".format(low_ci_rewards[um_sorted_i][-1],
                            high_ci_rewards[um_sorted_i][-1]))
    print("  alpha = {:.3f}, exp = {:3f}".format(max_alpha, max_alpha_exp))
    print("  gamma = {:.3f}".format(max_gamma))
    if max_is_softmax:
        print("  softmax with tau = {:.3f}".format(max_tau))
        print("    tau_const = {:e}".format(max_tau_const))
    else:
        print("  epsilon-greedy with epsilon = {:.3f}".format(max_epsilon))
        print("    epsilon_const = {:e}".format(max_epsilon_const))
    if um == 'FAQL':
        print("  faql_beta = {:.3f}, exp = {:.3f}".format(max_faql_beta,
                                                          max_faql_beta_exp))

    plt.figure()
    plt.boxplot(average_rewards[um_sorted_i[-1], :],
                whis=[5, 95], sym='', showmeans=True)


plt.figure()
plt.hist(avgavg_rewards)

plt.show()
