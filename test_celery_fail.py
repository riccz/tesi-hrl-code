import random
import time
import threading

from celery import Celery, group, chain
from celery.result import AsyncResult
from celery.app.task import Task

from hrl.utils.sched import retry_group, retry_task

app = Celery('test_celery_fail', config_source='celery_config')
app.conf.task_default_queue = 'test_queue'


class MyTask(Task):
    def on_success(self, retval, task_id, args, kwargs):
        print("Called on_success")
        super().on_success(retval, task_id, args, kwargs)

    def on_failure(self, exc, task_id, args, kwargs, einfo):
        print("Called on_failure")
        super().on_failure(exc, task_id, args, kwargs, einfo)

        
@app.task(acks_late=True, base=MyTask)
def can_fail(arg):
    time.sleep(random.random() * 30)
    if random.random() < 0.33:
        raise RuntimeError("FAIL!!")
    else:
        return arg

    
@app.task(acks_late=True)
def plusone(n):
    time.sleep(random.random() * 4)
    return n + 1


if __name__ == '__main__':
    sigs = [can_fail.si(n) for n in range(100)]

    def callback(task_id, value):
        print("Result for {!s}: {!s}".format(task_id, value))

    out = retry_group(sigs, callback=callback, timeout=15)

    assert([v for _, v in out] == list(range(len(sigs))))
