import numpy as np
from scipy.special import binom
import matplotlib.pyplot as plt

def binprob(N, p, T, k):
    bin_p = 1 - (1-p)**T
    return binom(N, k) * bin_p**k * (1-bin_p)**(N-k)

def simulate(N, p, T):
    Xs = np.zeros(N, dtype=bool)
    for _ in range(T):
        for i in range(N):
            if not Xs[i]:
                Xs[i] = np.random.random() < p
    return sum(Xs)

def avgprob(N, p, T, k, simcount=1000):
    counts = np.empty(simcount, dtype=int)
    for i in range(simcount):
        counts[i] = simulate(N, p, T)

    which = counts == k
    return (np.mean(which), np.std(which))

p = 1/3
T = 5
N = 25

plt.figure("Multistep Binomial test")
ks = range(N+1)
binprobs = np.array([binprob(N, p, T, k) for k in ks])
avgprobs = np.array([avgprob(N, p, T, k, simcount=10000) for k in ks])

assert(round(sum(binprobs), 6) == 1)
try:
    assert(round(sum(avgprobs[:,0]), 2) == 1)
except AssertionError:
    print("Sum of avgprobs = {:f}".format(sum(avgprobs[:,0])))

plt.plot(ks, binprobs)
(line,) = plt.plot(ks, avgprobs[:, 0])
plt.plot(ks, avgprobs[:, 0] + 1.96 * avgprobs[:, 1], color=line.get_color(), linestyle='dotted')
plt.plot(ks, avgprobs[:, 0] - 1.96 * avgprobs[:, 1], color=line.get_color(), linestyle='dotted')

plt.show()