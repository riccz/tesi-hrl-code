import copy
import itertools
import operator
import pickle

from matplotlib import animation
import matplotlib.pyplot as plt
import numpy as np

from hrl.utils import load_vars, avg_over_time, flatten_dicts

plt.close()

data = load_vars('multiagent.pickle.xz')
hist = data['hist']
random_hist = data['random_hist']
# policy = data['policy']
grid = data['grid']
# ql = data['ql']

plt.figure()
rewards = flatten_dicts(map(operator.itemgetter(2), hist))
for ag in sorted(rewards.keys()):
    avg_r = np.fromiter(avg_over_time(rewards[ag]), dtype=float)
    plt.plot(range(1, len(avg_r)+1), avg_r, label="IQL " + ag)

rand_rewards = flatten_dicts(map(operator.itemgetter(2), random_hist))
for ag in sorted(rand_rewards.keys()):
    avg_r = np.fromiter(avg_over_time(rand_rewards[ag]), dtype=float)
    plt.plot(range(1, len(avg_r)+1), avg_r, label="RW " + ag, linestyle='dashed')

plt.grid()
plt.legend()
plt.savefig('avg_reward.pdf', format='pdf')

pathfig = plt.figure()
pathax = plt.axes()
paths = list(map(operator.itemgetter(0), hist))
all_xs = np.fromiter(grid.state_space.sub('ag0').sub('pos').sub('x'), float)
all_ys = np.fromiter(grid.state_space.sub('ag0').sub('pos').sub('y'), float)
(plot_x, plot_y) = np.meshgrid(all_xs, all_ys)
rfunc = copy.deepcopy(grid._reward)
time_offset = len(paths) - 1000
def pathfig_anim(i):
    pathax.clear()
    pathax.set_xlim(grid.state_space.x_min, grid.state_space.x_max)
    pathax.set_ylim(grid.state_space.y_min, grid.state_space.y_max)
    pathax.set_prop_cycle(color=('red', 'green', 'magenta'))

    if i == 0:
        for j in range(time_offset):
            if j % data['itercount'] == 0:
                rfunc.reset()
            rfunc.update(paths[j])

    agent_lines = []
    agent_points = []
    for ag in grid.agent_keys:
        paths_head = map(operator.itemgetter(ag), itertools.islice(paths,
                                                                   time_offset,
                                                                   time_offset + i+1))
        ph1, ph2 = itertools.tee(paths_head)
        xs = np.fromiter((s['pos']['x'] for s in ph1), dtype=float)
        ys = np.fromiter((s['pos']['y'] for s in ph2), dtype=float)
        (line,) = pathax.plot(xs, ys, alpha=0.33, zorder=2)
        endpoint = pathax.scatter(xs[-1], ys[-1], c=line.get_color(), zorder=3)
        agent_lines.append(line)
        agent_points.append(endpoint)

    rfunc.update(paths[time_offset + i])
    contour = pathax.contourf(plot_x, plot_y, rfunc._reward_matrix.transpose(), 128, zorder=1)
    return (contour,) + tuple(agent_lines + agent_points)
pathanim = animation.FuncAnimation(pathfig, pathfig_anim,
                                   frames=len(paths)-time_offset, interval=250)

plt.show()
