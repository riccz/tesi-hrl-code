import argparse
import copy
import functools
import logging
import random
import warnings

from celery import Celery
from celery.exceptions import TimeoutError
from billiard.exceptions import WorkerLostError
import numpy as np

from hrl import klspi
from hrl import utils
from hrl import common
import hrl

with warnings.catch_warnings():
    warnings.simplefilter("ignore", FutureWarning)
    import h5py

logger = logging.getLogger(__name__)
logger.info("Running with commit {!s}".format(hrl.GIT_SHA1))
app = Celery('kernel_random_grid_search', config_source='celery_config')


def collect_async_results(func, results, timeout):
    waiting = list(enumerate(results))
    out = [None for _ in range(len(waiting))]
    while len(waiting) > 0:
        new_waiting = []
        for i, r in waiting:
            try:
                x = r.get(timeout)
            except (TimeoutError, WorkerLostError):
                new_waiting.append((i, r))
                continue
            out[i] = func(x)
        waiting = new_waiting
    return out


@app.task(acks_late=True)
def run_test(learner, test_iters):
    random.seed()
    np.random.seed()

    logger.debug("Starting test")
    avgr = learner.test(test_iters, reset=True)
    logger.debug("Test finished")
    return avgr


@app.task(acks_late=True)
def run_training(learner, train_samples, train_iters):
    random.seed()
    np.random.seed()

    learner.mdp.reset()
    for _ in range(train_iters):
        learner.learn(steps=train_samples)
    return learner


if __name__ == '__main__':
    parser_descr = 'Kernel random grid search'
    parser = argparse.ArgumentParser(description=parser_descr,
                                     allow_abbrev=False)

    parser.add_argument("--no-upload", help="Don't upload the results",
                        action='store_false', dest='upload')
    parser.add_argument("--upload", help="Upload the results",
                        action='store_true', dest='upload')
    parser.set_defaults(upload=True)
    parser.add_argument("--output", help="Output file",
                        type=str, default='kernel_random_grid_search.hdf5')
    parser.add_argument("--queue", type=str,
                        default=app.conf.task_default_queue)

    parser.add_argument("--count", type=int, default=1)
    parser.add_argument("--avgcount", type=int, default=1)
    parser.add_argument("--train-iters", type=int, default=10)
    parser.add_argument("--train-samples", type=int, default=250)
    parser.add_argument("--test-iters", type=int, default=10000)

    args = parser.parse_args()

    gammas = np.random.uniform(0.01, 0.95, args.count)
    epsilons = np.random.uniform(0.01, 1, args.count)
    ald_threshs = 10 ** np.random.uniform(-10, -1, args.count)
    lambda_hs = np.random.exponential(1, args.count)
    lambda_Qs = np.random.exponential(1, args.count)
    sigmas = np.random.uniform(0.1, 10, args.count)
    methods = ('KLSPI', 'RegLSTD')
    meth_maxlen = max(len(m) for m in methods)
    chosen_methods = np.random.choice(methods, args.count, p=(0.3, 0.7))

    ald_threshs[chosen_methods != 'KLSPI'] = np.nan
    lambda_hs[chosen_methods != 'RegLSTD'] = np.nan
    lambda_Qs[chosen_methods != 'RegLSTD'] = np.nan

    trained_res = np.empty((args.count, args.avgcount), dtype=object)
    for i in range(args.count):
        for j in range(args.avgcount):
            kern = functools.partial(common.kern, sigma=sigmas[i])
            if chosen_methods[i] == 'KLSPI':
                learner = klspi.KLSPILearner(common.grid,
                                             kern=kern,
                                             gamma=gammas[i],
                                             epsilon=epsilons[i],
                                             ald_thresh=ald_threshs[i])
            elif chosen_methods[i] == 'RegLSTD':
                learner = klspi.RegLSTDLearner(common.grid,
                                               kern=kern,
                                               gamma=gammas[i],
                                               epsilon=epsilons[i],
                                               lambda_h=lambda_hs[i],
                                               lambda_Q=lambda_Qs[i])
            else:
                raise RuntimeError()

            trained_res[i, j] = run_training.apply_async((learner,
                                                          args.train_samples,
                                                          args.train_iters),
                                                         queue=args.queue)

    avgr_res = collect_async_results(
        lambda tr: run_test.apply_async((tr, args.test_iters),
                                        queue=args.queue),
        trained_res.flat,
        timeout=30
    )

    def log_rewards(r):
        logger.debug("avg. reward = {:f}".format(r))
        return r

    average_rewards = collect_async_results(log_rewards, avgr_res,
                                            timeout=30)
    average_rewards = np.array(average_rewards).reshape(trained_res.shape)

    with h5py.File(args.output, 'w') as f:
        f.attrs['git_sha1'] = hrl.GIT_SHA1

        f.attrs['params'] = utils.pack_objects(
            train_samples=args.train_samples,
            train_iters=args.train_iters,
            test_iters=args.test_iters,
            count=args.count,
            avgcount=args.avgcount
        )

        avgrs_ds = f.create_dataset('avg_rewards', average_rewards.shape,
                                    dtype=float,
                                    compression='gzip')
        avgrs_ds[:] = average_rewards

    if args.upload:
        key = utils.aws_upload_data(args.output, 'random_kernel')
        logger.info("Uploaded with key {!s}".format(key))
    else:
        logger.warning("Running with --no-upload: the results were not uploaded")
