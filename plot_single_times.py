import argparse
import copy
import itertools
import math
import operator
import pathlib
import warnings

from matplotlib import animation
from matplotlib import pyplot as plt
import numpy as np

from hrl import utils
from hrl.common import algo_order, algo_color

with warnings.catch_warnings():
    warnings.simplefilter("ignore", FutureWarning)
    import h5py


def boxplot_times(title, ylabel, split_axis=False, **kwargs):
    data = []
    labels = []
    sorted_kwargs = sorted(kwargs.items(), key=lambda kv: algo_order[kv[0]])
    for label, key in sorted_kwargs:
        with utils.aws.aws_fetch_data(key) as f:
            times = np.asarray(f['time_data']['times'])
        data.append(times.reshape((-1,)) * 1000)  # [ms]
        labels.append(label)

    if split_axis:
        (fig, axes) = plt.subplots(2, 1, sharex=True)
    else:
        (fig, ax) = plt.subplots()
        axes = (ax,)
    fig.canvas.set_window_title(title)

    for ax in axes:
        bp = ax.boxplot(data, notch=False, labels=labels, showmeans=True,
                        whis=[1, 99],
                        flierprops={'marker': '+', 'visible': False},
                        meanprops={'marker': 'o'})
        for j, m in enumerate(bp['means']):
            mean_data = m.get_data()
            assert(len(mean_data) == 2)
            assert(len(mean_data[1]) == 1)
            print("{!s} mean = {:f} [ms]".format(labels[j], mean_data[1][0]))
            c = algo_color[labels[j]]
            m.set_markerfacecolor(c)
            m.set_markeredgecolor(c)
        for j, m in enumerate(bp['medians']):
            median_data = m.get_data()
            assert(len(median_data) == 2)
            assert(all(y == median_data[1][0] for y in median_data[1]))
            print("{!s} median = {:f} [ms]".format(labels[j], median_data[1][0]))
            c = algo_color[labels[j]]
            m.set_color(c)
        for j, w in enumerate(bp['whiskers']):
            whisk_data = w.get_data()
            assert(len(whisk_data) == 2)
            assert(all(x == whisk_data[0][0] for x in whisk_data[0]))
            print("{!s} whisk0 = {:f} [ms]".format(labels[math.floor(j/2)], whisk_data[1][0]))
            print("{!s} whisk1 = {:f} [ms]".format(labels[math.floor(j/2)], whisk_data[1][1]))
            c = algo_color[labels[math.floor(j/2)]]
            w.set_color(c)
        for j, cap in enumerate(bp['caps']):
            c = algo_color[labels[math.floor(j/2)]]
            cap.set_color(c)
        for j, b in enumerate(bp['boxes']):
            c = algo_color[labels[j]]
            b.set_color(c)

    axes[0].set_ylabel("{!s} [ms]".format(ylabel))
    axes[0].grid(True)

    if split_axis:
        (ax1, ax2) = axes
        ax1.spines['bottom'].set_visible(False)
        ax2.spines['top'].set_visible(False)
        ax1.xaxis.tick_top()
        ax1.tick_params(labeltop='off')  # don't put tick labels at the top
        ax2.xaxis.tick_bottom()

    return fig

# All on c5.xlarge, steps=10000, avgcount=50
train_faql_key = 'data/single_timing_faql/91785bc8/IzihOD9J9vde3Q45'
train_ql_key = 'data/single_timing_ql/802335b2/gyXBwgWEOShjA5GV'
train_ruql_key = 'data/single_timing_ruql/91785bc8/bMTEnDSXZyqcAutG'

# All on c5.xlarge, steps=10000, avgcount=50, train_steps=100000
test_faql_key = 'data/single_timing_faql/91785bc8/a22Hl7PeWpQeIFLq'
test_la_key = 'data/single_timing_la/802335b2/d48qoWgkcCl1ufvx'
test_ql_key = 'data/single_timing_ql/802335b2/d7Adg6LHRxpiZKIG'
test_ruql_key = 'data/single_timing_ruql/802335b2/wcRtjcvMI9sGguLo'
test_rw_key = 'data/single_timing_rw/802335b2/hqnOIwHtqcVh3tww'

train_fig = boxplot_times(title="Training times",
                               ylabel="Training step time",
                               QL=train_ql_key,
                               FAQL=train_faql_key,
                               RUQL=train_ruql_key)
train_fig.axes[0].set_ylim(0, 2.25)
train_fig.savefig('plots/single/train_times.pdf', format='pdf')

test_fig = boxplot_times(title="Test times",
                              ylabel="Test step time",
                              RW=test_rw_key,
                              LA=test_la_key,
                              QL=test_ql_key,
                              FAQL=test_faql_key,
                              RUQL=test_ruql_key)
test_fig.axes[0].set_yscale('log')
test_fig.axes[0].set_ylim(0.1, 1000)
test_fig.savefig('plots/single/test_times_log.pdf', format='pdf')

plt.show()
