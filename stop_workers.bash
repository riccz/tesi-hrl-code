#!/bin/bash

set -euo pipefail

qstat -j 'celery_worker' \
    | grep 'job_number' \
    | grep -oE '[0-9]+' \
    | xargs -n1 qdel
