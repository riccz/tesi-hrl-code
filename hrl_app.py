import itertools
import logging
import os
import operator
import random
import tempfile
import warnings

from celery import Celery
import numpy as np

from hrl import utils
from hrl.abstr import Learner
from hrl.utils.pack import unpack_history, pack_history, pack_objects, np2hdfds
import hrl

with warnings.catch_warnings():
    warnings.simplefilter("ignore", FutureWarning)
    import h5py

logger = logging.getLogger(__name__)
logger.info("Running with commit {!s}".format(hrl.GIT_SHA1))


def write_history(label, grid, history):
    hist_label = "{!s}_{!s}".format(label, utils.random_string(16))
    histfile = "/tmp/{!s}".format(hist_label)
    histkey = "tmp/{!s}".format(hist_label)
    with h5py.File(histfile, 'w') as h5f:
        (states, actions,
         rewards, new_states) = utils.pack_history(grid, history)
        shape = (len(history),)
        s_ds = h5f.create_dataset('states', shape, dtype=states.dtype,
                                  compression='gzip')
        s_ds[:] = states
        a_ds = h5f.create_dataset('actions', shape, dtype=actions.dtype,
                                  compression='gzip')
        a_ds[:] = actions
        r_ds = h5f.create_dataset('rewards', shape, dtype=rewards.dtype,
                                  compression='gzip')
        r_ds[:] = rewards
        ns_ds = h5f.create_dataset('new_states', shape, dtype=new_states.dtype,
                                   compression='gzip')
        ns_ds[:] = new_states
    utils.aws.upload_file(histfile, 'hrl.zanol.eu', histkey,
                          delete_source=True)
    return histkey


hrl_app = Celery('hrl_app', config_source='celery_config')


@hrl_app.task(acks_late=True)
def time_training(packed_learner, train_iters, return_trained=False):
    random.seed()
    np.random.seed()

    if isinstance(packed_learner, dict):
        packed_learner = packed_learner['learner']

    learner = Learner.unpack(packed_learner)
    learner.train_time_counter.reset()
    learner.train_time_counter.keep_all = True
    learner.learn(train_iters)
    if return_trained:
        return {
            'learner': learner.pack(),
            'time_samples': [s for s, _ in learner.train_time_counter.samples]
        }
    else:
        return {
            'time_samples': [s for s, _ in learner.train_time_counter.samples]
        }


@hrl_app.task(acks_late=True)
def time_test(packed_learner, test_iters):
    random.seed()
    np.random.seed()

    if isinstance(packed_learner, dict):
        packed_learner = packed_learner['learner']

    learner = Learner.unpack(packed_learner)
    learner.test_time_counter.reset()
    learner.test_time_counter.keep_all = True
    learner.test(test_iters, reset=True)
    return {
        'time_samples': [s for s, _ in learner.test_time_counter.samples]
    }


@hrl_app.task(acks_late=True)
def run_single_training(packed_learner, train_iters, measure_time=False):
    random.seed()
    np.random.seed()

    if isinstance(packed_learner, dict):
        packed_learner = packed_learner['learner']

    learner = Learner.unpack(packed_learner)
    learner.train_time_counter.reset()
    learner.learn(train_iters)
    if measure_time:
        return {
            'learner': learner.pack(),
            'avg_train_time': learner.train_time_counter.average,
            'std_train_time': learner.train_time_counter.std_dev
        }
    else:
        return learner.pack()


@hrl_app.task(acks_late=True)
def run_single_test(packed_learner, test_iters,
                    hist_label=None,
                    measure_time=False):
    random.seed()
    np.random.seed()

    learner = Learner.unpack(packed_learner)

    if hist_label is not None:
        history = []

        def save_history(s, a, r, ns):
            history.append((s, a, r, ns))
    else:
        save_history = None

    learner.test_time_counter.reset()
    avgr = learner.test(test_iters, after=save_history, reset=True)

    if hist_label is not None:
        histkey = write_history(hist_label, learner.mdp, history)
    else:
        histkey = None

    if measure_time:
        return {
            'avg_reward': avgr,
            'history_key': histkey,
            'avg_test_time': learner.test_time_counter.average,
            'std_test_time': learner.test_time_counter.std_dev
        }
    else:
        return (avgr, histkey)


@hrl_app.task(acks_late=True, max_retries=None, track_started=True)
def test_and_time(accum_data, test_iters, hist_label=None):
    random.seed()
    np.random.seed()

    if hist_label is not None:
        history = []

        def save_history(s, a, r, ns):
            history.append((s, a, r, ns))
    else:
        save_history = None

    learner = Learner.unpack(accum_data['learner'])
    avgr = learner.test(test_iters, reset=False,
                        after=save_history, real_mdp=True)

    if hist_label is not None:
        histkey = write_history(hist_label, learner.mdp, history)
    else:
        histkey = None

    if 'hist_list' in accum_data:
        hist_list = accum_data['hist_list'] + [histkey]
    else:
        hist_list = [histkey]

    if 'avgr' in accum_data:
        total_iters = test_iters + accum_data['tot_iters']
        total_avgr = (avgr * test_iters +
                      accum_data['avgr'] * accum_data['tot_iters'])
        total_avgr /= total_iters
    else:
        total_avgr = avgr
        total_iters = test_iters

    return {
        'avgr': total_avgr,
        'hist_list': hist_list,
        'learner': learner.pack(),
        'tot_iters': total_iters,
    }


@hrl_app.task(acks_late=True, max_retries=None, track_started=True)
def aggregate_test_and_time(*results, upload_label='test_time'):
    if type(results) is tuple and len(results) == 1:
        results = results[0]

    out = tempfile.NamedTemporaryFile(delete=False)
    out.close()
    out = h5py.File(out.name, 'w')
    out.attrs['git_sha1'] = hrl.GIT_SHA1

    # Same test iterations
    test_iters = results[0]['tot_iters']
    assert(all(r['tot_iters'] == test_iters for r in results[1:]))

    learners = [Learner.unpack(r['learner']) for r in results]

    # Collect times
    test_times_lists = [lr.test_time_counter.samples for lr in learners]
    test_times_merged = itertools.chain.from_iterable(test_times_lists)
    test_times_noweight = map(operator.itemgetter(0), test_times_merged)
    test_times = np.fromiter(test_times_noweight, dtype=float)

    # Reset to compare
    for lr in learners:
        lr.mdp.reset()
        lr.reset()

    # Same MDP and base learner
    mdp = learners[0].mdp
    # assert(all(lr.mdp == mdp for lr in learners[1:]))
    learner = learners[0]
    # assert(all(lr == learner for lr in learners[1:]))

    # Time samples can be too many
    learner.test_time_counter.keep_all = False
    learner.test_time_counter.samples = []
    gplot = out.create_group('plot_data')
    gplot.attrs['params'] = pack_objects(
        average_count=len(results),
        grid=mdp,
        learner=learner,
        test_iterations=test_iters,
    )

    # Collect avg rewards
    avg_rewards = np.fromiter((r['avgr'] for r in results), dtype=float)

    np2hdfds(gplot, test_times=test_times, avg_rewards=avg_rewards)

    # Collect histories
    for j, r in enumerate(results):
        if all(hk is None for hk in r['hist_list']):
            continue

        histfiles = [utils.aws.aws_fetch_data(hk) for hk in r['hist_list']]
        full_hist = []
        for hf in histfiles:
            full_hist += unpack_history(mdp, **dict(hf.items()))
            hf_fname = hf.filename
            hf.close()
            os.unlink(hf_fname)

        if j == 0:
            ghist = out.create_group('hist')
        else:
            ghist = out.create_group('hist_{:d}'.format(j))
        (states, actions, rewards, new_states) = pack_history(mdp, full_hist)
        np2hdfds(ghist,
                 states=states,
                 actions=actions,
                 rewards=rewards,
                 new_states=new_states)
    out_fname = out.filename
    out.close()
    key = utils.aws.aws_upload_data(out_fname, upload_label,
                                    delete_source=True)
    logger.info("Uploaded with key {!s}".format(key))
    return key
