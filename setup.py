from setuptools import setup, find_packages
from os import path

here = path.abspath(path.dirname(__file__))

setup(
    name='tesi-hrl-code',
    version='0.0.1',
    description='Code for the HRL thesis',
    # long_description=
    url='https://gitlab.com/riccz/tesi-hrl-code',
    author='Riccardo Zanol',
    author_email='ricc@zanol.eu',
    packages=find_packages(exclude=['tests']),
    install_requires=[
        'Pillow>=3.4',
        'boto3>=1.4',
        'botocore>=1.4',
        'cachetools>=1.1',
        'celery[redis]>=4.2',
        'h5py>=2.7',
        'matplotlib>=2.0',
        'numpy>=1.12',
        'scipy>=1.0',
    ],
    python_requires='>=3.5.3',
    test_suite='tests._discover_tests',
    # entry_points={
    #     'console_scripts': [
    #         'sample=sample:main',
    #     ],
    # },
)
