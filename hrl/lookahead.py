import contextlib
import copy
import functools
import itertools
import logging
import math
import operator
import time

import cachetools
import numpy as np

from . import utils
from .abstr import Learner
from .grid import GridMDP
from .utils.stats import measure_time as time_counter

logger = logging.getLogger(__name__)


def log_high(msg, delta, avg, std, high_thresh=3.3):
    logger.debug("{!s}: {:e} s,"
                 " expected <= {:e} s".format(msg, delta,
                                              avg + high_thresh * std))


def group_R_states(tr_probs, S):
    def splitter(nsp):
        (ns, _) = nsp
        nR = ns['R']
        xi = S.x.index(ns['ag0']['x'])
        yi = S.y.index(ns['ag0']['y'])
        return nR[xi, yi]

    (nsp1, nsp0) = utils.iter.partition(tr_probs, splitter)
    nsp1 = utils.iter.unstar(nsp1)
    nsp0 = utils.iter.unstar(nsp0)

    out = []
    if len(nsp1) > 0:
        (ns1, p1) = nsp1
        out.append((next(ns1), sum(p1)))
    if len(nsp0) > 0:
        (ns0, p0) = nsp0
        out.append((next(ns0), sum(p0)))
    return out


class LoggingLRUCache(cachetools.LRUCache):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.hitcount = 0
        self.misscount = 0

    def __getitem__(self, *args, **kwargs):
        try:
            item = super().__getitem__(*args, **kwargs)
            self.hitcount += 1
            return item
        except KeyError as ke:
            self.misscount += 1
            raise ke

    def hit_fraction(self):
        tot = self.hitcount + self.misscount
        if tot != 0:
            return self.hitcount / tot
        else:
            return math.nan

    def clear(self):
        super().clear()
        self.hitcount = 0
        self.misscount = 0


class LookaheadLearner(Learner):
    def __init__(self, mdp, gamma, la_steps,
                 cache_size=2**14, approx=False,
                  keep_time_samples=False):
        super().__init__(keep_time_samples=keep_time_samples)
        assert(la_steps > 0)
        assert(gamma >= 0 and gamma <= 1)

        self.__mdp = mdp
        self.gamma = gamma
        self.la_steps = la_steps
        self.approx = approx

        self._lookahead_Q_cache = LoggingLRUCache(cache_size)
        self.__build_cached_methods()

        self.avg_Q_time = utils.stats.AverageCounter()
        self.avg_prob_time = utils.stats.AverageCounter()
        self.avg_new_s_fanout = utils.stats.AverageCounter()

    def __build_cached_methods(self):
        cache_size = self._lookahead_Q_cache.maxsize
        wrappedfunc = self._lookahead_Q
        if cache_size > 0:
            keyfunc = self._lookahead_Q_cache_key
            cache_decor = cachetools.cached(self._lookahead_Q_cache,
                                            key=keyfunc)
            self._cached_lookahead_Q = cache_decor(wrappedfunc)
        else:
            self._cached_lookahead_Q = wrappedfunc

    def __getstate__(self):
        bak = self.__dict__['_cached_lookahead_Q']
        self.__dict__['_cached_lookahead_Q'] = None
        dictcopy = copy.deepcopy(self.__dict__)
        self.__dict__['_cached_lookahead_Q'] = bak
        return dictcopy

    def __setstate__(self, state):
        self.__dict__.update(state)
        self.__build_cached_methods()

    @property
    def mdp(self):
        return self.__mdp

    def _lookahead_Q_cache_key(self, ag, s, a, max_depth):
        k = (ag,
             self.mdp.state_space.index(s),
             self.mdp.action_space.single.index(a),
             max_depth)
        if isinstance(self.mdp, GridMDP):
            k += tuple(s[ag]['grid']['b'] if 'grid' in s[ag] else None
                       for ag in self.mdp.agent_keys)
        return k

    def _lookahead_Q(self, ag, s, a, max_depth):
        assert(max_depth > 0)

        time_counter_log = functools.partial(log_high,
                                             msg="High prob. generation time")
        with time_counter(self.avg_prob_time, on_high=time_counter_log):
            tr_probs = self.mdp.indep_transition_prob(ag, s, a)
            tr_probs = list(tr_probs)
        self.avg_new_s_fanout.add(len(tr_probs))
        (new_states, new_states_p) = utils.iter.unstar(tr_probs)

        if hasattr(self.mdp, 'average_indep_reward'):
            avg_r = self.mdp.average_indep_reward(ag, s, a)
        else:
            (new_states, tmp_new_states) = itertools.tee(new_states)
            (new_states_p, tmp_new_states_p) = itertools.tee(new_states_p)

            partial_r = functools.partial(self.mdp.indep_reward, ag, s, a)
            rewards = map(partial_r, new_states)
            avg_r = utils.iter.dotproduct(rewards, new_states_p)

            new_states = tmp_new_states
            new_states_p = tmp_new_states_p

        if max_depth - 1 > 0:
            maxQfunc = functools.partial(self._max_Q,
                                         ag, max_depth=max_depth - 1)
            maxQs = map(maxQfunc, new_states)
            avg_maxQ = utils.iter.dotproduct(maxQs, new_states_p)
        else:
            avg_maxQ = 0.0

        return avg_r + self.gamma * avg_maxQ

    def _max_Q(self, ag, s, max_depth):
        actions = self.mdp.valid_actions(s)[ag]
        Q = functools.partial(self._cached_lookahead_Q,
                              ag, s, max_depth=max_depth)
        return max(map(Q, actions))

    def Q(self, agent, state, action):
        def Q_time_log(delta, avg, std):
            logger.debug("Time to compute Q ({:e} s) is high:"
                         " expected <= {:e}".format(delta, avg + 3.3 * std))
            logger.debug("Cache size: {:d}, hit rate:"
                         " {:f}".format(len(self._lookahead_Q_cache),
                                        self.cache_hit_fraction()))
            logger.debug("Tr. prob. avg. fanout:"
                         " {:f}".format(self.avg_new_s_fanout.average))
        with time_counter(self.avg_Q_time, on_high=Q_time_log):
            out = self._cached_lookahead_Q(agent, state, action, self.la_steps)
        return out

    def _valid_joint_actions(self, state):
        actions = self.mdp.valid_actions(state)
        unlab_prod = itertools.product(*actions.values())

        def label_prod(up):
            return dict(zip(actions.keys(), up))

        return map(label_prod, unlab_prod)

    def allQs(self, agent, state):
        actions = self.mdp.valid_actions(state)[agent]
        return map(lambda a: (a, self.Q(agent, state, a)), actions)

    def reset(self):
        self._lookahead_Q_cache.clear()
        self.avg_Q_time.reset()
        self.avg_prob_time.reset()
        self.avg_new_s_fanout.reset()

    def policy(self):
        return self._greedy_policy

    def _greedy_policy(self, state):
        action = dict()
        for ag in self.mdp.agent_keys:
            action[ag] = self._greedy_single_policy(ag, state)
        logger.debug("Chosen one joint action")
        return self.mdp.action_space.make_item(**action)

    def _greedy_single_policy(self, agent, state):
        (actions, Qs) = utils.iter.unstar(self.allQs(agent, state))
        chosen_i = np.fromiter(Qs, dtype=float).argmax()
        a = next(itertools.islice(actions, int(chosen_i), None))
        return a

    def cache_hit_fraction(self):
        return self._lookahead_Q_cache.hit_fraction()
