import math
import random
import warnings

from scipy import optimize
import numpy as np
from numpy.polynomial import polynomial as poly

def all_zeros_concave(f, a, b, args=(),
                      xtol=2e-12, rtol=4*np.finfo(float).eps,
                      maxiter=1000):
    fa = f(a, *args)
    fb = f(b, *args)
    if np.sign(fa) != np.sign(fb):
        x0, res = optimize.brentq(f, a, b, args, xtol, rtol, maxiter,
                                  full_output=True, disp=False)
        if not res.converged: raise RuntimeError("Not converged")
        if res.iterations == maxiter: warnings.warn("Reached maxiter",
                                                    stacklevel=2)
        return [x0]
    elif fa > 0 and fb > 0:
        return []
    else:
        negf = lambda x: -f(x, *args)
        minres = optimize.minimize_scalar(negf, bracket=(a, b), method='brent',
                                          options=dict(xtol=xtol,
                                                       maxiter=maxiter))
        if not minres.success: raise RuntimeError("Max finding failed")

        if np.allclose(minres.fun, 0, rtol=rtol):
            return [minres.x]

        if minres.fun > 0: return []

        x0, res = optimize.brentq(f, a, minres.x, args, xtol, rtol, maxiter,
                                  full_output=True, disp=False)
        if not res.converged: raise RuntimeError("Not converged")
        if res.iterations == maxiter: warnings.warn("Reached maxiter",
                                                    stacklevel=2)
        x1, res = optimize.brentq(f, minres.x, b, args, xtol, rtol, maxiter,
                                  full_output=True, disp=False)
        if not res.converged: raise RuntimeError("Not converged")
        if res.iterations == maxiter: warnings.warn("Reached maxiter",
                                                    stacklevel=2)
        return [x0, x1]

class TremblayBattery:
    def __init__(self, **kwargs):
        self.E0 = kwargs['E0'] # [V]
        self.R = kwargs['R'] # [Ohm]
        self.K = kwargs['K'] # [V]
        self.A = kwargs['A'] # [V]
        self.B = kwargs['B'] / 3600 # [(Ah)^-1] -> [C^-1]
        self.Q = kwargs['Q'] * 3600 # [Ah] -> [C]

        self.used_q = 0 # [C]

        self.minV = kwargs['minV'] # Cutoff V
        self.maxV = self.output_voltage(0) # V_open at full charge

    def reset(self):
        self.used_q = 0

    @property
    def soc(self):
        return 1 - self.used_q / self.Q

    @soc.setter
    def soc(self, value):
        assert(value >= 0 and value <= 1)
        self.used_q = self.Q * (1 - value)

    def output_voltage(self, i_out):
        """Output voltage while drawing current i_out"""
        v_open = (
            self.E0 -
            self.K * self.Q / (self.Q - self.used_q) +
            self.A * np.exp(-self.B * self.used_q)
        )
        return v_open - self.R * i_out
    def draw_charge(self, q):
        q = q * 3600
        next_used_q = self.used_q + q
        if next_used_q > self.Q:
            raise ValueError("Would draw charge over capacity")
        self.used_q = next_used_q

    def draw_current(self, i_out, t):
        self.draw_charge(i_out * t / 3600)

    def draw_power(self, P_out, t):
        """Draw the given power for the given duration.
           Returns the output voltage and current at the start of the timeslot
        """
        i_out = self.output_current(P_out)
        if i_out > self.Q * self.soc / t:
            raise ValueError("Not enough remaining charge")
        v_out = self.output_voltage(i_out)
        self.draw_current(i_out, t)
        return (v_out, i_out)

    def output_current(self, P_out):
        """Find the output current given the power and the current SOC"""
        minI = max(0, P_out / self.maxV)
        maxI = P_out / self.minV

        poly1_a = -self.K * self.Q / (self.Q - self.used_q)
        poly1_b = self.A * np.exp(-self.B * self.used_q)
        poly_coeffs = [
            -P_out,
            self.E0 + poly1_a + poly1_b,
            -self.R
        ]

        roots = poly.polyroots(poly_coeffs)
        roots = [r for r in roots if np.isreal(r) and r >= minI and r <= maxI]
        if len(roots) == 0:
            raise ValueError("No root is real and within bounds")
        assert(len(roots) == 1)
        return roots[0]

    _find_i_out = output_current

class CounterBattery:
    def __init__(self, states, avg_lifetime, alpha=1.0):
        assert(avg_lifetime >= states)

        self.states = states
        self.avg_lifetime = avg_lifetime
        self.alpha = alpha

        if alpha == 1:
            self.ps = np.ones(states) * ((states - 1) / avg_lifetime)
            self.ps[0] = 0
        else:
            self.ps = np.empty(states)
            alphasum = (1 - alpha**(states-1)) / (1 - alpha)
            assert(avg_lifetime >= alphasum)
            self.ps[-1] = alphasum / avg_lifetime
            for i in range(states-1, 0, -1):
                self.ps[i-1] = self.ps[i] / alpha
            self.ps[0] = 0

        self.state = states - 1

    def reset(self):
        self.state = self.states - 1

    def draw(self):
        state = self.state
        assert(state > 0)
        if random.random() < self.ps[state]:
            self.state = state - 1


def make_drone_TB():
    Q = 5.5 # [Ah]
    R = 4.75e-3
    Efull = 16.2
    Eexp = 14.15
    A = Efull - Eexp
    B = 3/4
    Enom = 14
    Qnom = 4.5
    K = (Efull - Enom + A*(math.exp(-B * Qnom) - 1)) * (Q - Qnom) / Qnom
    plot_i = 20*Q
    E0 = Efull + K + R*plot_i - A
    minV = 12

    return TremblayBattery(E0=E0, R=R, K=K, A=A, B=B, Q=Q, minV=minV)
