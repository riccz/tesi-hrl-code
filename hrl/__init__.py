import logging
import logging.config
import os

from .version import git_sha1

here = os.path.abspath(os.path.dirname(__file__))
logging.config.fileConfig(os.path.join(here, os.pardir, 'logging.conf'))
GIT_SHA1 = git_sha1()
