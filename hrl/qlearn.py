import base64
import collections
import copy
import functools
import itertools
import logging
import numbers
import operator
import pathlib
import pickle
import random
import tempfile
import warnings
import zlib

import numpy as np

from . import utils
from .abstr import Learner
from .utils.stats import measure_time

with warnings.catch_warnings():
    warnings.simplefilter("ignore", FutureWarning)
    import h5py

logger = logging.getLogger(__name__)


def _hyp_decay_func(t, start, exp, flat):
    t = max(1, t - flat)
    return start / (t ** exp)


def hyp_decay(start, exp, flat=0):
    assert(start >= 0)
    assert(exp >= 0)
    assert(flat >= 0)
    return functools.partial(_hyp_decay_func, start=start, exp=exp, flat=flat)


def _lin_decay_func(t, start, const, flat, stop):
    t = max(0, t - flat)
    return max(stop, start * (1 - const * t))


def lin_decay(start, const, flat=0, stop=0):
    assert(start >= 0)
    assert(const >= 0)
    assert(stop >= 0)
    assert(flat >= 0)
    return functools.partial(_lin_decay_func, start=start, const=const,
                             flat=flat, stop=stop)


class ConstCallable:
    def __init__(self, value):
        self.value = value

    def __call__(self, *args, **kwargs):
        return self.value


class EpsilonGreedyPolicy:
    def __init__(self, epsilon):
        if callable(epsilon):
            self.epsilon = epsilon
        else:
            self.epsilon = ConstCallable(epsilon)

    def __call__(self, Qs, t):
        Qs = np.asarray(Qs)
        if Qs.ndim == 0:
            Qs = np.reshape(Qs, (1,))

        epsilon = self.epsilon(t)
        if np.random.random() >= epsilon:
            max_i = np.argmax(Qs)
            return max_i
        else:
            chosen_i = np.random.randint(0, len(Qs))
            return chosen_i

    def choice_prob(self, Qs, t):
        Qs = np.asarray(Qs)
        if Qs.ndim == 0:
            Qs = np.reshape(Qs, (1,))

        epsilon = self.epsilon(t)
        max_i = np.argmax(Qs)
        probs = np.full(len(Qs), epsilon / len(Qs))
        probs[max_i] += 1 - epsilon
        return probs


class SoftmaxPolicy:
    def __init__(self, tau):
        if callable(tau):
            self.tau = tau
        else:
            self.tau = ConstCallable(tau)

    def __call__(self, Qs, t):
        Qs = np.asarray(Qs)
        if Qs.ndim == 0:
            Qs = np.reshape(Qs, (1,))

        softmaxes = self.choice_prob(Qs, t)
        chosen_i = np.random.choice(range(len(Qs)), p=softmaxes)
        return chosen_i

    def choice_prob(self, Qs, t):
        tau = self.tau(t)
        Qs = np.asarray(Qs)
        exps = np.exp((Qs - Qs.max()) / tau)
        return exps / np.sum(exps)


class IndepQLearners(Learner):
    def __init__(self, mdp,
                 alpha,
                 gamma,
                 explore_policy,
                 update_method='QL',
                 faql_beta=None,
                 keep_time_samples=False):
        super().__init__(keep_time_samples=keep_time_samples)
        if update_method not in ('QL', 'FAQL', 'RUQL'):
            raise ValueError("Invalid update_method value")

        if update_method == 'FAQL' and faql_beta is None:
            raise ValueError('FAQL requires a value for faql_beta')

        self.__mdp = mdp
        self.gamma = gamma

        if callable(alpha):
            self.alpha = alpha
        else:
            self.alpha = ConstCallable(alpha)

        self.explore_policy = explore_policy

        self.update_method = update_method
        if faql_beta is None or callable(faql_beta):
            self.faql_beta = faql_beta
        else:
            self.faql_beta = ConstCallable(faql_beta)

        max_r = self.mdp.max_reward
        if max_r > 0:
            self.max_r_inf = max_r / (1 - self.gamma)
        else:
            self.max_r_inf = 0

        # Separate Q for each agent
        self.Qs = dict()
        for ag in self.mdp.agent_keys:
            A = self.mdp.action_space.sub(ag)
            S = self.mdp.observation_space(ag)
            self.Qs[ag] = np.zeros((len(S), len(A)))

        self.time = 0

    @property
    def mdp(self):
        return self.__mdp

    def reset(self):
        self.time = 0
        for ag in self.mdp.agent_keys:
            self.Qs[ag][...] = 0

    @staticmethod
    def _greedy_action(mdp, Qs, state):
        actions = dict()
        valid_actions = mdp.valid_actions(state)
        for ag in mdp.agent_keys:
            A = mdp.action_space.sub(ag)
            S = mdp.observation_space(ag)
            observ = mdp.observation(ag, state)
            Q = Qs[ag]
            valid_idx = A.index(valid_actions[ag])
            i_argmax = Q[S.index(observ), valid_idx].argmax()
            action_i = valid_idx[i_argmax]
            actions[ag] = A[action_i]
        return mdp.action_space.make_item(**actions)

    def greedy_action(self, state):
        return self._greedy_action(self.mdp, self.Qs, state)

    def explore_action(self, state):
        actions = dict()
        valid_actions = self.mdp.valid_actions(state)
        for ag in self.mdp.agent_keys:
            A = self.mdp.action_space.sub(ag)
            S = self.mdp.observation_space(ag)
            observ = self.mdp.observation(ag, state)
            Q = self.Qs[ag]
            valid_idx = A.index(valid_actions[ag])
            Qvals = Q[S.index(observ), valid_idx]
            chosen_i = self.explore_policy(Qvals, self.time)
            action_i = valid_idx[chosen_i]
            actions[ag] = A[action_i]
        return self.mdp.action_space.make_item(**actions)

    def _update_single(self, ag,
                       s, a, r, new_s,
                       valid_next_acts,
                       valid_acts):
        A = self.mdp.action_space.sub(ag)
        S = self.mdp.observation_space(ag)
        Q = self.Qs[ag]

        obs = self.mdp.observation(ag, s)
        new_obs = self.mdp.observation(ag, new_s)

        next_acts_i = A.index(valid_next_acts)
        maxQ = Q[S.index(new_obs), next_acts_i].max() + self.max_r_inf
        target = r + self.gamma * maxQ

        alpha = self.alpha(self.time)
        obs_idx = S.index(obs)
        a_idx = A.index(a)
        oldQ = Q[obs_idx, a_idx] + self.max_r_inf

        if self.update_method == 'QL':
            Q[obs_idx, a_idx] = ((1 - alpha) * oldQ + alpha * target
                                 - self.max_r_inf)
        elif self.update_method == 'RUQL':
            valid_acts_idx = A.index(valid_acts)
            Qvals = Q[obs_idx, valid_acts_idx]
            probs = self.explore_policy.choice_prob(Qvals, self.time)
            a_idx_in_valid = valid_acts_idx.index(a_idx)
            prob_a = probs[a_idx_in_valid]

            ruql_alpha = 1 - (1 - alpha) ** (1 / prob_a)

            Q[obs_idx, a_idx] = ((1 - ruql_alpha) * oldQ + ruql_alpha * target
                                 - self.max_r_inf)
        elif self.update_method == 'FAQL':
            valid_acts_idx = A.index(valid_acts)
            Qvals = Q[obs_idx, valid_acts_idx]
            probs = self.explore_policy.choice_prob(Qvals, self.time)
            a_idx_in_valid = valid_acts_idx.index(a_idx)
            prob_a = probs[a_idx_in_valid]

            faql_beta = self.faql_beta(self.time)
            faql_coeff = min(1, faql_beta / prob_a) * alpha

            Q[obs_idx, a_idx] = (oldQ + faql_coeff * (target - oldQ)
                                 - self.max_r_inf)

    def update(self, s, a, r, new_s):
        valid_next_acts = self.mdp.valid_actions(new_s)
        if self.update_method in ('FAQL', 'RUQL'):
            valid_acts = self.mdp.valid_actions(s)
        else:
            valid_acts = {ag: None for ag in self.mdp.agent_keys}

        for ag in self.mdp.agent_keys:
            self._update_single(ag, s, a[ag], r[ag], new_s,
                                valid_next_acts[ag],
                                valid_acts[ag])
        self.time += 1

    def learn(self, steps, after=None):
        for step in range(steps):
            with measure_time(self.train_time_counter):
                a = self.explore_action(self.mdp.state)
                (s, r, ns) = self.mdp.do_action(a)
                self.update(s, a, r, ns)
            if after is not None:
                after(s, a, r, ns)

    def policy(self):
        return QPolicy(self.mdp, self.Qs)

    def pack(self, size_thresh=512 * 1024):
        hdf_kw = {ag: self.Qs[ag] for ag in self.mdp.agent_keys}
        hdf_kw['train_times'] = np.array(self.train_time_counter.samples)
        hdf_kw['test_times'] = np.array(self.test_time_counter.samples)
        label = type(self).__name__
        (tmpfile, packed_id) = utils.pack.np2hdf(label, **hdf_kw)
        stripped = copy.copy(self)
        stripped.train_time_counter = copy.copy(stripped.train_time_counter)
        stripped.test_time_counter = copy.copy(stripped.test_time_counter)
        stripped.Qs = None
        stripped.train_time_counter.samples = None
        stripped.test_time_counter.samples = None

        tmppath = pathlib.Path(tmpfile)
        if size_thresh is not None and tmppath.stat().st_size >= size_thresh:
            s3key = "tmp/{!s}".format(packed_id)
            utils.aws.upload_file(tmpfile, 'hrl.zanol.eu', s3key,
                                  delete_source=True)
            data = pickle.dumps((IndepQLearners, stripped, s3key),
                                protocol=4)
        else:
            Qs_data = tmppath.read_bytes()
            data = pickle.dumps((IndepQLearners, stripped, Qs_data),
                                protocol=4)
            tmppath.unlink()
        return base64.b64encode(zlib.compress(data))

    @staticmethod
    def unpack(packed):
        loaded = pickle.loads(zlib.decompress(base64.b64decode(packed)))
        if loaded[0] is IndepQLearners:
            (_, stripped, Qs) = loaded
            if isinstance(Qs, str):
                s3key = Qs
                h5f = utils.aws.aws_fetch_data(s3key)
            else:
                Qs_data = Qs
                tmp = tempfile.NamedTemporaryFile(delete=False)
                tmp.write(Qs_data)
                tmp.close()
                h5f = h5py.File(tmp.name, 'r')

            stripped.Qs = dict()
            for ag in stripped.mdp.agent_keys:
                stripped.Qs[ag] = np.asarray(h5f[ag], dtype=float)
            stripped.train_time_counter.samples = list(h5f['train_times'])
            stripped.test_time_counter.samples = list(h5f['test_times'])
            h5f.close()
            return stripped
        elif issubclass(loaded[0], IndepQLearners):
            return loaded[0].unpack(packed)
        else:
            raise TypeError()


class QPolicy:
    def __init__(self, mdp, Qs):
        self.mdp = mdp
        self.Qs = copy.deepcopy(Qs)

    def __call__(self, state):
        return IndepQLearners._greedy_action(self.mdp, self.Qs, state)
