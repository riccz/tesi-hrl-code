import logging

from .abstr import Learner

logger = logging.getLogger(__name__)


class RandomWalker(Learner):
    def __init__(self, mdp, keep_time_samples=False):
        super().__init__(keep_time_samples=keep_time_samples)
        self.__mdp = mdp

    @property
    def mdp(self):
        return self.__mdp

    def policy(self):
        return self.mdp.random_action
