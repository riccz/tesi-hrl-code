import logging
import pathlib
import re
import subprocess

logger = logging.getLogger(__name__)


def git_sha1(git_bin='git'):
    try:
        git_proc = subprocess.run([git_bin, "log",
                                   "-1",
                                   "--format=%H"],
                                  check=True,
                                  stdout=subprocess.PIPE)
        git_sha1 = git_proc.stdout.strip().decode()
        return git_sha1
    except FileNotFoundError:
        logger.warning("Git executable was not found")
        gitdir = pathlib.Path('.git')
        if not gitdir.is_dir():
            logger.warning("Git directory '{!s}' was not found".format(gitdir))
            raise RuntimeError("Cannot determine the current git commit")
        head = (gitdir / 'HEAD').read_text().strip()

        while not re.fullmatch('[0-9a-fA-F]{40}', head):
            ref_m = re.fullmatch('ref:(.*)', head)
            if ref_m:
                ref = ref_m.group(1).strip()
                head = (gitdir / ref).read_text().strip()
            else:
                logger.warning("Unexpected HEAD: \"{!s}\"".format(head))
                raise RuntimeError("Cannot determine the current git commit")
        return head
