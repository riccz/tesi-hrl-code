import functools
import itertools
import logging
import math
import operator
import random

import numpy as np

from .abstr import Learner
from . import utils

logger = logging.getLogger(__name__)


class Node:
    NODE_TYPES = {'STATE': 0, 'ACTION': 1}

    __slots__ = ['node_type', 'children', 'parent', 'depth']

    def __init__(self, node_type):
        self.children = []
        self.parent = None
        self.depth = 0
        if isinstance(node_type, str):
            node_type = Node.NODE_TYPES[node_type]
        else:
            assert(node_type in Node.NODE_TYPES.values())
        self.node_type = node_type


class StateNode(Node):
    __slots__ = ['state', 'count', 'value']

    def __init__(self, state):
        super().__init__('STATE')
        self.count = 0
        self.state = state
        self.value = None


class ActionNode(Node):
    __slots__ = ['action', 'count', 'value', 'child_index']

    def __init__(self, action):
        super().__init__('ACTION')
        self.count = 0
        self.action = action
        self.value = None
        self.child_index = dict()


class MCTS(Learner):
    def __init__(self, mdp, gamma, max_depth, uct_c, rollout_count,
                 max_fanout=None,
                 keep_time_samples=False):
        super().__init__(keep_time_samples=keep_time_samples)
        self.__mdp = mdp
        self.gamma = gamma
        self.max_depth = max_depth
        self.uct_c = uct_c
        self.rollout_count = rollout_count
        self.max_fanout = max_fanout

    @property
    def mdp(self):
        return self.__mdp

    def policy(self):
        return self._greedy_policy

    def _greedy_single_policy(self, agent, state):
        (actions, Qs) = utils.iter.unstar(self.allQs(agent, state))
        chosen_i = np.fromiter(Qs, dtype=float).argmax()
        a = next(itertools.islice(actions, int(chosen_i), None))
        return a

    def _greedy_policy(self, state):
        action = dict()
        for ag in self.mdp.agent_keys:
            action[ag] = self._greedy_single_policy(ag, state)
        logger.debug("Chosen one joint action")
        return self.mdp.action_space.make_item(**action)

    def _uct_value(self, action_node):
        Na = action_node.count
        if Na == 0:
            return np.inf
        else:
            Q = action_node.value
            Ns = action_node.parent.count
            reg = np.sqrt(np.log(Ns) / Na)
            return Q + self.uct_c * reg

    def _rollout(self, agent, root):
        cum_disc_r = 0
        state_node = root

        while math.floor(state_node.depth / 2) < self.max_depth:
            if len(state_node.children) == 0:
                actions = self.mdp.valid_actions(state_node.state)
                for a in actions[agent]:
                    an = ActionNode(a)
                    an.parent = state_node
                    an.depth = state_node.depth + 1
                    state_node.children.append(an)
                not_visited = state_node.children
            else:
                not_visited = list(filter(lambda an: an.count == 0,
                                          state_node.children))

            if len(not_visited) > 0:
                action_node = random.choice(not_visited)
            else:
                uct_values = list(map(self._uct_value, state_node.children))
                i = np.argmax(uct_values)
                action_node = state_node.children[i]

            (r, new_s) = self.mdp.simulate_indep_action(agent,
                                                        state_node.state,
                                                        action_node.action)
            disc_gamma = self.gamma ** math.floor(action_node.depth / 2)
            cum_disc_r += disc_gamma * r

            new_s_idx = self.mdp.state_space.index(new_s)
            new_s_node = action_node.child_index.get(new_s_idx)
            if new_s_node is None:
                if self.max_fanout is not None and (
                        len(action_node.children) >= self.max_fanout):
                    # probs = [s.count / action_node.count
                    #          for s in action_node.children]
                    new_s_node = np.random.choice(action_node.children)  # ,
                                                  # p=probs)
                else:
                    new_s_node = StateNode(new_s)
                    new_s_node.parent = action_node
                    new_s_node.depth = action_node.depth + 1
                    action_node.children.append(new_s_node)
                    action_node.child_index[new_s_idx] = new_s_node

            state_node = new_s_node

        valued_node = state_node
        valued_node.value = cum_disc_r
        valued_node.count += 1

        while valued_node.parent is not None:
            valued_node = valued_node.parent
            valued_node.count += 1
            children_values = map(operator.attrgetter('value'),
                                  valued_node.children)
            children_values = filter(lambda v: v is not None, children_values)
            valued_node.value = utils.iter.average(children_values)

    def allQs(self, agent, state):
        qtree = StateNode(state)
        for _ in range(self.rollout_count):
            self._rollout(agent, qtree)
        uct_values = [(an.action, self._uct_value(an))
                      for an in qtree.children]
        return uct_values
