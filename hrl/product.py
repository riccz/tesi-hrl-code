from collections import OrderedDict
import copy
import functools
import itertools
import logging
import numbers
import operator

import numpy as np

from .abstr import InvSeq
from .utils.misc import cached_property

logger = logging.getLogger(__name__)


def _unravel_reduce(acc, s):
    (idx, out) = acc
    out.append(idx % s)
    idx //= s
    return (idx, out)


def unravel_index(idx, shape):
    revshape = reversed(shape)
    (_, revout) = functools.reduce(_unravel_reduce, revshape, (idx, []))
    return tuple(reversed(revout))


def _ravel_reduce(out, x):
    (i, s) = x
    return out * s + i


def ravel_multi_index(midx, shape):
    xs = zip(midx, shape)
    (i0, _) = next(xs)
    return functools.reduce(_ravel_reduce, xs, i0)


class ProductSpace(InvSeq):
    """Class to build a bi-map as the cross-product of other bi-maps

    This class is a InvSeq where the item set is built as the
    cross-product of some other given InvSeq. The keys are still
    flat.

    """

    def __init__(self, **kwargs):
        """Construct using the bi-maps passed as arguments

        The subspace names are stored in increasing order.

        """
        super().__init__()
        self.spaces = OrderedDict()
        lens = []
        for k, v in sorted(kwargs.items()):
            self.spaces[k] = v
            lens.append(v.max_key + 1)
        self.shape = tuple(lens)

        out_dtype_arg = []
        for k, s in self.spaces.items():
            out_dtype_arg.append((k, s.pack(s[0]).dtype))
        self._out_dtype = np.dtype(out_dtype_arg)

    def __eq__(self, other):
        return self.spaces == other.spaces

    @cached_property
    def max_key(self):
        return functools.reduce(operator.mul, self.shape, 1) - 1

    def __len__(self):
        return functools.reduce(operator.mul, self.shape, 1)

    def k2i(self, key):
        if not isinstance(key, numbers.Integral):
            raise TypeError("Key {!s} is not an integer".format(key))
        if key < 0 or key > self.max_key:
            raise IndexError("Key {!s} is out of range".format(key))
        subkeys = unravel_index(key, self.shape)
        sk_iter = zip(self.spaces.values(), subkeys)
        subitems = itertools.starmap(operator.getitem, sk_iter)
        prod_item = {k: si for k, si in zip(self.spaces.keys(), subitems)}
        return prod_item

    def i2k(self, item):
        if not isinstance(item, self.item_type):
            temsg = "Item {!s} is not a {!s}".format(item, self.item_type)
            raise TypeError(temsg)
        try:
            subkeys = tuple(s.i2k(item[k]) for k, s in self.spaces.items())
        except (TypeError, ValueError):
            vemsg = ("Item {!s} does not contain"
                     " compatible subitems").format(item)
            raise ValueError(vemsg)
        return ravel_multi_index(subkeys, self.shape)

    def __getitem__(self, key):
        if isinstance(key, tuple):
            # Use directly as subkeys
            which_ints = [isinstance(k, numbers.Integral) for k in key]
            if all(which_ints):
                sk_iter = zip(self.spaces.values(), key)
                subitems = itertools.starmap(operator.getitem, sk_iter)
                k_si = zip(self.spaces.keys(), subitems)
                prod_item = {k: si for k, si in k_si}
                return prod_item
            else:
                # tuple of iterables
                iter_keys = [(k,) if is_int else k
                             for k, is_int in zip(key, which_ints)]
                sk_iter = zip(self.spaces.values(), iter_keys)
                subitems = itertools.starmap(operator.getitem, sk_iter)

                (si1, si2) = itertools.tee(subitems)
                outshape = tuple(len(si) for si in si1)
                out = np.empty(outshape, dtype=object)
                for i, p in enumerate(itertools.product(*si2)):
                    state = {k: v for k, v in zip(self.spaces.keys(), p)}
                    out.flat[i] = state
                return out
        else:
            return super().__getitem__(key)

    def sub(self, key):
        return self.spaces[key]

    @property
    def subkeys(self):
        return self.spaces.keys()

    @property
    def subspaces(self):
        return self.spaces.values()

    def make_item(self, **kwargs):
        """Construct a ProductState using the given arguments

        The arguments must be the same keywords used to name the
        component spaces. The value of each argument must be a dict
        and is passed as a set of keyword arguments to the
        corresponding space to build each piece of the ProductState.

        """
        sorted_keys = sorted(kwargs.keys())
        if len(kwargs) != len(self.spaces):
            raise ValueError("Wrong number of arguments")
        if any(spk != k for spk, k in zip(self.spaces.keys(), sorted_keys)):
            raise ValueError("Must use the same subspace names")

        statevars = dict()
        for k, s in zip(sorted_keys, self.spaces.values()):
            v = kwargs[k]
            if v in s:
                statevars[k] = copy.deepcopy(v)
            elif isinstance(v, dict):
                statevars[k] = s.make_item(**v)
            elif isinstance(v, tuple):
                statevars[k] = s.make_item(*v)
            else:
                statevars[k] = s.make_item(v)
        return statevars

    def pack(self, state):
        out = np.empty(tuple(), dtype=self._out_dtype)
        for k, s in self.spaces.items():
            out[k] = s.pack(state[k])
        return out

    def unpack(self, packed):
        kw = {k: s.unpack(packed[k])
              for k, s in self.spaces.items()}
        return self.make_item(**kw)
