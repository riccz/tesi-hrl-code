import copy
import functools
import itertools
import logging
import operator
import random

from scipy import linalg
import numpy as np

from hrl import utils

logger = logging.getLogger(__name__)


def ald(samples, kern, thresh):
    if thresh < 0:
        raise ValueError("Negative ALD threshold")
    if thresh == 0:
        D = list(samples)
        logger.debug("Skipping ALD: thresh = 0,"
                     " len(D) = {:d}".format(len(D)))
        return D

    samples_iter = iter(samples)
    s = next(samples_iter)
    D = [s]
    K = [[kern(s, s)]]
    Kinv = linalg.pinvh(K)

    for s in samples_iter:
        kvec = kern(D, s)
        c = np.matmul(Kinv, kvec)
        kss = kern(s, s)
        delta = kss - np.vdot(c, kvec)

        if delta >= thresh:
            for i, row in enumerate(K):
                row.append(kvec[i])  # new column K(si, st)
            K.append(list(kvec))
            K[-1].append(kss)
            Kinv = linalg.pinvh(K)
            D.append(s)
    logger.debug("ALD finished: len(D) = {:d}".format(len(D)))
    return D


def klstdq(samples, kern, gamma, ald_thresh):
    (samples_cp, samples) = itertools.tee(samples)
    ald_samples = map(operator.itemgetter(0, 1), samples_cp)
    D = ald(ald_samples, kern=kern, thresh=ald_thresh)

    A = np.zeros((len(D), len(D)))
    b = np.zeros(len(D))
    for s in samples:
        sa = s[0:2]
        r = s[2]
        nsa = s[3:5]

        ksa_vec = kern(D, sa)
        knsa_vec = kern(D, nsa)

        A += np.matmul(ksa_vec[:, np.newaxis],
                       (ksa_vec - gamma * knsa_vec)[np.newaxis, :])
        b += ksa_vec * r

    Ainv = linalg.pinv2(A)
    alpha = np.matmul(Ainv, b)
    return (alpha, D)


def reg_lstd(Z, Zhalf, rvec, kern, gamma, lambda_h, lambda_Q):
    N = len(Zhalf)

    K_h = np.empty((N, N))
    for j in range(N):
        K_h[:, j] = kern(Zhalf, Zhalf[j])

    K_Q = np.empty((2*N, 2*N))
    for j in range(2*N):
        K_Q[:, j] = kern(Z, Z[j])

    C1 = np.hstack((np.eye(N), np.zeros((N, N))))
    C2 = np.hstack((np.zeros((N, N)), np.eye(N)))

    E = K_h @ linalg.inv(K_h + N * lambda_h * np.eye(N))

    F = C1 - gamma * E @ C2

    alphas = (linalg.inv(F.transpose() @ F @ K_Q +
                         N * lambda_Q * np.eye(2*N)) @
              F.transpose() @ E @ rvec)

    return alphas


class epsilon_greedy_exploration:
    def __init__(self, epsilon):
        self.epsilon = epsilon

    def __call__(self, Qs):
        if random.random() < self.epsilon:
            return random.randrange(len(Qs))
        else:
            return np.argmax(Qs)


class APILearner:
    def __init__(self, mdp, gamma, exploration):
        if gamma < 0 or gamma > 1:
            raise ValueError("The reward discount gamma must be in [0,1]")

        self.mdp = mdp
        self.gamma = gamma
        self.exploration = exploration
        self.time = 0

    def reset(self):
        self.time = 0

    def _random_policy(self, state):
        if isinstance(state, tuple):
            (state, R) = state
        return self.mdp.random_action(state)

    def _exploration_policy(self, state):
        (Qs, actions) = self.allQs(state)
        chosen_i = self.exploration(Qs)
        return actions[chosen_i]

    def _greedy_policy(self, state):
        (Qs, actions) = self.allQs(state)
        chosen_i = np.argmax(Qs)
        return actions[chosen_i]

    def allQs(self, state):
        if isinstance(state, tuple):
            (mdpstate, R) = state
        else:
            mdpstate = state

        valid_actions = self.mdp.valid_actions(mdpstate)
        assert(len(valid_actions) == 1)  # Single agent only

        Qs = [self.Q(state, {'ag0': a}) for a in valid_actions['ag0']]
        return (Qs, [{'ag0': a} for a in valid_actions['ag0']])

    def learn(self, steps, after=None):
        if self.time == 0:
            logger.debug("Start learning with a random policy")
            policy = self._random_policy
        else:
            logger.debug("Start learning with the previous policy")
            policy = self._exploration_policy

        self.time += 1
        samples = []
        na = None
        for _ in range(steps):
            s = self.mdp.state

            if hasattr(self.mdp, '_reward'):
                R = self.mdp._reward._reward_matrix
                s = (s, R)

            a = policy(s) if na is None else na
            (_, r, ns) = self.mdp.do_action(a)

            if isinstance(r, dict):
                assert(len(r) == 1)
                r = r['ag0']
            if hasattr(self.mdp, '_reward'):
                nR = self.mdp._reward._reward_matrix
                ns = (ns, nR)

            na = policy(ns)
            samples.append((s, a, r, ns, na))

            if after is not None:
                after(s, a, r, ns)

        logger.debug("Sample generation complete:"
                     " {:d} samples".format(len(samples)))

        self._update_Q(samples)
        logger.debug("AP iteration {:d} complete".format(self.time))

    def test(self, steps, reset=True, after=None):
        if self.time == 0:
            logger.warning("Testing without a learned policy")
            policy = self._random_policy
        else:
            policy = self._greedy_policy

        test_mdp = copy.deepcopy(self.mdp)
        if reset:
            test_mdp.reset()

        rewards = []
        for _ in range(steps):
            s = self.mdp.state

            if hasattr(self.mdp, '_reward'):
                R = self.mdp._reward._reward_matrix
                s = (s, R)

            a = policy(s)
            (_, r, ns) = self.mdp.do_action(a)
            rewards.append(r)

            if hasattr(self.mdp, '_reward'):
                nR = self.mdp._reward._reward_matrix
                ns = (ns, nR)

            if after is not None:
                after(s, a, r, ns)

        rewards = utils.flatten_dicts(rewards)
        all_rewards = itertools.chain.from_iterable(rewards.values())
        return np.fromiter(all_rewards, dtype=float).mean()


class KLSPILearner(APILearner):
    def __init__(self, mdp, kern, gamma, ald_thresh=0, epsilon=0):
        if gamma < 0 or gamma > 1:
            raise ValueError("gamma must be in [0,1]")
        if ald_thresh < 0:
            raise ValueError("ald_thresh must be >= 0")
        if epsilon < 0 or epsilon > 1:
            raise ValueError("epsilon must be in [0,1]")

        super().__init__(mdp, gamma,
                         epsilon_greedy_exploration(epsilon))

        self.kern = kern
        self.ald_thresh = ald_thresh

        self.alpha = None
        self.D = None

    def reset(self):
        super().reset()
        self.alpha = None
        self.D = None

    def _update_Q(self, samples):
        (self.alpha, self.D) = klstdq(samples, self.kern,
                                      self.gamma, self.ald_thresh)

    def Q(self, state, action):
        sa = (state, action)
        ksa_vec = self.kern(self.D, sa)
        return np.vdot(self.alpha, ksa_vec)

    def policy(self):
        return KLSPIPolicy(self.mdp, self.alpha, self.D, self.kern)


class RegLSTDLearner(APILearner):
    def __init__(self, mdp, kern, gamma,
                 lambda_h=1, lambda_Q=1,
                 epsilon=0):
        if gamma < 0 or gamma > 1:
            raise ValueError("gamma must be in [0,1]")
        if epsilon < 0 or epsilon > 1:
            raise ValueError("epsilon must be in [0,1]")
        if lambda_h < 0:
            raise ValueError("lambda_h must be >= 0")
        if lambda_Q < 0:
            raise ValueError("lambda_Q must be >= 0")

        super().__init__(mdp, gamma,
                         epsilon_greedy_exploration(epsilon))

        self.kern = kern
        self.lambda_h = lambda_h
        self.lambda_Q = lambda_Q

        self.alpha = None
        self.Z = None

    def reset(self):
        super().reset()
        self.alpha = None
        self.Z = None

    def _update_Q(self, samples):
        rvec = []
        Zhalf = []
        Z = []
        for s, a, r, ns, na in samples:
            rvec.append(r)
            Zhalf.append((s, a))
            Z.append((s, a))
            Z.append((ns, na))

        self.alpha = reg_lstd(Z, Zhalf, rvec,
                              kern=self.kern,
                              gamma=self.gamma,
                              lambda_h=self.lambda_h,
                              lambda_Q=self.lambda_Q)
        self.Z = Z

    def Q(self, state, action):
        sa = (state, action)
        ksa_vec = self.kern(self.Z, sa)
        return np.vdot(self.alpha, ksa_vec)

    def policy(self):
        return KLSPIPolicy(self.mdp, self.alpha, self.Z, self.kern)


class KLSPIPolicy:
    def __init__(self, mdp, alpha, D, kern):
        self.mdp = mdp
        self.alpha = copy.deepcopy(alpha)
        self.D = copy.deepcopy(D)
        self.kern = kern

    def __call__(self, state, epsilon=0):
        if isinstance(state, tuple):
            (mdpstate, R) = state
        else:
            mdpstate = state

        if random.random() < epsilon:
            return self.mdp.random_action(mdpstate)

        valid_actions = self.mdp.valid_actions(mdpstate)
        assert(len(valid_actions) == 1)
        Qs = np.empty(len(valid_actions['ag0']))
        for i, a in enumerate(valid_actions['ag0']):
            sa = (state, {'ag0': a})
            ksa_vec = np.fromiter((self.kern(sd, sa) for sd in self.D),
                                  dtype=float)
            Qs[i] = np.vdot(self.alpha, ksa_vec)

        best_i = np.argmax(Qs)
        return {'ag0': valid_actions['ag0'][best_i]}


def rbf_kernel(s1, s2, sigma=1.0, sigma_sq=None):
    if sigma_sq is not None:
        logger.warning("Use sigma instead of sigma_sq")
        sigma = np.sqrt(sigma_sq)

    diff = np.subtract(s1, s2)
    if diff.ndim < 2:
        norm2 = np.vdot(diff, diff)
    else:
        norm2 = np.linalg.norm(diff, axis=1, ord=2) ** 2
    return np.exp(-norm2 / (2 * sigma**2))
