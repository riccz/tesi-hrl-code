from collections import OrderedDict
import itertools
import numbers

import numpy as np

from . import utils
from .abstr import InvSeq
from .utils.misc import cached_property


class UnionSpace(InvSeq):
    """Class to build a bi-map as the union of other bi-maps

    This class is a InvSeq where the item set is given by the union
    of the item sets of other InvSeqs. The keyspace is still an
    integer range.

    """

    def __init__(self, **kwargs):
        """Initialize using the given InvSeq objects

        The argument names are used to track the corresponding spaces.

        """
        super().__init__()
        self.spaces = OrderedDict()
        lens = []
        for k, v in sorted(kwargs.items()):
            self.spaces[k] = v
            lens.append(v.max_key + 1)
        self.shape = tuple(lens)

        ranges = [range(0, lens[0])]
        for l in lens[1:]:
            last_r = ranges[-1]
            ranges.append(range(last_r.stop, last_r.stop+l))

        (z1, z2) = itertools.tee(zip(ranges, self.spaces.keys()))
        self._range2name = {r: n for r, n in z1}
        self._name2range = {n: r for r, n in z2}

        key_maxlen = max(len(k.encode()) for k in self.subkeys)
        out_dtype_arg = [('which', ('S', key_maxlen))]
        for k, s in self.spaces.items():
            out_dtype_arg.append((k, s.pack(s[0]).dtype))
        self._out_dtype = np.dtype(out_dtype_arg)

    @cached_property
    def max_key(self):
        return sum(self.shape) - 1

    def __len__(self):
        return self.max_key + 1

    def __eq__(self, other):
        return self.spaces == other.spaces

    def k2i(self, key):
        if not isinstance(key, numbers.Integral):
            raise TypeError("Key {!s} is not an integer".format(key))
        if key < 0 or key > self.max_key:
            raise IndexError("Key {!s} is out of range".format(key))

        match_key = utils.iter.starfilter(lambda r, _: key in r,
                                          self._range2name.items())
        rn = next(match_key, None)
        if rn is None:
            raise IndexError()
        (r, n) = rn
        subkey = key - r.start
        s = self.spaces[n]
        return {n: s[subkey]}

    def i2k(self, item):
        if not isinstance(item, self.item_type):
            match_subspace = utils.iter.starfilter(lambda _, s: item in s,
                                                   self.spaces.items())
            which_subspace = next(match_subspace, None)
            if which_subspace is None:
                raise TypeError()
            (n, s) = which_subspace
        else:
            names = list(item.keys())
            if len(names) > 1:
                raise TypeError()
            n = names[0]
            s = self.spaces[n]
        subkey = s.index(item[n])
        return self._name2range[n].start + subkey

    def sub(self, key):
        return self.spaces[key]

    @property
    def subkeys(self):
        return self.spaces.keys()

    @property
    def subspaces(self):
        return self.spaces.values()

    def make_item(self, **kwargs):
        keys = list(kwargs.keys())
        if len(keys) > 1:
            raise ValueError("Wrong number of arguments")

        k = keys[0]
        s = self.spaces.get(k)
        if s is None:
            raise ValueError("Must use the same subspace names")

        v = kwargs[k]
        if v in s:
            return {k: v}
        elif isinstance(v, dict):
            return {k: s.make_item(**v)}
        elif isinstance(v, tuple):
            return {k: s.make_item(*v)}
        else:
            return {k: s.make_item(v)}

    def pack(self, state):
        (k, v) = next(iter(state.items()))
        subpacked = self.spaces[k].pack(v)
        out = np.empty(tuple(), dtype=self._out_dtype)
        out[k] = subpacked
        out['which'] = k.encode()
        return out

    def unpack(self, packed):
        k = bytes(packed['which']).strip(b'\x00').decode()
        s = self.spaces[k]
        sub = s.unpack(packed[k])
        return {k: sub}
