from itertools import starmap
import copy
import functools
import itertools
import logging
import math
import numbers
import operator
import random

from scipy import stats
import cachetools
import numpy as np

from . import utils
from .abstr import InvSeq
from .basic import QuantizedSpace, NamedSpace
from .product import ProductSpace
from .union import UnionSpace
from .utils import partition, remove_existing, cached_property
from .utils.iter import starfilter, select

logger = logging.getLogger(__name__)


class SafeLRUCache(cachetools.LRUCache):
    """LRU cache that deepcopies the returned values.
       Prevents modification of the cached value by accident.
    """
    def __getitem__(self, *args, **kwargs):
        return copy.deepcopy(super().__getitem__(*args, **kwargs))


class SeededLRUCache(cachetools.LRUCache):
    """LRU cache that can be pre-seeded with a fixed set of values.
       The pre-seeded values are never purged.
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._preseeded = dict()
        self.__noval = object()

    def __getitem__(self, key):
        v = self._preseeded.get(key, self.__noval)
        if v is not self.__noval:
            return v
        else:
            return super().__getitem__(key)

    def seed(self, key, value):
        self._preseeded[key] = value

    def clear_preseeded(self):
        self._preseeded.clear()


def reward_transition_prob_on(R, Rspace, filter_pos=None):
    x0i = Rspace.xspace.index(0.0)
    y0i = Rspace.yspace.index(0.0)

    assert(not R[x0i, y0i])
    R[x0i, y0i] = True
    can_toggle_on = np.where(np.logical_not(R))
    R[x0i, y0i] = False

    if filter_pos is not None:
        xy = zip(*can_toggle_on)
        can_toggle_on = list(starfilter(filter_pos, xy))
        if len(can_toggle_on) == 0:
            can_toggle_on = ([], [])
        else:
            can_toggle_on = utils.iter.unstar(can_toggle_on, lazy=False)

    N = len(can_toggle_on[0])
    pons = Rspace.pon[can_toggle_on]
    all_possible = itertools.product((True, False),
                                     repeat=N)

    # Build list with (next R indices, prob)
    probs = []
    for v in all_possible:
        # Split pon(x, y) in pon, 1-pon
        (pon_ones, pon_zeros) = utils.partition(zip(v, pons),
                                                operator.itemgetter(0))
        pon_zeros = starmap(lambda _, p: 1 - p, pon_zeros)
        pon_ones = map(operator.itemgetter(1), pon_ones)

        p = functools.reduce(operator.mul,
                             itertools.chain(pon_ones, pon_zeros),
                             1)
        nextR = R.copy()
        nextR[can_toggle_on] = v
        nextR_idx = Rspace.index(nextR)
        probs.append((nextR_idx, p))
    return probs


def reward_transition_prob_off(R, Rspace, agents_pos):
    assert(isinstance(agents_pos, tuple))
    assert(len(agents_pos) == 2)
    assert(len(agents_pos[0]) == len(agents_pos[1]))

    # Filter only on positions: off are handled by
    # reward_transition_prob_on
    filt_agent_pos = ([], [])
    for xi, yi in zip(*agents_pos):
        if R[xi, yi]:
            filt_agent_pos[0].append(xi)
            filt_agent_pos[1].append(yi)
    agents_pos = filt_agent_pos

    N = len(agents_pos[0])
    poffs = Rspace.poff[agents_pos]
    all_possible = itertools.product((True, False),
                                     repeat=N)

    probs = []
    for v in all_possible:
        # Split poff(x, y) in pon, 1-pon
        (poff_ones, poff_zeros) = utils.partition(zip(v, poffs),
                                                  operator.itemgetter(0))
        poff_ones = starmap(lambda _, p: 1 - p, poff_ones)
        poff_zeros = map(operator.itemgetter(1), poff_zeros)

        p = functools.reduce(operator.mul,
                             itertools.chain(poff_ones, poff_zeros),
                             1)
        nextR = R.copy()
        nextR[agents_pos] = v
        nextR_idx = Rspace.index(nextR)
        probs.append((nextR_idx, p))
    return probs


def reward_transition_prob(R, Rspace, agents_pos, filter_on=None):
    can_toggle_on = zip(*np.where(np.logical_not(R)))
    if filter_on is not None:
        can_toggle_on = starfilter(filter_on, can_toggle_on)
    can_change = set(itertools.chain(zip(*agents_pos), can_toggle_on))

    x0i = Rspace.xspace.index(0.0)
    y0i = Rspace.yspace.index(0.0)
    remove_existing(can_change, (x0i, y0i))

    if len(can_change) == 0:
        return [(Rspace.index(R), 1.0)]

    can_change_unzip = utils.iter.unzip(can_change)
    can_change_unzip = tuple(np.fromiter(cc, dtype=int)
                             for cc in can_change_unzip)

    def apply_change(bitvec):
        nextR = R.copy()
        nextR[can_change_unzip] = bitvec
        nextR_idx = Rspace.index(nextR)
        return nextR_idx

    oldvec = np.asarray(R[can_change_unzip], dtype=bool)
    pon_vec = Rspace.pon[can_change_unzip]
    poff_vec = Rspace.poff[can_change_unzip]
    prob_maps = [
        1 - pon_vec,
        pon_vec,
        poff_vec,
        1 - poff_vec
    ]

    def bitvec_prob(bitvec):
        prob_vec = select(2 * oldvec + bitvec, zip(*prob_maps))
        return functools.reduce(operator.mul, prob_vec, 1.0)

    possible_bitvecs = list(itertools.product((True, False),
                                              repeat=len(can_change)))
    newRidxs = map(apply_change, possible_bitvecs)
    Rps = map(bitvec_prob, possible_bitvecs)
    p_prod = zip(newRidxs, Rps)
    # p_prod = list(p_prod)
    # assert(len(set(Ridx for Ridx, _ in p_prod)) == len(p_prod))
    return p_prod


def bitmatrix2int(bits):
    bits = np.reshape(bits, (-1,))
    padlen = math.ceil(len(bits) / 8) * 8 - len(bits)
    padded_bits = np.pad(bits, (padlen, 0), 'constant', constant_values=0)
    packed_bits = np.packbits(padded_bits)
    packed_int = functools.reduce(lambda i, b: (i << 8) + int(b),
                                  packed_bits, 0)
    return packed_int


def int2bitmatrix(packed_int, shape):
    bitlength = functools.reduce(operator.mul, shape, 1)
    bytelength = math.ceil(bitlength / 8)
    packed_bytes = packed_int.to_bytes(bytelength, 'big')
    packed_bytes = np.frombuffer(packed_bytes, dtype=np.uint8)
    unpacked_bits = np.unpackbits(packed_bytes)
    unpadded = unpacked_bits[-bitlength:]
    bitmatrix = np.reshape(unpadded, shape)
    return np.asarray(bitmatrix, dtype=bool)


def battery_bins(steps, minstep):
    return np.geomspace(minstep, 1, steps)


def _agent_keys(n):
    return tuple('ag{:d}'.format(i) for i in range(n))


class GridActionSpace(ProductSpace):
    def __init__(self, agents=1):
        if agents < 1:
            raise ValueError("At least one agent")

        self.single = NamedSpace('stay', 'up', 'down', 'left', 'right',
                                 'return')

        self.agent_keys = _agent_keys(agents)
        multiag_kw = {ag: self.single for ag in self.agent_keys}
        super().__init__(**multiag_kw)


class GridRewardSpace(InvSeq):
    def __init__(self, xspace, yspace, pon, poff):
        super().__init__()
        self.xspace = xspace
        self.yspace = yspace
        self.state_shape = (len(xspace), len(yspace))

        pon = np.array(pon, dtype=float)
        assert(np.all(pon >= 0))
        assert(np.all(pon <= 1))
        if pon.ndim == 0:
            self.pon = np.ones(self.state_shape) * pon
        else:
            assert(pon.shape == self.state_shape)
            self.pon = pon

        poff = np.array(poff, dtype=float)
        assert(np.all(poff >= 0))
        assert(np.all(poff <= 1))
        if poff.ndim == 0:
            self.poff = np.ones(self.state_shape) * poff
        else:
            assert(poff.shape == self.state_shape)
            self.poff = poff

    def __eq__(self, other):
        return (self.xspace == other.xspace and
                self.yspace == other.yspace and
                np.all(self.pon == other.pon) and
                np.all(self.poff == other.poff))

    @cached_property
    def max_key(self):
        return 2 ** (len(self.xspace) * len(self.yspace)) - 1

    def __len__(self):
        return self.max_key + 1

    def k2i(self, key):
        if not isinstance(key, numbers.Integral):
            raise TypeError("Key {!s} is not an integer".format(key))
        if key < 0 or key >= len(self):
            raise IndexError("Key {!s} is out of range".format(key))

        if not isinstance(key, int):
            key = int(key)
        return int2bitmatrix(key, self.state_shape)

    def i2k(self, item):
        if not isinstance(item, self.item_type):
            raise TypeError("Item {!s}"
                            " is not a {!s}".format(item, self.item_type))
        if item.shape != self.state_shape:
            raise ValueError("Item {!s}"
                             " does not have"
                             " the correct"
                             " shape {!s}".format(item, self.state_shape))

        return bitmatrix2int(item)

    def make_item(self, bitmatrix):
        bitmatrix = np.asarray(bitmatrix)
        if bitmatrix.dtype is not np.dtype(bool):
            assert(np.all(np.logical_or(bitmatrix == 0, bitmatrix == 1)))
            bitmatrix = np.asarray(bitmatrix, dtype=bool)
        if bitmatrix in self:
            return bitmatrix
        else:
            raise ValueError("The bit matrix does not belong to this space")

    def pack(self, state):
        # Named field to avoid conversion to (bool NxM)
        out_dtype = np.dtype([('R', bool, self.state_shape)])
        out = np.empty(tuple(), dtype=out_dtype)
        out['R'] = state
        return out

    def unpack(self, state):
        return np.asarray(state['R'], dtype=bool)

    def at(self, state, x, y):
        xi = self.xspace.index(x)
        yi = self.yspace.index(y)
        return float(state[xi, yi])

    def set(self, state, x, y, val):
        xi = self.xspace.index(x)
        yi = self.yspace.index(y)
        state[xi, yi] = bool(val)

    def maybe_toggle(self, state, x, y):
        xi = self.xspace.index(x)
        yi = self.yspace.index(y)
        self.maybe_toggle_indices(state, xi, yi)

    def maybe_toggle_indices(self, state, xi, yi):
        if state[xi, yi]:
            p = self.poff[xi, yi]
        else:
            p = self.pon[xi, yi]

        if random.random() < p:
            state[xi, yi] = not state[xi, yi]


class GridSpace(ProductSpace):
    def __init__(self, x_min, x_max, x_steps,
                 y_min, y_max, y_steps,
                 ret_steps,
                 pon, poff,
                 b_steps=None, b_bins=None,
                 agents=1):
        if agents < 1:
            raise ValueError("At least one agent")

        self.x = QuantizedSpace(a=x_min, b=x_max, steps=x_steps)
        self.y = QuantizedSpace(a=y_min, b=y_max, steps=y_steps)
        self.b = QuantizedSpace(a=0, b=1, steps=b_steps, bins=b_bins)
        self.prev_a = GridActionSpace(agents).single
        self.ret = NamedSpace(*list(range(ret_steps)))
        grid = ProductSpace(x=self.x, y=self.y, b=self.b, prev_a=self.prev_a)
        self.single = UnionSpace(grid=grid, ret=self.ret)

        self.R = GridRewardSpace(self.x, self.y, pon, poff)

        self.agent_keys = _agent_keys(agents)
        multiag_kw = {ag: self.single for ag in self.agent_keys}
        multiag_kw['R'] = self.R
        super().__init__(**multiag_kw)

    @property
    def x_steps(self):
        return self.x.steps

    @property
    def x_min(self):
        return self.x.a

    @property
    def x_max(self):
        return self.x.b

    @property
    def y_steps(self):
        return self.y.steps

    @property
    def y_min(self):
        return self.y.a

    @property
    def y_max(self):
        return self.y.b

    @property
    def b_steps(self):
        return self.b.steps

    @property
    def b_bins(self):
        return self.b.bins

    @property
    def b_min(self):
        return self.b.a

    @property
    def b_max(self):
        return self.b.b

    @property
    def ret_steps(self):
        return len(self.ret)

    @property
    def agents(self):
        return len(self.agent_keys)


class GridObservationSpace(UnionSpace):
    @staticmethod
    def make_each_agent(fullspace, *args, **kwargs):
        return {ag: GridObservationSpace(fullspace, agent=ag,
                                         *args, **kwargs)
                for ag in fullspace.agent_keys}

    def __init__(self, fullspace, agent, d_steps, theta_steps):
        if agent not in fullspace.agent_keys:
            raise ValueError("The agent {!s} does not exist".format(agent))

        self.fullspace = fullspace
        self.agent = agent

        x_span = fullspace.x.b - fullspace.x.a
        y_span = fullspace.y.b - fullspace.y.a
        min_d = min(fullspace.x.stepsize, fullspace.y.stepsize)
        max_d = np.hypot(x_span, y_span)
        d_bins = np.geomspace(min_d, max_d, d_steps)
        d_bins[-1] = max_d  # Fix approximation
        self.d = QuantizedSpace(a=0, b=max_d, bins=d_bins)
        self.theta = QuantizedSpace(a=-3/4*math.pi, b=5/4*math.pi,
                                    steps=theta_steps)
        self.r = NamedSpace(True, False)

        grid_kw = {'d': self.d, 'theta': self.theta, 'r': self.r}
        for k, s in fullspace.sub(self.agent).sub('grid').spaces.items():
            assert(k not in grid_kw)
            grid_kw[k] = s

        grid = ProductSpace(**grid_kw)
        super().__init__(grid=grid, ret=self.fullspace.ret)

    @property
    def agent_keys(self):
        return self.fullspace.agent_keys

    def _min_dist(self, fullstate):
        g = fullstate[self.agent]['grid']
        x = g['x']
        y = g['y']

        def dist(otherag):
            og = fullstate[otherag].get('grid')
            if og is None:
                return self.d.b

            otherx = og['x']
            othery = og['y']
            return np.hypot(x - otherx, y - othery)

        dists = list(map(dist, self.fullspace.agent_keys))
        i_min = np.argmin(dists)
        ag_min = utils.iter.nth(self.fullspace.agent_keys, i_min)
        return (dists[i_min], ag_min)

    def reduce(self, fullstate):
        if 'grid' not in fullstate[self.agent]:
            return self.make_item(ret=fullstate[self.agent]['ret'])

        grid_kw = fullstate[self.agent]['grid']

        x = fullstate[self.agent]['grid']['x']
        y = fullstate[self.agent]['grid']['y']

        (d, ag_close) = self._min_dist(fullstate)
        og = fullstate[ag_close].get('grid')
        if og is None or self.d.index(d) == 0:
            theta = 0.0
        else:
            otherx = og['x']
            othery = og['y']
            theta = np.arctan2(othery - y, otherx - x)
            if theta <= -3/4*math.pi:
                theta += 2*math.pi

        grid_kw['d'] = d
        grid_kw['theta'] = theta
        grid_kw['r'] = self.fullspace.R.at(fullstate['R'], x, y)
        return self.make_item(grid=grid_kw)


class GridMDP:
    def __init__(self, grid_radius, grid_steps,
                 avg_steps_on,
                 avg_steps_off,
                 d_steps,
                 theta_steps,
                 b_steps=None, b_bins=None,
                 agents=1,
                 T=None):
        self.power_hover = 338  # W
        self.power_hover_std = 19.79e-3 * self.power_hover
        self.power_move = 848  # W
        self.power_move_std = 76.92e-3 * self.power_move

        self.Q = 5.2 * 3600
        self.Vnom = 14.8

        self.move_speed = 8  # m/s
        self.return_speed = 13  # m/s

        if T is None:
            self.T = (2*grid_radius / grid_steps) / self.move_speed
        else:
            assert(T > 0)
            self.T = T

        maxdist = np.hypot(grid_radius, grid_radius)
        ret_steps = math.ceil((maxdist / self.return_speed) / self.T)

        self.state_space = GridSpace(agents=agents,
                                     x_steps=grid_steps,
                                     x_min=-grid_radius,
                                     x_max=grid_radius,
                                     y_steps=grid_steps,
                                     y_min=-grid_radius,
                                     y_max=grid_radius,
                                     b_steps=b_steps,
                                     b_bins=b_bins,
                                     ret_steps=ret_steps,
                                     pon=1/avg_steps_off,
                                     poff=1/avg_steps_on)
        self.action_space = GridActionSpace(agents=agents)

        # Not used: always one step
        stay_steps = 1
        gamma = 1

        if agents > 1 and stay_steps > 1:
            raise ValueError("stay_steps for multiple agents"
                             " is not implemented")
        self.stay_steps = stay_steps
        self.gamma = gamma

        gspace = self.state_space.single.sub('grid')
        start_one = gspace.make_item(x=0.0, y=0.0, b=1.0, prev_a='return')
        starting_state_kw = {ag: {'grid': start_one}
                             for ag in self.state_space.agent_keys}

        R = self.state_space.sub('R')
        xi = self.state_space.x.index(0.0)
        yi = self.state_space.y.index(0.0)
        R.pon[xi, yi] = 0
        R.poff[xi, yi] = 1
        start_R = np.ones(R.state_shape, dtype=bool)
        start_R[xi, yi] = False
        start_R = R.make_item(start_R)
        starting_state_kw['R'] = start_R

        self.starting_state = self.state_space.make_item(**starting_state_kw)
        self.starting_state_single = start_one

        if self.gamma == 1:
            self.max_reward = 1.0 * self.stay_steps
        else:
            self.max_reward = (1.0 *
                               (1 - self.gamma ** self.stay_steps) /
                               (1 - self.gamma))

        self._obs_spaces = GridObservationSpace.make_each_agent(
            self.state_space,
            d_steps=d_steps,
            theta_steps=theta_steps
        )

        self.state = copy.deepcopy(self.starting_state)
        self.time = 0

        va_cache_size = 8192
        self._va_cache = SafeLRUCache(va_cache_size)
        self._btrp_cache = SeededLRUCache(8192)
        orig_batt_func = type(self)._battery_trans_prob.__wrapped__
        for b in self.state_space.b:
            for a in self.action_space.single:
                key = self._btrp_key(b, a, prob_thresh=1e-2)
                newb = orig_batt_func(self, b, a, prob_thresh=1e-2)
                self._btrp_cache.seed(key, newb)

    def reset(self):
        self.state = copy.deepcopy(self.starting_state)
        self.time = 0
        self._va_cache.clear()
        self._btrp_cache.clear()

    @property
    def agent_keys(self):
        return self.state_space.agent_keys

    @property
    def agents(self):
        return len(self.agent_keys)

    def __xy_eq(self, lhs, rhs):
        return (self.state_space.x.items_eq(lhs['x'], rhs['x']) and
                self.state_space.y.items_eq(lhs['y'], rhs['y']))

    def valid_actions(self, state=None, agent=None):
        if state is None:
            state = self.state
        vas = self._cached_valid_actions(state)
        if agent is None:
            return vas
        else:
            return vas[agent]

    def _va_cache_key(self, state):
        k = (self.state_space.index(state), )
        # k += tuple(state[ag]['grid']['b'] if 'grid' in state[ag] else None
        #            for ag in self.agent_keys)
        return k

    def _get_va_cache(self):
        return self._va_cache

    @cachetools.cachedmethod(_get_va_cache, key=_va_cache_key)
    def _cached_valid_actions(self, state):
        # No case for an empty battery because the state goes directly in the
        # retchain. s - a -> {ret: n} for any a if new_s[b] == 0
        actions = dict()
        xsp = self.state_space.x
        ysp = self.state_space.y
        asp = self.action_space.single

        # Split returning from regular
        (ret, nonret) = partition(self.agent_keys,
                                  lambda ag: 'ret' in state[ag])

        # Force return to start
        for ag in ret:
            actions[ag] = [asp.make_item('return')]

        # Handle collisions at the start position
        def start_filter_func(ag):
            return self.__xy_eq(state[ag]['grid'],
                                self.starting_state[ag]['grid'])
        ag_start = list(filter(start_filter_func, nonret))

        if len(ag_start) > 1:
            # Assign the 4 move actions cycling through the agents
            move_names = sorted(['up', 'right', 'down', 'left'])
            ag_cycle = itertools.cycle(sorted(ag_start))
            for ag, m in zip(ag_cycle, move_names):
                ag_acts = actions.setdefault(ag, [])
                a = self.action_space.sub(ag).make_item(m)
                ag_acts.append(a)

            # Also add the stay actions
            for ag in ag_start:
                stay = self.action_space.sub(ag).make_item('stay')
                actions[ag].append(stay)

        # If not at start, all actions by default
        nonret_acts = asp[:]
        nonret_acts.remove('return')
        for ag in nonret:
            actions.setdefault(ag, copy.deepcopy(nonret_acts))

        # Remove wrong actions
        for ag in nonret:
            x = state[ag]['grid']['x']
            y = state[ag]['grid']['y']

            # Remove actions at the border
            if xsp.items_eq(x, xsp.min):
                remove_existing(actions[ag], 'left')
            elif xsp.items_eq(x, xsp.max):
                remove_existing(actions[ag], 'right')
            if ysp.items_eq(y, ysp.min):
                remove_existing(actions[ag], 'down')
            elif ysp.items_eq(y, ysp.max):
                remove_existing(actions[ag], 'up')

            # Remove possible collisions
            for otherag in filter(lambda oag: oag != ag, nonret):
                xdiff = xsp.index(x) - xsp.index(state[otherag]['grid']['x'])
                ydiff = ysp.index(y) - ysp.index(state[otherag]['grid']['y'])

                if xdiff in [-1, -2] and ydiff == 0:
                    remove_existing(actions[ag], 'right')
                    remove_existing(actions[otherag], 'left')
                elif xdiff in [1, 2] and ydiff == 0:
                    remove_existing(actions[ag], 'left')
                    remove_existing(actions[otherag], 'right')
                elif ydiff in [-1, -2] and xdiff == 0:
                    remove_existing(actions[ag], 'up')
                    remove_existing(actions[otherag], 'down')
                elif ydiff in [1, 2] and xdiff == 0:
                    remove_existing(actions[ag], 'down')
                    remove_existing(actions[otherag], 'up')
                elif xdiff == -1 and ydiff == -1:
                    remove_existing(actions[ag], 'up')
                    remove_existing(actions[ag], 'right')
                    remove_existing(actions[otherag], 'down')
                    remove_existing(actions[otherag], 'left')
                elif xdiff == -1 and ydiff == 1:
                    remove_existing(actions[ag], 'down')
                    remove_existing(actions[ag], 'right')
                    remove_existing(actions[otherag], 'up')
                    remove_existing(actions[otherag], 'left')
                elif xdiff == 1 and ydiff == -1:
                    remove_existing(actions[ag], 'left')
                    remove_existing(actions[ag], 'up')
                    remove_existing(actions[otherag], 'right')
                    remove_existing(actions[otherag], 'down')
                elif xdiff == 1 and ydiff == 1:
                    remove_existing(actions[ag], 'left')
                    remove_existing(actions[ag], 'down')
                    remove_existing(actions[otherag], 'right')
                    remove_existing(actions[otherag], 'up')

        assert(all(len(actions[ag]) > 0 for ag in self.agent_keys))
        return actions

    def observation_space(self, agent):
        return self._obs_spaces[agent]

    def observation(self, agent, state=None):
        """Return an observation of the state from the POV of the given agent

        """
        if state is None:
            state = self.state

        return self.observation_space(agent).reduce(state)

    def _check_valid_action(self, action, state=None):
        actions = self.valid_actions(state)
        for ag in self.agent_keys:
            if action[ag] not in actions[ag]:
                verr_msg = ("Action {!s} for agent {!s} is not valid in"
                            " state {!s}".format(action, ag, state))
                raise ValueError(verr_msg)

    def battery_usage(self, current_b, action, avgonly=False):
        if action == 'stay':
            mu = self.power_hover
            std = self.power_hover_std
        else:
            mu = self.power_move
            std = self.power_move_std

        if avgonly:
            power = mu
        else:
            power = np.random.normal(mu, std)
        while power < 0:
            power = np.random.normal(mu, std)

        # Nominal voltage
        return power / self.Vnom * self.T / self.Q

    def reward(self, s=None, a=None, new_s=None):
        if s is None:
            s = self.state
        if a is None:
            raise ValueError("a is required")
        if new_s is None:
            raise ValueError("new_s is required")

        rvec = dict()
        for ag in self.agent_keys:
            rvec[ag] = self.indep_reward(ag, s, a[ag], new_s)
        return rvec

    def indep_reward(self, agent, state, action, new_state):
        # assert(agent in self.agent_keys)
        # assert(state in self.state_space)
        assert(action in self.action_space.single)
        # assert(new_state in self.state_space)

        if 'grid' in new_state[agent] and action == 'stay':
            x = new_state[agent]['grid']['x']
            y = new_state[agent]['grid']['y']
            return self.state_space.R.at(new_state['R'], x, y)
        else:
            return 0.0

    # def average_indep_reward(self, agent, state, action):
    #     ns_probs = self.indep_transition_prob(agent, state, action[agent])
    #     (nss, probs) = utils.iter.unstar(ns_probs, lazy=False)
    #     rfunc = functools.partial(self.indep_reward, agent, state, action)
    #     rewards = [rfunc(ns) for ns in nss]
    #     return np.vdot(rewards, probs)

    def _determ_agent_trans(self, state_ag, action, empty_battery):
        """Deterministic part of the agent's next state.

           state_ag -- Current state, must be from state_space.single
           action -- Agent's action (single action)
           empty_battery -- Is the battery at the next state empty?

           Returns a new agent state with b set to None (or absent).
        """
        # assert(state_ag in self.state_space.single)

        if 'ret' in state_ag:
            if state_ag['ret'] != 0:
                # returning agents don't modify anything else (incl. R)
                new_state = copy.deepcopy(state_ag)
                new_state['ret'] -= 1
                return new_state
            else:
                # resume at start pos (x, y, b) with prev_a = 'return'
                new_state = {'grid': copy.deepcopy(self.starting_state_single)}
                new_state['grid']['prev_a'] = action
                new_state['grid']['b'] = None
                return new_state
        elif empty_battery:
            dist = math.hypot(state_ag['grid']['x'], state_ag['grid']['y'])
            ret_steps = math.ceil(dist / self.return_speed / self.T)
            assert(ret_steps <= self.state_space.ret_steps)
            # Could empty when exiting from start
            ret_steps = max(1, ret_steps)
            new_state = {'ret': ret_steps - 1}
            return new_state
        else:
            new_state = copy.deepcopy(state_ag)
            new_state['grid']['b'] = None
            new_state['grid']['prev_a'] = action
            X = self.state_space.x
            Y = self.state_space.y
            xi = X.index(state_ag['grid']['x'])
            yi = Y.index(state_ag['grid']['y'])
            if action == 'up':
                new_state['grid']['y'] = Y[yi + 1]
            elif action == 'down':
                new_state['grid']['y'] = Y[yi - 1]
            elif action == 'left':
                new_state['grid']['x'] = X[xi - 1]
            elif action == 'right':
                new_state['grid']['x'] = X[xi + 1]
            # elif action == 'stay':
            #     pass
            return new_state

    def _move_state(self, state, action):
        new_state = copy.deepcopy(state)
        B = self.state_space.b
        X = self.state_space.x
        Y = self.state_space.y
        R = self.state_space.R
        (toggle_xi, toggle_yi) = np.where(np.logical_not(state['R']))
        where_toggle = set(zip(toggle_xi, toggle_yi))
        for ag in self.agent_keys:
            # Agents with empty battery
            if 'ret' in state[ag]:
                if state[ag]['ret'] != 0:
                    # returning agents don't modify anything else (incl. R)
                    new_state[ag]['ret'] -= 1
                else:
                    # resume at start pos (x, y, b) with prev_a = 'return'
                    new_state[ag] = copy.deepcopy(self.starting_state[ag])
                    new_state[ag]['grid']['prev_a'] = action[ag]
            else:
                old_b = state[ag]['grid']['b']
                # Recharge when staying at start
                if (action[ag] == 'stay' and
                    self.__xy_eq(state[ag]['grid'], self.starting_state_single)):
                    new_b = B.make_item(1)
                else:
                    b_delta = self.battery_usage(old_b, action[ag])
                    new_b = B.make_item(max(0.0, float(old_b) - b_delta))

                if B.items_eq(new_b, 0):
                    dist = math.hypot(state[ag]['grid']['x'], state[ag]['grid']['y'])
                    ret_steps = math.ceil(dist / self.return_speed / self.T)
                    assert(ret_steps <= self.state_space.ret_steps)
                    new_state[ag] = {'ret': max(0, ret_steps - 1)}
                else:
                    new_state[ag]['grid']['b'] = new_b
                    new_state[ag]['grid']['prev_a'] = action[ag]
                    xi = X.index(state[ag]['grid']['x'])
                    yi = Y.index(state[ag]['grid']['y'])
                    if action[ag] == 'up':
                        new_state[ag]['grid']['y'] = Y[yi + 1]
                    elif action[ag] == 'down':
                        new_state[ag]['grid']['y'] = Y[yi - 1]
                    elif action[ag] == 'left':
                        new_state[ag]['grid']['x'] = X[xi - 1]
                    elif action[ag] == 'right':
                        new_state[ag]['grid']['x'] = X[xi + 1]
                    elif action[ag] == 'stay':
                        where_toggle.add((xi, yi))

        for xi, yi in where_toggle:
            R.maybe_toggle_indices(new_state['R'], xi, yi)
        return new_state

    def do_action(self, a):
        self._check_valid_action(a, self.state)
        s = self.state
        new_s = self._move_state(s, a)
        r = self.reward(s, a, new_s)
        self.state = new_s
        self.time += 1
        return (s, r, new_s)

    def simulate_indep_action(self, agent, s, a):
        other_a = self.random_action(s)
        other_a[agent] = a
        a = other_a

        if self.agents == 1 and a['ag0'] == 'stay':
            repeat = self.stay_steps
        else:
            repeat = 1
        total_r = {ag: 0 for ag in self.agent_keys}
        orig_state = s
        for rep in range(repeat):
            try:
                self._check_valid_action(a, s)
            except ValueError as e:
                if rep == 0:
                    raise e
                else:
                    break
            new_s = self._move_state(s, a)
            r = self.reward(s, a, new_s)
            for ag in self.agent_keys:
                total_r[ag] += (self.gamma ** rep) * r[ag]
            s = new_s
        return (total_r[agent], new_s)

    def _state_hash(self, state):
        k = (self.state_space.index(state), )
        # Different for real batt values
        k += tuple(state[ag]['grid']['b'] if 'grid' in state[ag] else None
                   for ag in self.agent_keys)
        return hash(k)

    def indep_transition_prob(self, agent, state, action):
        valid_actions_others = self.valid_actions(state)
        del valid_actions_others[agent]
        va_lens = [len(va) for va in valid_actions_others.values()]
        va_prod_len = np.product(va_lens)

        all_probs = dict()
        hashwrapper = utils.misc.hash_wrap_factory(self._state_hash)

        def wrap_scale_func(s, p):
            return (hashwrapper(s), p / va_prod_len)

        for others in itertools.product(*valid_actions_others.values()):
            fullaction = dict(zip(valid_actions_others.keys(), others))
            fullaction[agent] = action

            probs = self.transition_prob(state, fullaction,
                                         approx=True, agent=agent)

            wrapped_scaled_probs = starmap(wrap_scale_func, probs)
            for ws, p in wrapped_scaled_probs:
                oldp = all_probs.get(ws, 0)
                all_probs[ws] = oldp + p

        return starmap(lambda ws, p: (ws.wrapped, p),
                                 all_probs.items())

    def transition_prob(self, state, action, approx=False, agent=None):
        """Transition probabilities.

           Returns an iterable of (s', p) where p = T(s, joint a, s').
           If `approx` is True only considers the point of view of `agent`:
           other agents have deterministic battery, reward matrix changes only
           under `agent` and the locations at distance of one step.
        """
        assert(self.stay_steps == 1)

        battery_probs = self._joint_battery_trans_prob(state, action,
                                                       prob_thresh=1e-2,
                                                       approx=approx,
                                                       main_agent=agent)
        def bprob2sprob(jb_idxs, p):
            """Make full state without reward matrix from joint batteries."""
            s = {'R': None}
            for ag, new_b_idx in jb_idxs.items():
                new_sag = self._determ_agent_trans(state[ag], action[ag],
                                                   new_b_idx == 0)
                if 'grid' in new_sag:
                    new_sag['grid']['b'] = self.state_space.b[new_b_idx]
                s[ag] = new_sag
            return (s, p)

        newstate_probs = starmap(bprob2sprob, battery_probs)

        def addR2sprob(newstate, p):
            """Produce an iterable over the joint prob. of newstate and
               all possible reward matrices R.
            """
            stay_positions = ([], [])
            next_positions = ([], [])
            for ag in self.agent_keys:
                if approx and agent is not None and ag != agent:
                    # Ignore effect of other agents
                    continue

                if 'grid' not in newstate[ag]:
                    # Agent is returning: no effect on reward
                    continue

                nx = newstate[ag]['grid']['x']
                ny = newstate[ag]['grid']['y']
                nxi = self.state_space.x.index(nx)
                nyi = self.state_space.y.index(ny)
                next_positions[0].append(nxi)
                next_positions[1].append(nyi)

                if action[ag] == 'stay':
                    stay_positions[0].append(nxi)
                    stay_positions[1].append(nyi)

            if approx:
                next_pos_set = set(zip(*next_positions))

                def filter_on(xi, yi):
                    return (xi, yi) in next_pos_set
            else:
                filter_on = None

            R_probs = reward_transition_prob(state['R'],
                                            self.state_space.R,
                                            agents_pos=stay_positions,
                                            filter_on=filter_on)
            def R_news_product(Ridx, Rp):
                """Mix one reward matrix with one newstate"""
                R = self.state_space.R[Ridx]
                full_newstate = copy.deepcopy(newstate)
                full_newstate['R'] = R
                return (full_newstate, p * Rp)

            return starmap(R_news_product, R_probs)
        return itertools.chain.from_iterable(starmap(addR2sprob, newstate_probs))

    transition_prob_iter = transition_prob

    def _get_btrp_cache(self):
        return self._btrp_cache

    def _btrp_key(self, s_batt, action, prob_thresh=None):
        return (s_batt, action, prob_thresh)

    @cachetools.cachedmethod(_get_btrp_cache, key=_btrp_key)
    def _battery_trans_prob(self, s_batt, action, prob_thresh=1e-3):
        s_batt_idx = self.state_space.b.index(s_batt)
        lower_bins = np.asarray(self.state_space.b.bins[:s_batt_idx + 1])

        deltasoc_scaling = self.T / (self.Vnom * self.Q)
        if action == 'stay':
            mu = self.power_hover * deltasoc_scaling
            sigma = self.power_hover_std * deltasoc_scaling
        else:
            mu = self.power_move * deltasoc_scaling
            sigma = self.power_move_std * deltasoc_scaling

        deltasoc_cdf = stats.norm.cdf(s_batt - lower_bins, loc=mu, scale=sigma)

        interval_probs = [1 - deltasoc_cdf[0]]
        interval_probs += [deltasoc_cdf[i] - deltasoc_cdf[i+1]
                           for i in range(len(lower_bins)-1)]
        idx_prob = starfilter(lambda i, p: p >= prob_thresh,
                                         enumerate(interval_probs))
        return tuple(idx_prob)

    def _joint_battery_trans_prob(self, state, action, prob_thresh=1e-3,
                                  approx=False, main_agent=None):
        batt_probs = dict()
        for ag in self.agent_keys:
            if 'grid' not in state[ag]:
                if state[ag]['ret'] > 0:
                    batt_probs[ag] = [(0, 1.0)]
                else:
                    batt_probs[ag] = [(self.state_space.b_steps - 1, 1.0)]
            elif action[ag] == 'stay' and self.__xy_eq(state[ag]['grid'], self.starting_state_single):
                batt_probs[ag] = [(self.state_space.b_steps - 1, 1.0)]
            elif approx and main_agent is not None and ag != main_agent:
                deltasoc = self.battery_usage(
                    state[ag]['grid']['b'], action[ag], avgonly=True)
                newb_idx = self.state_space.b.index(
                    max(0, state[ag]['grid']['b'] - deltasoc))
                batt_probs[ag] = [(newb_idx, 1.0)]
            else:
                batt_probs[ag] = self._battery_trans_prob(
                    state[ag]['grid']['b'], action[ag], prob_thresh=prob_thresh)

        def joint_prob(prob_tuples):
            (indices, probs) = utils.iter.unzip(prob_tuples, lazy=False)
            joint_batt = dict(zip(batt_probs.keys(), indices))
            return (joint_batt, np.product(probs))

        bprods = itertools.product(*batt_probs.values())
        joint_batt_probs = map(joint_prob, bprods)
        return starfilter(lambda jb, p: p >= prob_thresh, joint_batt_probs)

    def random_action(self, state=None):
        actions = self.valid_actions(state)
        a_kw = {ag: random.choice(ag_acts) for ag, ag_acts in actions.items()}
        return self.action_space.make_item(**a_kw)
