import collections.abc
import logging

from billiard.exceptions import WorkerLostError
from celery import group
from celery.exceptions import TimeoutError
from celery.result import ResultSet

logger = logging.getLogger(__name__)


def collect_async_results(func, results, timeout, give_index=False):
    waiting = list(enumerate(results))
    out = [None for _ in range(len(waiting))]
    while len(waiting) > 0:
        new_waiting = []
        for i, r in waiting:
            try:
                x = r.get(timeout)
            except (TimeoutError, WorkerLostError):
                new_waiting.append((i, r))
                continue
            if give_index:
                out[i] = func(i, x)
            else:
                out[i] = func(x)
        waiting = new_waiting
    return out


def retry_group(signatures, callback=None, timeout=None, use_workers=True):
    out = dict()
    if use_workers:
        results = group(iter(signatures)).apply_async()
    else:
        results = group(iter(signatures)).apply()
    res_idx = {r.id: r for r in results}
    out_idx = {r.id: i for i, r in enumerate(results)}
    while len(out) < len(signatures):
        failed_tasks = []
        resubmit_res = []

        def inner_callback(task_id, value):
            res = res_idx[task_id]
            if res.status == 'SUCCESS':
                idx = out_idx[task_id]
                out[idx] = (res, value)
                if callback is not None:
                    callback(task_id, value)
            else:
                logger.warning("Task {!s} failed"
                               " with {!s}: {!s}".format(task_id,
                                                         type(value).__name__,
                                                         value))
                failed_tasks.append(task_id)
                idx = out_idx[task_id]
                sig = signatures[idx]
                if use_workers:
                    newres = sig.apply_async()
                else:
                    newres = sig.apply()
                resubmit_res.append((idx, newres))

        done = []
        cumulative_wait = {r.id: 0.0 for r in results}
        while len(done) < len(results):
            for r in results:
                if r.id in done:
                    continue

                try:
                    if timeout is None:
                        r_to = 1.0
                    else:
                        r_to = timeout / 10
                        r_to = min(r_to, timeout - cumulative_wait[r.id])
                        r_to = max(r_to, 1.0)

                    r_value = r.get(timeout=r_to, propagate=False)
                except Exception as exc:
                    for rid in cumulative_wait:
                        cumulative_wait[rid] += r_to
                    if timeout is not None and cumulative_wait[r.id] >= timeout:
                        r.revoke()
                        r.forget()
                        inner_callback(r.id, exc)
                        done.append(r.id)
                else:
                    inner_callback(r.id, r_value)
                    done.append(r.id)

        if len(failed_tasks) > 0:
            logger.info("Resubmitted {:d}"
                        " failed tasks".format(len(failed_tasks)))
        results = ResultSet([r for _, r in resubmit_res], app=results.app)
        res_idx = {r.id: r for r in results}
        out_idx = {r.id: i for i, r in resubmit_res}

    return [v for _, v in sorted(out.items())]


def retry_task(signature, timeout=None, callback=None):
    out = retry_group([signature], timeout=timeout, callback=callback)
    return out[0]


def revoke_all(async_result, terminate=False):
    if not isinstance(async_result, collections.abc.Iterable):
        async_result = [async_result]

    for r in async_result:
        while r is not None:
            r.revoke(terminate=terminate)
            r = r.parent


def parents_chain(result):
    parents = [result]
    top = result
    while top.parent is not None:
        parents.append(top.parent)
        top = top.parent
    return reversed(parents)
