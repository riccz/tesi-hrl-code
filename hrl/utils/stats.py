import contextlib
import math
import time

from scipy import stats


def clipped_norm_integral(a, b, mu=0.0, sigma=1.0, k=3):
    cdf = stats.norm(loc=mu, scale=sigma).cdf

    low = max(a, mu - k*sigma)
    high = min(b, mu + k*sigma)
    assert(high >= low)

    return (cdf(high) - cdf(low)) / (cdf(mu + k*sigma) - cdf(mu - k*sigma))


class AverageCounter:
    def __init__(self, keep_all=False):
        self.avg = 0.0
        self.avg_sq = 0.0
        self.sum_w = 0.0

        self.keep_all = keep_all
        self.samples = []

    def add(self, x, w=1.0):
        if self.keep_all:
            self.samples.append((x, w))

        new_sumw = self.sum_w + w
        self.avg = (self.sum_w * self.avg +
                    x * w) / new_sumw
        self.avg_sq = (self.sum_w * self.avg_sq +
                       (x ** 2) * w) / new_sumw
        self.sum_w = new_sumw

    @property
    def average(self):
        return self.avg

    @property
    def variance(self):
        return self.avg_sq - (self.avg ** 2)

    @property
    def std_dev(self):
        return math.sqrt(self.variance)

    @property
    def total_weight(self):
        return self.sum_w

    def reset(self):
        self.avg = 0.0
        self.avg_sq = 0.0
        self.sum_w = 0.0
        self.samples = []


@contextlib.contextmanager
def measure_time(counter, on_high=None, high_N=100, high_thresh=3.3):
    start = time.process_time()
    yield
    delta = time.process_time() - start
    counter.add(delta)
    if on_high is None:
        return
    elif counter.total_weight >= high_N:
        avg = counter.average
        std = counter.std_dev
        if delta - avg > high_thresh * std:
            on_high(delta=delta, avg=avg, std=std)
