import collections
import functools
import itertools
import logging
import lzma
import os
import pathlib
import pickle
import random
import re
import string
import subprocess
import time
import warnings
import zlib

import numpy as np

logger = logging.getLogger(__name__)

IMMUTABLE_TYPES = (int, str, bool, float, tuple)


def deprecated(func):
    """This is a decorator which can be used to mark functions as
    deprecated. It will result in a warning being emitted when the
    function is used.

    """

    @functools.wraps(func)
    def new_func(*args, **kwargs):
        # warnings.simplefilter('always', DeprecationWarning)  # turn off filter
        warnings.warn("Call to deprecated function {}.".format(func.__name__),
                      category=DeprecationWarning,
                      stacklevel=2)
        # warnings.simplefilter('default', DeprecationWarning)  # reset filter
        return func(*args, **kwargs)
    return new_func

def methdispatch(func):
    dispatcher = functools.singledispatch(func)
    def wrapper(*args, **kw):
        return dispatcher.dispatch(type(args[1]))(*args, **kw)
    wrapper.register = dispatcher.register
    wrapper.registry = dispatcher.registry
    functools.update_wrapper(wrapper, func)
    return wrapper


def remove_existing(seq, item):
    try:
        seq.remove(item)
        return True
    except (ValueError, KeyError):
        return False


class ProgressCounter:
    def __init__(self, tot):
        self.cnt = 0
        self.tot = tot
        self.lastprog = None
        self.prog = 0.0

    def show_progress(self, s, a, r, ns, t, history):
        self.cnt += 1
        self.prog = self.cnt / self.tot

        now = time.monotonic()
        if (self.lastprog is None
            or now - self.lastprog >= 1
            or self.prog == 1):
            self.lastprog = now
            print("\rDone {:.2%}".format(self.prog), end='')
        if self.prog == 1: print()

    @property
    def progress(self):
        return self.prog


class cached_property:
    def __init__(self, factory):
        self._attr_name = factory.__name__
        self._factory = factory

    def __get__(self, instance, owner):
        attr = self._factory(instance)
        setattr(instance, self._attr_name, attr)
        return attr


class cached_method:
    def __init__(self, factory):
        self._factory = factory
        self._value = None
        self._hasvalue = False

    def __get__(self, instance, owner):
        return functools.partial(self, instance)

    def __call__(self, instance):
        if not self._hasvalue:
            self._value = self._factory(instance)
        return self._value


class HashWrap:
    __slots__ = ['hashfunc', 'wrapped']

    def __init__(self, x, hashfunc):
        self.hashfunc = hashfunc
        self.wrapped = x

    def __hash__(self):
        return self.hashfunc(self.wrapped)


def hash_wrap_factory(hashfunc):
    return functools.partial(HashWrap, hashfunc=hashfunc)
