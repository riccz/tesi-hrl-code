import itertools
import logging
import logging.config
import lzma
import operator
import os
import pathlib
import pickle
import random
import re
import string
import subprocess
import time
import warnings
import zlib

import boto3
import botocore

from . import pack
from .. import GIT_SHA1

with warnings.catch_warnings():
    warnings.simplefilter("ignore", FutureWarning)
    import h5py

logger = logging.getLogger(__name__)


def upload_file(fname, bucket, key, check_exists=True, delete_source=False):
    s3 = boto3.resource('s3')
    obj = s3.Object(bucket, key)

    if check_exists:
        try:
            obj.load()
        except botocore.exceptions.ClientError as e:
            if e.response['Error']['Code'] != '404':
                raise e

    obj.upload_file(str(fname))

    if delete_source:
        pathlib.Path(fname).unlink()


def upload(body, bucket, key, check_exists=True):
    s3 = boto3.resource('s3')
    obj = s3.Object(bucket, key)

    if check_exists:
        try:
            obj.load()
        except botocore.exceptions.ClientError as e:
            if e.response['Error']['Code'] != '404':
                raise e

    obj.put(body)


def random_string(n):
    alph = string.ascii_lowercase + string.ascii_uppercase + string.digits
    return ''.join(random.choice(alph) for _ in range(n))


def aws_upload_data(fname, name, delete_source=False):
    sha1 = GIT_SHA1
    randkey = random_string(16)
    key = "data/{!s}/{!s}/{!s}".format(name, sha1[:8], randkey)
    upload_file(fname, 'hrl.zanol.eu', key, delete_source=delete_source)
    return key


def aws_fetch_data(key):
    logger.debug("Fetching AWS key '{!s}'".format(key))
    path = pathlib.Path.home().joinpath(".cache", "hrl_fetch_data", key)
    if not path.exists():
        logger.debug("Key '{!s}' is not cached, downloading".format(key))
        config = botocore.client.Config(read_timeout=300)
        s3 = boto3.client('s3', config=config)
        path.parent.mkdir(parents=True, exist_ok=True)
        tmpfile = str(path) + ".tmp" + random_string(16)
        s3.download_file('hrl.zanol.eu', key, tmpfile)
        pathlib.Path(tmpfile).rename(path)
        logger.debug("Download of key '{!s}' OK".format(key))
    return h5py.File(str(path), 'r')


def aws_fetch_data_prefix(prefix, filter_func=None):
    bucket = boto3.resource('s3').Bucket('hrl.zanol.eu')
    items = dict()
    for obj in bucket.objects.filter(Prefix=prefix):
        if filter_func is None or filter_func(obj):
            items[obj.key] = aws_fetch_data(obj.key)
    return items


def fetch_data_by_commit(label, commits):
    files = dict()
    for c in commits:
        short = c[:8]
        prefix = "data/{!s}/{!s}".format(label, short)
        c_files = aws_fetch_data_prefix(prefix)
        for k, f in c_files.items():
            if get_hdf5_commit(f) != c:
                f.close()
            else:
                files[k] = f
    return files


def fetch_latest(label, commits=[], filter_func=None):
    short_commits = [c[:8] for c in commits]
    prefix = "data/{!s}/".format(label)
    bucket = boto3.resource('s3').Bucket('hrl.zanol.eu')
    objs = bucket.objects.filter(Prefix=prefix)
    sorted_objs = sorted(objs,
                         key=operator.attrgetter('last_modified'),
                         reverse=True)
    for obj in sorted_objs:
        if len(short_commits) > 0:
            short_c = obj.key.replace(prefix, '', 1).split('/')[0]
            if short_c not in short_commits:
                continue

            f = aws_fetch_data(obj.key)
            if get_hdf5_commit(f) not in commits:
                f.close()
                continue
        else:
            f = aws_fetch_data(obj.key)

        if filter_func is not None and not filter_func(obj):
            f.close()
            continue

        return (obj.key, f)
    raise RuntimeError("Not found")


def get_hdf5_commit(h5f):
    where_params = itertools.chain((h5f,), h5f.values())
    (wp1, wp2) = itertools.tee(where_params)

    g = next(filter(lambda g: 'git_sha1' in g.attrs, wp1), None)
    if g is not None:
        return g.attrs['git_sha1']

    g = next(filter(lambda g: 'params' in g.attrs, wp2), None)
    if g is not None:
        params = pack.unpack_objects(g.attrs['params'])
        if 'git_sha1' in params:
            return params['git_sha1']

    raise RuntimeError("Commit SHA1 not found")
