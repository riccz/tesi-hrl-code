import lzma
import pathlib
import pickle
import warnings
import zlib

import numpy as np

from . import aws

with warnings.catch_warnings():
    warnings.simplefilter("ignore", FutureWarning)
    import h5py


def insert_hdf(src, dest):
    for k in src:
        if isinstance(src[k], h5py.Group):
            assert(k not in dest)
            g_src = src[k]
            g_dest = dest.create_group(k)
            insert_hdf(g_src, g_dest)
        else:
            assert(k not in dest)
            ds_src = src[k]
            ds_dest = dest.create_dataset(k, ds_src.shape,
                                          dtype=ds_src.dtype,
                                          compression='gzip')
            ds_dest[...] = ds_src
    # TODO: handle attributes


def np2hdfds(group, **kwargs):
    for k, v in kwargs.items():
        ds = group.create_dataset(k, v.shape, dtype=v.dtype,
                                  compression='gzip')
        ds[...] = v


def np2hdf(label, **kwargs):
    found = False
    while not found:
        packed_id = "{!s}_{!s}".format(label, aws.random_string(16))
        tmpfile = pathlib.Path("/tmp/{!s}".format(packed_id))
        found = not tmpfile.exists()

    with h5py.File(tmpfile, 'w') as h5f:
        np2hdfds(h5f, **kwargs)
    return (tmpfile, packed_id)


def save_vars(fname, **kwargs):
    ps = pickle.dumps(kwargs, protocol=4)
    cps = lzma.compress(ps)
    with open(fname, 'wb') as f:
        f.write(cps)


def pack_objects(*args, **kwargs):
    if len(args) > 0 and len(kwargs) > 0:
        raise ValueError("Either keywords or positional args")
    if len(args) == 0 and len(kwargs) == 0:
        raise ValueError("One of keywords or positional args")

    if len(args) > 1:
        data = pickle.dumps(args, protocol=4)
    elif len(args) == 1:
        data = pickle.dumps(args[0], protocol=4)
    else:
        data = pickle.dumps(kwargs, protocol=4)

    compressed = zlib.compress(data)
    return np.void(compressed)


def unpack_objects(data):
    data = zlib.decompress(data)
    objs = pickle.loads(data)
    return objs


def load_vars(fname):
    with open(fname, 'rb') as f:
        cps = f.read()
    ps = lzma.decompress(cps)
    return pickle.loads(ps)


def pack_history(grid, hist):
    (s, a, r, ns) = hist[0]
    ps = grid.state_space.pack(s)
    pa = grid.action_space.pack(a)
    pr = np.empty(tuple(), dtype=[(ag, float) for ag in grid.agent_keys])
    for ag in grid.agent_keys:
        pr[ag] = r[ag]
    pns = grid.state_space.pack(ns)

    states = np.empty(len(hist), dtype=ps.dtype)
    actions = np.empty(len(hist), dtype=pa.dtype)
    rewards = np.empty(len(hist), dtype=pr.dtype)
    next_states = np.empty(len(hist), dtype=pns.dtype)

    for j, h in enumerate(hist):
        (s, a, r, ns) = h

        states[j] = grid.state_space.pack(s)
        actions[j] = grid.action_space.pack(a)
        for ag in grid.agent_keys:
            rewards[j][ag] = r[ag]
        next_states[j] = grid.state_space.pack(ns)
    return (states, actions, rewards, next_states)


def unpack_history(grid, states, actions, rewards, new_states):
    hist = []
    for ps, pa, pr, pns in zip(states, actions, rewards, new_states):
        s = grid.state_space.unpack(ps)
        a = grid.action_space.unpack(pa)
        r = {ag: pr[ag] for ag in grid.agent_keys}
        ns = grid.state_space.unpack(pns)
        hist.append((s, a, r, ns))
    return hist
