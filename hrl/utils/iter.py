import contextlib
import functools
import itertools
import operator


class ReusableIter:
    def __init__(self, iterable):
        self.base = iter(iterable)

    def __iter__(self):
        (i0, i1) = itertools.tee(self.base)
        self.base = i0
        return i1

    def __enter__(self):
        return self.__iter__()

    def __exit__(self, exc_type, exc_val, exc_tb):
        return False


def starfilter(func, iterable):
    return filter(lambda x: func(*x), iterable)


def _average_reduce_op(r, x):
    (n, tot) = r
    n += 1
    tot += x
    return (n, tot)


def average(seq):
    """Compute the average of a sequence"""
    (n, tot) = functools.reduce(_average_reduce_op, seq, (0, 0.0))
    return tot / n


def avg_over_time(xs):
    """Compute the average of xs[:N] for every value of N"""
    return itertools.starmap(lambda i, xsum: xsum / (i + 1),
                             enumerate(itertools.accumulate(xs)))


def flatten_dicts(seq, return_iters=False):
    """Convert an iterable over dicts into a dict of iterables: [{'x': 0,
    'y': 1}, {'x': 2, 'y': 3}] -> {'x': [0, 2], 'y': [1, 3]}

    """
    seqit = iter(seq)
    first = next(seqit, None)
    if first is None:
        return dict()
    keys = sorted(first.keys())

    def check_same_keys(item):
        if sorted(item.keys()) != keys:
            raise ValueError("Item with keys {!s} that differ from the first"
                             " ones: {!s}".format(sorted(item.keys()), keys))
        return item

    checked_seqit = map(check_same_keys, seqit)
    restored_seqit = itertools.chain([first], checked_seqit)
    tees = itertools.tee(restored_seqit, len(keys))

    if return_iters:
        def take_key(k, seq):
            return map(operator.itemgetter(k), seq)
    else:
        def take_key(k, seq):
            return list(map(operator.itemgetter(k), seq))

    return {k: take_key(k, t) for k, t in zip(keys, tees)}


def zip_same(*iterables):
    fill = object()
    zipped = itertools.zip_longest(*iterables, fillvalue=fill)

    def raise_on_fill(xs):
        if any(x is fill for x in xs):
            raise ValueError("Iterables did not have the same length")
        return xs

    return map(raise_on_fill, zipped)


def unflatten_dicts(d):
    keys = d.keys()
    zipped_vals = zip_same(*d.values())

    def make_dict(zipped):
        return dict(zip(keys, zipped))

    return map(make_dict, zipped_vals)


def sliding_window(iterable, winsize=2):
    """Sliding window of `winsize` elements over `iterable`

    With winsize=2: [1,2,3,4] -> [(1,2), (2, 3), (3, 4)]. The
    iteration stops when the end of the window reaches past the end;
    i.e. the last returned tuple contains `iterable[-winsize:]`

    """
    iters = itertools.tee(iterable, winsize)
    for w in range(1, winsize):
        for i in range(w, winsize):
            next(iters[i], None)
    return zip(*iters)


def partition(iterable, predicate):
    """Split an iterable according to a predicate over its elements. The
    first returned iterator contains the items that satisfy the
    predicate. The second iterator contains the other items.

    """
    def splitter(lists, elem):
        if predicate(elem):
            lists[0].append(elem)
        else:
            lists[1].append(elem)
        return lists
    return functools.reduce(splitter, iterable, ([], []))


def unstar(seq, lazy=True):
    """Convert an iterable over N-tuples into N iterables over the single
    components: [(1, 2, 3), (4, 5, 6)] -> ([1, 4], [2, 5], [3, 6])

    """
    seq_iter = iter(seq)
    first = next(seq_iter, None)
    if first is None:
        return tuple()
    N = len(first)

    seq_orig = itertools.chain([first], seq_iter)
    tees = itertools.tee(seq_orig, N)

    def take_ith(i, seq):
        return map(operator.itemgetter(i), seq)

    out = tuple(take_ith(i, t) for i, t in enumerate(tees))
    if lazy:
        return out
    else:
        return tuple(list(l) for l in out)


unzip = unstar


def dotproduct(vec1, vec2, sum=sum, map=map, mul=operator.mul):
    """Dot product between two iterables"""
    return sum(map(mul, vec1, vec2))


def nth(seq, n):
    seq_iter = iter(seq)
    for _ in range(n - 1):
        next(seq_iter)
    return next(seq_iter)


def select(key_iter, maps_iter):
    """Select the elements from maps_iter that correspond to the keys
       from key_iter,
    """
    return itertools.starmap(operator.getitem, zip(maps_iter, key_iter))