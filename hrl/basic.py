import numbers
import logging

import numpy as np

from .abstr import InvSeq
from .utils.misc import cached_property

logger = logging.getLogger(__name__)


class QuantizedSpace(InvSeq):
    """Class used to represent quantized action/state spaces

    This represents a continuous space by quantizing it in a
    given number of steps or bins

    """

    def _make_with_steps(self, a, b, steps):
        if b <= a:
            raise ValueError("The interval cannot be empty")
        if steps <= 0:
            raise ValueError("There must be at least 1 step")

        self.a = a
        self.b = b
        self.steps = steps

        self.__stepsize = (self.b - self.a) / self.steps
        self.bins = tuple(self.a + self.__stepsize * i
                          for i in range(1, self.steps + 1))

    def _make_with_bins(self, a, b, bins):
        if b <= a:
            raise ValueError("The interval cannot be empty")
        if len(bins) == 0:
            raise ValueError("There must be at least 1 bin")
        if bins[0] <= a:
            ve_msg = "The first bin must not be empty (> {:f})".format(a)
            raise ValueError(ve_msg)
        if bins[-1] != b:
            raise ValueError("The last bin must end at {:f}".format(b))

        self.a = a
        self.b = b
        self.bins = tuple(sorted(set(bins)))
        self.steps = len(bins)
        self.__stepsize = None

        if len(self.bins) != len(bins):
            raise ValueError("The bins must be sorted and unique")
        elif any(sb != b for sb, b in zip(self.bins, bins)):
            raise ValueError("The bins must be sorted and unique")

    @property
    def stepsize(self):
        s = self.__stepsize
        if s is None:
            raise RuntimeError("The stepsize is not fixed")
        else:
            return s

    def __init__(self, a, b, steps=None, bins=None):
        """Represent the interval [a,b], quantized

        The quantization can be uniform in `steps` elements or
        specified by the `bins` argument. The elements of `bins`
        define the upper limits of the quantization interval. The last
        element of `bins` should be `b`.

        """
        super().__init__()

        if bins is not None and steps is None:
            self._make_with_bins(a, b, bins)
        elif steps is not None and bins is None:
            self._make_with_steps(a, b, steps)
        elif steps is not None and bins is not None:
            logger.warning("Both steps and bins were given")
            if len(bins) != steps:
                raise ValueError("Mismatch between bins and steps")
            self._make_with_bins(a, b, bins)
        else:
            raise ValueError("One of bins or steps is required")

    def __eq__(self, other):
        return (self.a == other.a and self.b == other.b and
                self.bins == other.bins)

    def __len__(self):
        return self.steps

    def k2i(self, key):
        if not isinstance(key, numbers.Integral):
            raise TypeError("Key {!s} is not an integer".format(key))
        if key < 0 or key >= len(self):
            raise IndexError("Key {!s} is out of range".format(key))

        if key == 0:
            mid = (self.a + self.bins[0]) / 2
        else:
            mid = (self.bins[key - 1] + self.bins[key]) / 2
        return float(mid)

    def i2k(self, item):
        if not isinstance(item, numbers.Real):
            raise TypeError("Item {!s} has the wrong type".format(item))
        if item < self.a or item > self.b:
            raise ValueError("Item {!s} is out of range".format(item))
        return int(np.digitize(item, self.bins, right=True))

    def make_item(self, x):
        """Return the item that best approximates the given point"""
        if x not in self:
            raise ValueError("The point {!s} is out of range".format(x))
        return float(x)

    @cached_property
    def min(self):
        return self[0]

    @cached_property
    def max(self):
        return self[len(self) - 1]

    def items_lt(self, lhs, rhs):
        return self.i2k(lhs) < self.i2k(rhs)

    def items_le(self, lhs, rhs):
        return self.i2k(lhs) <= self.i2k(rhs)

    def items_gt(self, lhs, rhs):
        return self.i2k(lhs) > self.i2k(rhs)

    def items_ge(self, lhs, rhs):
        return self.i2k(lhs) >= self.i2k(rhs)

    def pack(self, state):
        return np.array(state, dtype=float)

    def unpack(self, packed):
        return self.make_item(float(packed))


class NamedSpace(InvSeq):
    def __init__(self, *args):
        super().__init__()
        if len(args) == 0:
            raise ValueError("Give at least one state name")
        types = [type(a) for a in args]
        if any(t is not types[0] for t in types[1:]):
            raise TypeError("All the state names must have the same type")
        self._forward = tuple(sorted(args))
        self._reverse = {n: i for i, n in enumerate(self._forward)}

        if isinstance(args[0], str):
            maxlen = max(len(a.encode()) for a in self._forward)
            self.__out_dtype = np.dtype(('S', maxlen))
        else:
            self.__out_dtype = np.dtype(type(args[0]))

    def __len__(self):
        return len(self._forward)

    def __eq__(self, other):
        return self._forward == other._forward

    def k2i(self, key):
        if not isinstance(key, numbers.Integral):
            raise TypeError("Key {!s} is not an integer".format(key))
        if key < 0 or key >= len(self):
            raise IndexError("Key {!s} is out of range".format(key))
        return self._forward[key]

    def i2k(self, item):
        if not isinstance(item, self.item_type):
            raise TypeError("Item {!s} has the wrong type".format(item))
        try:
            return self._reverse[item]
        except KeyError as e:
            vemsg = "Item {!s} is not part of this state".format(item)
            raise ValueError(vemsg) from e

    def make_item(self, name):
        if name not in self._forward:
            vemsg = "Item {!s} is not part of this space".format(name)
            raise ValueError(vemsg)
        return self.item_type(name)

    def pack(self, state):
        if isinstance(state, str):
            state = state.encode()
        return np.array(state, dtype=self.__out_dtype)

    def unpack(self, packed):
        if isinstance(self[0], str):
            return self.make_item(bytes(packed).strip(b'\x00').decode())
        else:
            return self.item_type(packed)
