import argparse
import pathlib

import numpy as np

from .grid import GridMDP, battery_bins
from .klspi import rbf_kernel

# Single agent grid definition
grid_stepsize = 100
T = 15
grid_steps = 5
b_bins = battery_bins(10, 0.016)
grid = GridMDP(agents=1,
               grid_radius=grid_stepsize*grid_steps/2,
               grid_steps=grid_steps,
               b_bins=b_bins,
               avg_steps_on=5,
               avg_steps_off=30,
               d_steps=4,
               theta_steps=4,
               T=T)

# Multi agent grid definition
multigrid = GridMDP(agents=4,
                    grid_radius=grid_stepsize*grid_steps/2,
                    grid_steps=grid_steps,
                    b_bins=b_bins,
                    avg_steps_on=5,
                    avg_steps_off=30,
                    d_steps=4,
                    theta_steps=4,
                    T=T)

# Common commandline arguments
output_opts_parser = argparse.ArgumentParser(add_help=False)
output_opts_parser.add_argument("--output",
                                help="Output HDF5 file",
                                type=pathlib.Path,
                                default=pathlib.Path('out.hdf5'))
_upload_group = output_opts_parser.add_mutually_exclusive_group()
_upload_group.add_argument("--no-upload",
                           help="Don't upload the results",
                           action='store_false',
                           dest='upload')
_upload_group.add_argument("--upload",
                           help="Upload the results",
                           action='store_true',
                           dest='upload')
_upload_group.set_defaults(upload=True)

plot_opts_parser = argparse.ArgumentParser(add_help=False)
_datasrc_group = plot_opts_parser.add_mutually_exclusive_group()
_datasrc_group.add_argument("--data", help="Data file to load", type=str)
_datasrc_group.add_argument("--latest", help="Load latest data",
                            action='store_const', const=None,
                            dest='data')


def preprocess(sa):
    """Preprocessing of (state, action) pairs for KLSPI"""
    if isinstance(sa, tuple):
        was_single = True
        sa = [sa]
    else:
        was_single = False

    out_sa_size = ((3 + grid._reward._reward_matrix.size) *
                   len(grid.action_space.sub('ag0')))
    out = np.zeros((len(sa), out_sa_size))
    for i, sa_elem in enumerate(sa):
        (sR, action) = sa_elem
        (state, reward_matrix) = sR

        b = np.array(state['ag0']['b'])
        R = np.reshape(reward_matrix, -1)

        x = state['ag0']['x']
        x /= grid.state_space.x_max

        y = state['ag0']['y']
        y /= grid.state_space.y_max

        xybR = np.empty(3 + len(R))
        xybR[0] = x
        xybR[1] = y
        xybR[2] = b
        xybR[3:] = R

        A = grid.action_space.sub('ag0')
        a_idx = A.index(action['ag0'])
        start_nz = a_idx * len(xybR)
        end_nz = start_nz + len(xybR)
        out[i, start_nz:end_nz] = xybR

    return out if not was_single else out[0]


def kern(sa1, sa2, sigma=1.0):
    """Kernel used by KLSPI"""
    pp1 = preprocess(sa1)
    pp2 = preprocess(sa2)
    return rbf_kernel(pp1, pp2, sigma=sigma)


algo_order = {
    'RW': 0,
    'LA': 1,
    'MCTS': 1,
    'QL': 2,
    'FAQL': 3,
    'RUQL': 4,
}

algo_color = {
    'RW': 'C0',
    'LA': 'C1',
    'MCTS': 'C1',
    'QL': 'C2',
    'FAQL': 'C3',
    'RUQL': 'C4',
}