import abc
import base64
import collections.abc as cabc
import copy
import itertools
import logging
import numbers
import pickle
import random
import zlib

import numpy as np

from .utils.iter import flatten_dicts
from .utils.misc import cached_property
from .utils.stats import AverageCounter, measure_time

logger = logging.getLogger(__name__)


class InvSeq(cabc.Sequence):
    """Abstract base class for invertible sequences

    This ABC represents a bidirectional map between a range of
    integers [0, N) and a set of items. The map and its reverse are
    defined by the methods `k2i` and `i2k` which must be
    implemented by concrete subclasses. The `__len__` method must also
    be defined by subclasses to return the number of items N.

    Items with a specific value should be constructed through the
    `make_item` method, which must be implemented by concrete
    subclasses.

    In addition to the abstract methods, the equality comparison
    `__eq__` and `__ne__` must be implemented in every concrete
    subclass.

    """
    @abc.abstractmethod
    def __len__(self):
        """The number of distinct items in the sequence"""
        raise NotImplementedError()

    @abc.abstractmethod
    def k2i(self, key):
        """Map an integer key to an item

        Must raise an IndexError on invalid keys.

        """
        raise NotImplementedError()

    @abc.abstractmethod
    def i2k(self, item):
        """Map an item to its key

        Must raise a ValueError on invalid items.

        """
        raise NotImplementedError()

    @abc.abstractmethod
    def make_item(self, *args, **kwargs):
        """Construct a specific item from this sequence

        The arguments are defined by the concrete implementations.

        """
        raise NotImplementedError()

    def __call__(self, *args, **kwargs):
        """Alias to allow short make_item statements"""
        return self.make_item(*args, **kwargs)

    @cached_property
    def max_key(self):
        """Use or override this instead of __len__ when the sequence is longer
        than `sys.maxsize`

        """
        if hasattr(self, '__len__'):
            return len(self) - 1
        else:
            raise NotImplementedError()

    @cached_property
    def item_type(self):
        return type(self.k2i(0))

    def __getitem__(self, key):
        """Return the item for the given key or keys"""
        if isinstance(key, numbers.Integral):
            if key < 0:
                key = self.max_key + 1 + key
            return self.k2i(key)
        elif isinstance(key, cabc.Iterable):
            return [self.k2i(k) for k in key]
        elif isinstance(key, slice):
            if key.start is None:
                start = 0
            elif key.start < 0:
                start = self.max_key + 1 + key.start
            else:
                start = key.start
            if key.stop is None:
                stop = self.max_key + 1
            elif key.stop < 0:
                stop = self.max_key + 1 + key.stop
            else:
                stop = key.stop
            if key.step is None:
                step = 1
            else:
                step = key.step
            return [self.k2i(k) for k in range(start, stop, step)]
        else:
            raise TypeError("The key {!s} is not an integer,"
                            " iterable or slice".format(key))

    # def iter_slice(self, start=None, stop=None, step=None):
    #     """Return an iterator over the items with keys in the given slice"""
    #     if start is None: start = 0
    #     elif start < 0: start = len(self) + start
    #     if stop is None: stop = len(self)
    #     elif stop < 0: stop = len(self) + stop
    #     if step is None: step = 1
    #     return map(self._key2item, range(start, stop, step))

    # def iter_getitems(self, keys):
    #     return map(self._key2item, keys)

    def index(self, item, i=None, j=None):
        """Return the key for the given item or items"""
        if i is None:
            i = 0
        if j is None:
            j = self.max_key + 1

        try:
            k = self.i2k(item)
        except (TypeError, ValueError) as e:
            if isinstance(item, cabc.Iterable):
                if i != 0 or j != self.max_key + 1:
                    raise ValueError("Cannot use bounds with an iterable")
                return [self.i2k(i) for i in item]
            else:
                raise TypeError("The item {!s} is not an item"
                                " or an iterable".format(item))
        else:
            if k < i or k >= j:
                raise ValueError("Out of given bounds")
            return k

    # def iter_index(self, items):
    #     return map(self._item2key, item)

    # @abc.abstractmethod
    # def make_like(self, item):
    #     raise NotImplementedError()

    def count(self, item):
        """Override for efficiency"""
        return int(item in self)

    def __contains__(self, item):
        """Override for efficiency"""
        try:
            self.i2k(item)
            return True
        except (ValueError, TypeError):
            return False

    def random(self):
        """Return a random item"""
        return self.k2i(random.randrange(self.max_key + 1))

    def items_eq(self, lhs, rhs):
        return self.i2k(lhs) == self.i2k(rhs)

    def items_ne(self, lhs, rhs):
        return not self.items_eq(lhs, rhs)


class Learner(abc.ABC):
    @property
    @abc.abstractmethod
    def mdp(self):
        raise NotImplementedError()

    @abc.abstractmethod
    def policy(self):
        raise NotImplementedError()

    def __init__(self, keep_time_samples=False):
        super().__init__()
        self.train_time_counter = AverageCounter(keep_all=keep_time_samples)
        self.test_time_counter = AverageCounter(keep_all=keep_time_samples)

    def reset(self):
        pass

    def pack(self):
        data = pickle.dumps((Learner, self), protocol=4)
        comp = zlib.compress(data)
        return base64.b64encode(comp)

    @staticmethod
    def unpack(packed):
        decomp = zlib.decompress(base64.b64decode(packed))
        loaded = pickle.loads(decomp)
        LoadedType = loaded[0]
        if LoadedType is Learner:
            return loaded[1]
        elif issubclass(LoadedType, Learner):
            return LoadedType.unpack(packed)
        else:
            raise TypeError()

    def learn(self, steps, after=None):
        logger.warning("Class {!s} does not learn".format(type(self).__name__))
        self.test(steps, after=after)

    def test(self, steps, reset=True, after=None, real_mdp=False):
        if real_mdp:
            test_mdp = self.mdp
        else:
            test_mdp = copy.deepcopy(self.mdp)
        if reset:
            test_mdp.reset()

        policy = self.policy()
        rewards = []
        for t in range(steps):
            with measure_time(self.test_time_counter):
                a = policy(test_mdp.state)
                (s, r, ns) = test_mdp.do_action(a)
            rewards.append(r)

            if after is not None:
                after(s, a, r, ns)

        split_rewards = flatten_dicts(rewards)
        all_rewards = itertools.chain.from_iterable(split_rewards.values())
        return np.fromiter(all_rewards, dtype=float).mean()
