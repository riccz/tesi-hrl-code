import collections.abc as cabc
import copy
import itertools
import random

import numpy as np

from .basic import NamedSpace
from .product import ProductSpace
from .utils import flatten_dicts


class SimpleMDP:
    def __init__(self):
        self.epsilon = 0.2
        inner_state_sp = NamedSpace('A', 'B', 'C', 'D')
        self.state_space = ProductSpace(ag0=inner_state_sp)
        self.state_space.__dict__['single'] = self.state_space.sub('ag0')
        inner_action_sp = NamedSpace('left', 'right', 'stay')
        self.action_space = ProductSpace(ag0=inner_action_sp)
        self.action_space.__dict__['single'] = self.action_space.sub('ag0')

        self.starting_state = self.state_space.make_item(ag0='A')
        self.state = self.starting_state
        self.time = 0

    def valid_actions(self, state=None, agent=None):
        if state is None: state = self.state

        actions = flatten_dicts(self.action_space[:])

        if state['ag0'] == 'A':
            actions['ag0'].remove('left')
        elif state['ag0'] == 'D':
            actions['ag0'].remove('right')

        return actions if agent is None else actions['ag0']

    def indep_transition_prob(self, agent, state, action):
        assert(agent == 'ag0')
        return self.transition_prob_iter(state, {'ag0': action})


    def indep_reward(self, agent, s, a, new_s):
        return self.reward(s, a, new_s)[agent]

    def transition_prob(self, s=None, a=None, new_s=None):
        if a is None:
            raise RuntimeError()

        if s is None: s = self.state

        vas = self.valid_actions(s)
        if a['ag0'] not in vas['ag0']:
            raise ValueError()

        # Aidx = lambda n: self.action_space.index({ag0: n})
        Sidx = lambda n: self.state_space.index({'ag0': n})

        probs = np.zeros(len(self.state_space))

        if s['ag0'] == 'A':
            if a['ag0'] == 'stay':
                probs[Sidx('A')] = 1 - self.epsilon/2
                probs[Sidx('B')] = self.epsilon/2
            elif a['ag0'] == 'right':
                probs[Sidx('A')] = self.epsilon/2
                probs[Sidx('B')] = 1 - self.epsilon/2
        elif s['ag0'] == 'B':
            if a['ag0'] == 'stay':
                probs[Sidx('A')] = self.epsilon/3
                probs[Sidx('B')] = 1 - self.epsilon*2/3
                probs[Sidx('C')] = self.epsilon/3
            elif a['ag0'] == 'left':
                probs[Sidx('A')] = 1 - self.epsilon*2/3
                probs[Sidx('B')] = self.epsilon/3
                probs[Sidx('C')] = self.epsilon/3
            elif a['ag0'] == 'right':
                probs[Sidx('A')] = self.epsilon/3
                probs[Sidx('B')] = self.epsilon/3
                probs[Sidx('C')] = 1 - self.epsilon*2/3
        elif s['ag0'] == 'C':
            if a['ag0'] == 'stay':
                probs[Sidx('B')] = self.epsilon/3
                probs[Sidx('C')] = 1 - self.epsilon*2/3
                probs[Sidx('D')] = self.epsilon/3
            elif a['ag0'] == 'left':
                probs[Sidx('B')] = 1 - self.epsilon*2/3
                probs[Sidx('C')] = self.epsilon/3
                probs[Sidx('D')] = self.epsilon/3
            elif a['ag0'] == 'right':
                probs[Sidx('B')] = self.epsilon/3
                probs[Sidx('C')] = self.epsilon/3
                probs[Sidx('D')] = 1 - self.epsilon*2/3
        elif s['ag0'] == 'D':
            if a['ag0'] == 'stay':
                probs[Sidx('C')] = self.epsilon/2
                probs[Sidx('D')] = 1 - self.epsilon/2
            elif a['ag0'] == 'left':
                probs[Sidx('C')] = 1 - self.epsilon/2
                probs[Sidx('D')] = self.epsilon/2

        if new_s is None:
            return probs
        else:
            return probs[self.state_space.index(new_s)]

    def transition_prob_iter(self, s, a, approx=False):
        probs = self.transition_prob(s, a)
        return itertools.starmap(lambda sidx, p: (self.state_space[sidx], p),
                                 filter(lambda x: x[1] > 0, enumerate(probs)))

    def reward(self, s=None, a=None, new_s=None):
        if a is None or new_s is None:
            raise RuntimeError()

        if s is None: s = self.state

        if s['ag0'] == 'D' and new_s['ag0'] == 'D':
            return {'ag0': 0.0}
        else:
            return {'ag0': -1.0}

    @property
    def max_reward(self):
        return 0.0

    @property
    def agent_keys(self):
        return ['ag0']

    @property
    def agents(self):
        return 1

    def next_state(self, *, s=None, a):
        ns_probs = self.transition_prob(s=s, a=a)
        ns_i = np.random.choice(range(len(self.state_space)), p=ns_probs)
        return self.state_space[ns_i]

    def do_action(self, a):
        ns = self.next_state(a=a)
        r = self.reward(a=a, new_s=ns)
        s = copy.deepcopy(self.state)
        self.state = ns
        self.time += 1
        return (s, r, ns)

    def reset(self):
        self.state = self.starting_state
        self.time = 0

    def random_action(self, state=None):
        actions = self.valid_actions(state)
        if not isinstance(actions, cabc.Mapping):
            return random.choice(actions)
        else:
            a_kw = {ag: random.choice(ag_acts) for ag, ag_acts in actions.items()}
            return self.action_space.make_item(**a_kw)

    def observation(self, agent, state=None):
        return self.state if state is None else state

    def observation_space(self, agent):
        return self.state_space

    def randomQ(self, state, action, gamma, maxsteps):
        T = self.transition_prob(state, action)
        Q = 0
        for ns, p_ns in zip(self.state_space, T):
            if p_ns == 0:
                continue

            r = self.reward(state, action, ns)
            Q += r['ag0']
            if maxsteps > 0:
                valid_actions = self.valid_actions(ns)
                nextQs = [self.randomQ(ns, {'ag0': a},
                                       gamma, maxsteps-1)
                          for a in valid_actions['ag0']]
                Q += gamma * np.mean(nextQs)
        return Q


class SimpleMultiMDP:
    def __init__(self):
        S0 = NamedSpace('A', 'B', 'C', 'D', 'E')
        S1 = copy.deepcopy(S0)

        A0 = NamedSpace('left', 'right', 'stay')
        A1 = copy.deepcopy(A0)

        self.state_space = ProductSpace(ag0=S0, ag1=S1)
        self.action_space = ProductSpace(ag0=A0, ag1=A1)

        self.starting_state = self.state_space.make_item(ag0='A', ag1='D')
        self.state = self.starting_state
        self.time = 0

    def valid_actions(self, state=None):
        if state is None: state = self.state

        actions = {ag: self.action_space.sub(ag)[:] for ag in ['ag0', 'ag1']}

        for ag in ['ag0', 'ag1']:
            if state[ag] == 'A':
                actions[ag].remove('left')
            elif state[ag] == 'D':
                actions[ag].remove('right')
            elif state[ag] == 'E':
                actions[ag] = [self.action_space.spaces[ag].make_item('stay')]
        # if state['ag0'] == 'B' and state['ag1'] != 'C':
        #     actions['ag0'].remove('right')
        # if state['ag1'] == 'C' and state['ag1'] != 'B':
        #     actions['ag1'].remove('left')

        return actions

    def transition_prob(self, *, s=None, a, new_s=None):
        if s is None: s = self.state
        vas = self.valid_actions(s)
        if (a['ag0'] not in vas['ag0'] or
            a['ag1'] not in vas['ag1']):
            raise ValueError()

        left_map = {'B':'A', 'C': 'E', 'D':'C'}
        right_map = {'A':'B', 'B': 'E', 'C':'D'}

        only_ns = copy.deepcopy(s)
        for ag in ['ag0', 'ag1']:
            si_str = str(s[ag])
            ai = a[ag]
            Si = self.state_space.spaces[ag]
            if ai == 'stay':
                pass
            elif ai == 'left':
                only_ns[ag] = Si(left_map[si_str])
            elif ai == 'right':
                only_ns[ag] = Si(right_map[si_str])

        if new_s is not None:
            return float(only_ns == new_s)
        else:
            probs = np.zeros(len(self.state_space))
            probs[self.state_space.index(only_ns)] = 1
            return probs

    def reward(self, *, s=None, a, new_s):
        if s is None: s = self.state

        rs = dict()
        for ag in self.agent_keys:
            if s[ag] == 'E':
                rs[ag] = 0.0
            # elif new_s[ag] == 'E':
            #     if all(new_s[ag] == 'E' for ag in self.agent_keys):
            #         rs[ag] = -1.0
            #     else:
            #         rs[ag] = -10.0
            else:
                rs[ag] = -1.0
        return rs

    @property
    def max_reward(self):
        return 0.0

    @property
    def agent_keys(self):
        return ['ag0', 'ag1']

    def next_state(self, *, s=None, a):
        ns_probs = self.transition_prob(s=s, a=a)
        ns_i = np.random.choice(range(len(self.state_space)), p=ns_probs)
        return self.state_space[ns_i]

    def do_action(self, a):
        ns = self.next_state(a=a)
        r = self.reward(a=a, new_s=ns)
        s = copy.deepcopy(self.state)
        self.state = ns
        self.time += 1
        return (s, r, ns)

    def reset(self):
        self.state = self.starting_state
        self.time = 0

    def random_action(self, state=None):
        actions = self.valid_actions(state)
        if not isinstance(actions, cabc.Mapping):
            return random.choice(actions)
        else:
            a_kw = {ag: random.choice(ag_acts) for ag, ag_acts in actions.items()}
            return self.action_space.make_item(**a_kw)

    def observation(self, agent, state=None):
        return self.state if state is None else state

    def observation_space(self, agent):
        return self.state_space
