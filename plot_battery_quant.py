import numpy as np
import matplotlib.pyplot as plt

from hrl.grid import battery_bins

grid_stepsize = 100
batt_bins = 10

hover_batt_use = 0.122/100  # per s
slow_batt_use = 0.038/100  # per m at 8m/s
move_speed = 8  # m/s
stay_time = 15

step_move = slow_batt_use * grid_stepsize
step_stay = hover_batt_use * stay_time
minstep = 0.016
print("The minstep is {:f}".format(minstep))

bins = battery_bins(batt_bins, minstep)

xs = np.linspace(0, 1, 4096)
quant_idxs = np.digitize(xs, bins, right=True)

out_vals = np.fromiter(map(lambda x, y: (x + y) / 2,
                           bins, [0] + list(bins[:-1])),
                       dtype=float)

quant_xs = out_vals[quant_idxs]

plt.close()
plt.figure()
(line,) = plt.plot(xs, quant_xs)
plt.plot(xs, xs, linestyle='dotted', color=line.get_color())
plt.grid()
plt.xlabel("SOC")
plt.ylabel("Quantized SOC")
plt.xlim(1.05, -0.05)
plt.ylim(-0.05, 1.05)
plt.savefig("plots/quantized_soc.pdf", format='pdf')
plt.show()