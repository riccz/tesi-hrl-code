import argparse
import logging
import warnings

from celery import group
import numpy as np

from hrl import utils
from hrl.common import multigrid, output_opts_parser
from hrl.random_walker import RandomWalker
from hrl_app import run_single_test
import hrl

with warnings.catch_warnings():
    warnings.simplefilter("ignore", FutureWarning)
    import h5py

logger = logging.getLogger(__name__)

logger.info("Running with commit {!s}".format(hrl.GIT_SHA1))
parser_descr = 'Build an average reward plot for single-agent lookahead'
parser = argparse.ArgumentParser(description=parser_descr,
                                 allow_abbrev=False,
                                 parents=[output_opts_parser])
parser.add_argument("--avgcount", help="Number of runs to average",
                    type=int, default=50)
parser.add_argument("--test-iters", help="Test iterations",
                    type=int, default=10000)
args = parser.parse_args()

base_rw = RandomWalker(multigrid)
base_packed = base_rw.pack()
# Keep one history
test0 = run_single_test.si(base_packed, args.test_iters, hist_label='hist')
test = run_single_test.si(base_packed, args.test_iters)
alltests = [test0] + [test for _ in range(1, args.avgcount)]
test_group = group(alltests)
test_results = test_group.apply_async()

# Wait for the results
average_rewards = np.empty(args.avgcount)
for i, r in enumerate(test_results):
    (avgr, histkey) = test_results[i].get()
    logger.debug("Test {:d} complete: avgr = {:f}".format(i, avgr))
    average_rewards[i] = avgr
    if i == 0:
        logger.debug("Keep the full history")
        histkey_keep = histkey

with h5py.File(args.output, 'w') as h5f:
    h5f.attrs['git_sha1'] = hrl.GIT_SHA1

    g = h5f.create_group('plot_data')
    g.attrs['params'] = utils.pack_objects(test_iterations=args.test_iters,
                                           average_count=args.avgcount,
                                           grid=multigrid,
                                           learner=base_rw)

    avgr_ds = g.create_dataset('avg_rewards',
                               average_rewards.shape,
                               dtype=float,
                               compression='gzip')
    avgr_ds[:] = average_rewards

    g_hist = h5f.create_group('hist')
    tmp_hist = utils.aws.aws_fetch_data(histkey_keep)
    s_ds = g_hist.create_dataset('states', tmp_hist['states'].shape,
                                 dtype=tmp_hist['states'].dtype,
                                 compression='gzip')
    s_ds[:] = tmp_hist['states']

    a_ds = g_hist.create_dataset('actions', tmp_hist['actions'].shape,
                                 dtype=tmp_hist['actions'].dtype,
                                 compression='gzip')
    a_ds[:] = tmp_hist['actions']

    r_ds = g_hist.create_dataset('rewards', tmp_hist['rewards'].shape,
                                 dtype=tmp_hist['rewards'].dtype,
                                 compression='gzip')
    r_ds[:] = tmp_hist['rewards']

    ns_ds = g_hist.create_dataset('new_states',
                                  tmp_hist['new_states'].shape,
                                  dtype=tmp_hist['new_states'].dtype,
                                  compression='gzip')
    ns_ds[:] = tmp_hist['new_states']
    tmp_hist.close()

if args.upload:
    key = utils.aws_upload_data(args.output, 'multi_random_walk')
    logger.info("Uploaded with key {!s}".format(key))
else:
    logger.warning("Running with --no-upload:"
                   " the results were not uploaded")
