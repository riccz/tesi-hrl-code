import math

from hrl import mcts, grid

grid_stepsize = 30
grid_steps = 5
b_bins = grid.battery_bins(10, grid_stepsize)
g = grid.GridMDP(agents=4,
                 grid_radius=grid_stepsize*grid_steps/2,
                 grid_steps=grid_steps,
                 b_bins=b_bins,
                 avg_steps_on=3,
                 avg_steps_off=30,
                 d_steps=4,
                 theta_steps=4)

m = mcts.MCTS(g, gamma=0.6,
              max_depth=5,
              uct_c=math.sqrt(2),
              rollout_count=100,
              max_fanout=24)

avgr = m.test(30)
print("Average test reward: {:f}".format(avgr))
