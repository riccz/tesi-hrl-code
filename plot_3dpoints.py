import math
import itertools

from scipy import optimize
import matplotlib.pyplot as plt
import numpy as np

plt.close()

fig = plt.figure()
origax = fig.add_subplot(1,1,1)
plotax = fig.add_axes(origax.get_position(), frameon=False)

orig_plot = plt.imread("3dpoints.png")
origax.imshow(orig_plot)
origax.tick_params(which='both', bottom='off', top='off', left='off', right='off',
                   labelbottom='off', labeltop='off',
                   labelleft='off', labelright='off')

plotax.set_xlim(0, 1400)
plotax.set_ylim(0.3, 1)
plotax.grid()
plotax.set_xticks(np.arange(0, 1400+1, 200))
plotax.set_yticks(np.arange(0.3, 1+0.05, 0.1))
# plotax.minorticks_on()

plotax.set_prop_cycle(linestyle=['dashed']*4,
                      color=['blue', 'red', 'teal', 'magenta'])

reward_epsilon=1e-4
reward_phi_up=1+1e-4
reward_phi_down=0.9875
reward_phi_down_slow=(0.998)
reward_shift_level=0.4

T = 30/8
def reward_down(t):
    fast_exp = reward_phi_down ** (t/T)
    tshift = math.log(reward_shift_level)/math.log(reward_phi_down) * T
    slow_exp = reward_shift_level * reward_phi_down_slow ** ((t - tshift)/T)
    if fast_exp <= reward_shift_level:
        return np.maximum(reward_epsilon, slow_exp)
    else:
        return np.maximum(reward_epsilon, fast_exp)

rec_reward_down = np.empty(math.ceil(1400/T))
rec_reward_down[0] = 1
for k in range(1, len(rec_reward_down)):
    r = rec_reward_down[k-1] * reward_phi_down
    if r >= reward_shift_level:
        rec_reward_down[k] = r
    else:
        rec_reward_down[k] = rec_reward_down[k-1] * reward_phi_down_slow
    rec_reward_down[k] = max(rec_reward_down[k], reward_epsilon)

    
tshift = math.log(reward_shift_level)/math.log(reward_phi_down) * T
alpha = math.log(reward_phi_down) / T
beta = reward_shift_level / (reward_phi_down_slow ** (tshift/T)) * math.log(reward_phi_down_slow) / T

rec_reward_dt = np.empty(math.ceil(1400/T))
rec_reward_dt[0] = alpha
for k in range(1, len(rec_reward_dt)):
    r = rec_reward_dt[k-1] * reward_phi_down
    if r <= reward_shift_level * alpha:
        rec_reward_dt[k] = r
    elif r > reward_shift_level*alpha and rec_reward_dt[k-1] <= reward_shift_level*alpha: 
        rec_reward_dt[k] = beta * reward_phi_down_slow ** (tshift/T)
    else:
        rec_reward_dt[k] = rec_reward_dt[k-1] * reward_phi_down_slow
rec_reward_dt = 1/alpha * rec_reward_dt

def reward_down_dt(t):
    if t <= tshift:
        r = alpha * reward_phi_down ** (t/T)
    else:
        r = beta * reward_phi_down_slow ** (t/T)
    return min(r, -reward_epsilon)
            
    
ts = np.linspace(0, 1400, 4096)

r_down = np.fromiter((reward_down(t) for t in ts), dtype=float)
plotax.plot(ts, r_down)
plotax.step(np.arange(math.ceil(1400/T)) * T, rec_reward_down)
#plotax.set_aspect(orig_plot.shape[0] / orig_plot.shape[1])

r_dt = np.fromiter((reward_down_dt(t) / alpha for t in ts), dtype=float)
plotax.plot(ts, r_dt)
plotax.step(np.arange(math.ceil(1400/T)) * T, rec_reward_dt)

# plotax.legend()

# plt.xlim(0, 1.1*Q)
# plt.ylim(minV * 0.95, Efull * 1.05)
# plt.grid()

#plotax.set_xlabel('Used charge [mAh]')
#plotax.set_ylabel('V_out [V]')

plt.show()
