#!/bin/sh

# Enable this with:
# `git config --local core.sshCommand './git_ssh_override.sh'`

set -eu

if echo "$@" | grep 'login.dei.unipd.it' >/dev/null; then
    sshpass -f ~/.ssh/dei_pass ssh $@
else
    ssh $@
fi
