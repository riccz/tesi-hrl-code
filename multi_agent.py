import argparse
import itertools
import logging
import math
import operator
import warnings

from celery import chain, group
import numpy as np

from hrl import qlearn
from hrl import utils
from hrl.abstr import Learner
from hrl.common import multigrid, output_opts_parser
import hrl
import hrl_app

with warnings.catch_warnings():
    warnings.simplefilter("ignore", FutureWarning)
    import h5py

logger = logging.getLogger(__name__)


def take_item(item, otype=float):
    return np.vectorize(operator.itemgetter(item), otypes=(otype,))


logger.info("Running with commit {!s}".format(hrl.GIT_SHA1))
parser_descr = 'Build an average reward plot for multi-agent Q-learning'
parser = argparse.ArgumentParser(description=parser_descr,
                                 allow_abbrev=False,
                                 parents=[output_opts_parser])
parser.add_argument("--avgcount", help="Number of runs to average",
                    type=int, default=200)
parser.add_argument("--total-training", help="Total training iterations",
                    type=int, default=2000000)
parser.add_argument("--test-iters", help="Test iterations",
                    type=int, default=10000)
parser.add_argument("--test-points", help="Test points",
                    type=int, default=24)
parser.add_argument("--method", type=str, default="QL")
args = parser.parse_args()

train_iters = math.ceil(args.total_training / args.test_points)

if args.method == 'RUQL':
    tau = qlearn.lin_decay(start=0.755,
                               const=2.531404e-07,
                               stop=1e-3)
    expl = qlearn.SoftmaxPolicy(tau)
    alpha = qlearn.hyp_decay(start=0.689, exp=0.511096)
    base_ql = qlearn.IndepQLearners(multigrid, alpha=alpha, gamma=0.724,
                                    explore_policy=expl,
                                    update_method='RUQL')
elif args.method == 'QL':
    tau = qlearn.lin_decay(start=1.271,
                           const=3.098697e-08,
                           stop=1e-3)
    expl = qlearn.SoftmaxPolicy(tau)
    alpha = qlearn.hyp_decay(start=0.970, exp=0.529704)
    base_ql = qlearn.IndepQLearners(multigrid, alpha=alpha, gamma=0.436,
                                    explore_policy=expl,
                                    update_method='QL')
elif args.method == 'FAQL':
    tau = qlearn.lin_decay(start=1.210,
                           const=4.442685e-07,
                           stop=1e-3)
    expl = qlearn.SoftmaxPolicy(tau)
    alpha = qlearn.hyp_decay(start=0.565, exp=0.704049)
    faql_beta = qlearn.hyp_decay(start=0.061, exp=0.004)
    base_ql = qlearn.IndepQLearners(multigrid, alpha=alpha, gamma=0.437,
                                    explore_policy=expl,
                                    update_method='FAQL',
                                    faql_beta=faql_beta)

base_packed = base_ql.pack()

average_rewards = np.empty((args.avgcount, args.test_points))
average_test_times = np.empty((args.avgcount, args.test_points))
average_train_times = np.empty((args.avgcount, args.test_points))
stddev_test_times = np.empty((args.avgcount, args.test_points))
stddev_train_times = np.empty((args.avgcount, args.test_points))
fully_trained_learners = np.empty(args.avgcount, dtype=h5py.special_dtype(vlen=bytes))

train_first = hrl_app.run_single_training.si(base_packed, train_iters,
                                             measure_time=True)
parallel_training_first = [train_first for _ in range(args.avgcount)]
logger.debug("First training")
train_results = utils.sched.retry_group(parallel_training_first, timeout=15*60)
train_results = [v for _, v in train_results]
logger.debug("First training done")

parallel_test = []
for i, r in enumerate(train_results):
    if i == 0:
        hist_label = 'hist_first'
    else:
        hist_label = None
    parallel_test.append(hrl_app.run_single_test.si(r['learner'],
                                                    args.test_iters,
                                                    hist_label=hist_label,
                                                    measure_time=True))
    average_train_times[i, 0] = r['avg_train_time']
    stddev_train_times[i, 0] = r['std_train_time']

for j in range(0, args.test_points):
    logger.debug("Testing point {:d}".format(j))
    test_results = utils.sched.retry_group(parallel_test, timeout=600)
    test_results = np.array([v for _, v in test_results], dtype=object)
    logger.debug("Testing {:d} done".format(j))

    average_rewards[:, j] = take_item('avg_reward')(test_results)
    average_test_times[:, j] = take_item('avg_test_time')(test_results)
    stddev_test_times[:, j] = take_item('std_test_time')(test_results)
    if j == 0:
        histkey_first = test_results[0]['history_key']
    elif j == args.test_points - 1:
        histkey_last = test_results[0]['history_key']

    if j == args.test_points - 1:
        break

    parallel_training = [hrl_app.run_single_training.si(r['learner'],
                                                        train_iters,
                                                        measure_time=True)
                         for r in train_results]
    logger.debug("Training point {:d}".format(j+1))
    train_results = utils.sched.retry_group(parallel_training, timeout=15*60)
    train_results = [v for _, v in train_results]
    logger.debug("Training {:d} done".format(j+1))

    if j == args.test_points - 1:
        for i, r in enumerate(train_results):
            learner = Learner.unpack(r['learner'])
            fully_trained_learners[i] = learner.pack(size_thresh=None)

    parallel_test = []
    for i, r in enumerate(train_results):
        if i == 0 and j + 1 == args.test_points - 1:
            hist_label = 'hist_last'
        else:
            hist_label = None
        parallel_test.append(hrl_app.run_single_test.si(r['learner'],
                                                        args.test_iters,
                                                        hist_label=hist_label,
                                                        measure_time=True))
        average_train_times[i, j+1] = r['avg_train_time']
        stddev_train_times[i, j+1] = r['std_train_time']

with h5py.File(args.output, 'w') as h5f:
    h5f.attrs['git_sha1'] = hrl.GIT_SHA1

    g = h5f.create_group('plot_data')
    g.attrs['params'] = utils.pack_objects(
        args=args,
        average_count=args.avgcount,
        grid=multigrid,
        learner=base_ql,
        test_iterations=args.test_iters,
        test_points=args.test_points,
        training_iterations=train_iters,
    )

    utils.pack.np2hdfds(
        g,
        avg_rewards=average_rewards,
        avg_test_times=average_test_times,
        avg_train_times=average_train_times,
        std_test_times=stddev_test_times,
        std_train_times=stddev_train_times,
        fully_trained_learners=fully_trained_learners,
    )

    g_first = h5f.create_group('hist_first')
    tmp_first = utils.aws.aws_fetch_data(histkey_first)
    utils.pack.insert_hdf(tmp_first, g_first)
    tmp_first.close()

    g_last = h5f.create_group('hist_last')
    tmp_last = utils.aws.aws_fetch_data(histkey_last)
    utils.pack.insert_hdf(tmp_last, g_last)
    tmp_last.close()

if args.upload:
    key = utils.aws_upload_data(args.output, 'multi_agent')
    logger.info("Uploaded with key {!s}".format(key))
else:
    logger.warning("Running with --no-upload:"
                   " the results were not uploaded")
