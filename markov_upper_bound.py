import operator
import itertools

import numpy as np
from scipy.special import binom
import matplotlib.pyplot as plt

from hrl.utils.iter import dotproduct

def nstep_2state_MC(pon, poff, n, s_from, s_to):
    a = pon
    b = poff
    X = [b, a,
         b, a]
    Y = [a, -a,
         -b, b]
    idx = 2*s_from + s_to
    return (X[idx] + Y[idx] * (1 - a - b)**n) / (a+b)

def prob_on_count(pon, poff, N, steps, on_from, on_to):
    ponon_n = nstep_2state_MC(pon, poff, steps, 1, 1)
    poffon_n = nstep_2state_MC(pon, poff, steps, 0, 1)

    def on2on(l):
        if l > on_from: return 0
        return _binprob(on_from, ponon_n, l)

    def off2on(l):
        if on_to - l > N - on_from: return 0
        return _binprob(N - on_from, poffon_n, on_to - l)

    all_on2on = map(on2on, range(on_to + 1))
    all_off2on = map(off2on, range(on_to + 1))

    return dotproduct(all_on2on, all_off2on)

def _binprob(N, p, k):
    if N == 0:
        assert(k == 0)
        return 1
    return binom(N, k) * p**k * (1-p)**(N-k)

def binprob(N, p, T, k):
    bin_p = 1 - (1-p)**T
    return _binprob(N, bin_p, k)

def transition_prob(N, pon, poff, s_from, s_to, thresh=1e-16, can_turn_off=False):
    if s_from == 0:
        return binprob(N, pon, 1, s_to)
    elif not can_turn_off and s_to < s_from - 1:
        return 0
    else:
        def prob_fixed_i(i):
            if can_turn_off:
                bin = prob_on_count(pon, poff, N-1, 2+i, s_from-1, s_to)
            else:
                bin = prob_on_count(pon, 0, N-1, 2+i, s_from-1, s_to)
            i_prob = (1 - poff)**i * poff
            return bin * i_prob

        def not_too_small(args):
            (i, p) = args
            return i <= 10/poff or p >= thresh

        prob_all_is = map(prob_fixed_i, itertools.count())
        filtered_prob = itertools.takewhile(not_too_small, enumerate(prob_all_is))
        total_prob = sum(map(operator.itemgetter(1), filtered_prob))
        return total_prob

N = 5*5 - 1  # Exclude start
pon = 1/30
poff = 1/5

P = np.empty((N, N))
for j in range(N):
    for k in range(N):
        P[j, k] = transition_prob(N, pon, poff, j, k, can_turn_off=True)

Psum = np.sum(P, axis=1)
for j in range(N):
    assert(round(Psum[j], 8) == 1)

PminI = P - np.eye(N)
linsys = np.concatenate((PminI, np.ones((N, 1))), axis=1)
coeff = np.zeros(N+1)
coeff[-1] = 1

pinv = np.linalg.pinv(linsys)
stationary = np.dot(coeff, pinv)

good_avgreward = 1/poff
bad_avgreward = 0
avgRvec = [bad_avgreward] + [good_avgreward] * (N-1)

good_avgtime = 2 + 1/poff
bad_avgtime = 1
avgTvec = [bad_avgtime] + [good_avgtime] * (N-1)

avgreward = dotproduct(stationary, avgRvec) / dotproduct(stationary, avgTvec)
print("Average long-term reward = {:f}".format(avgreward))

plt.figure("Stationary N_ON")
plt.plot(range(N), stationary)

plt.show()