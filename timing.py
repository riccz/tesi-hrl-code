import argparse
import logging
import math
import operator
import sys
import warnings

from celery import chain
import numpy as np

from hrl import qlearn, random_walker, lookahead, mcts
from hrl import utils
from hrl.common import grid, output_opts_parser, multigrid
import hrl
import hrl_app

with warnings.catch_warnings():
    warnings.simplefilter("ignore", FutureWarning)
    import h5py

logger = logging.getLogger(__name__)


def take_item(item, otype=float):
    return np.vectorize(operator.itemgetter(item), otypes=(otype,))


logger.info("Running with commit {!s}".format(hrl.GIT_SHA1))

parser = argparse.ArgumentParser(description='Runtime measurements',
                                 allow_abbrev=False,
                                 parents=[output_opts_parser])

_sm_group = parser.add_mutually_exclusive_group()
_sm_group.add_argument("--single", help="Run with a single agent",
                       action='store_false',
                       dest='multi')
_sm_group.add_argument("--multi", help="Run with multiple agents",
                       action='store_true',
                       dest='multi')
_sm_group.set_defaults(multi=False)

_tt_group = parser.add_mutually_exclusive_group()
_tt_group.add_argument('--train', action='store_true',
                       help='Test the training time', dest='train')
_tt_group.add_argument('--test', action='store_false',
                       help='Test the testing time', dest='train')
_tt_group.set_defaults(train=False)

parser.add_argument("--method", choices=["RW", "LA", "RUQL", "FAQL", "QL"],
                     help='Method to test', required=True)
parser.add_argument("--steps", type=int, help='Steps per run', default=10000)
parser.add_argument("--train_steps", type=int, help='Steps to train before testing',
                    default=100000)
parser.add_argument("--avgcount", type=int, help='Runs to average', default=50)
args = parser.parse_args()

if args.method in ['RW', 'LA'] and args.train:
    print("Method {!r} is not trainable".format(args.method))
    sys.exit(1)

if not args.multi:  # Single agent params
    if args.method == 'RW':
        base_ql = random_walker.RandomWalker(grid)
    elif args.method == 'LA':
        base_ql = lookahead.LookaheadLearner(grid,
        gamma=0.6, la_steps=3, cache_size=0)
    elif args.method == 'RUQL':
        epsilon = qlearn.lin_decay(start=0.400,
                            const=7.944949e-08,
                            stop=1e-3)
        expl = qlearn.EpsilonGreedyPolicy(epsilon)
        alpha = qlearn.hyp_decay(start=0.557, exp=0.509798)
        base_ql = qlearn.IndepQLearners(grid, alpha=alpha, gamma=0.630,
                                        explore_policy=expl,
                                        update_method='RUQL')
    elif args.method == 'QL':
        tau = qlearn.lin_decay(start=0.059,
                            const=1.535910e-08,
                            stop=1e-3)
        expl = qlearn.SoftmaxPolicy(tau)
        alpha = qlearn.hyp_decay(start=0.718, exp=0.505740)
        base_ql = qlearn.IndepQLearners(grid, alpha=alpha, gamma=0.603,
                                        explore_policy=expl,
                                        update_method='QL')
    elif args.method == 'FAQL':
        tau = qlearn.lin_decay(start=1.763,
                               const=6.979787e-07,
                               stop=1e-3)
        expl = qlearn.SoftmaxPolicy(tau)
        alpha = qlearn.hyp_decay(start=0.545, exp=0.522846)
        faql_beta = qlearn.hyp_decay(start=0.192, exp=0.014)
        base_ql = qlearn.IndepQLearners(grid, alpha=alpha, gamma=0.661,
                                        explore_policy=expl,
                                        update_method='FAQL',
                                        faql_beta=faql_beta)
else:  # Multiagent params
    if args.method == 'RW':
        base_ql = random_walker.RandomWalker(multigrid)
    elif args.method == 'LA':
        base_ql = mcts.MCTS(multigrid, gamma=0.6,
                            max_depth=2,
                            rollout_count=10,
                            uct_c=np.sqrt(2),
                            max_fanout=16)
    elif args.method == 'RUQL':
        tau = qlearn.lin_decay(start=0.755,
                               const=2.531404e-07,
                               stop=1e-3)
        expl = qlearn.SoftmaxPolicy(tau)
        alpha = qlearn.hyp_decay(start=0.689, exp=0.511096)
        base_ql = qlearn.IndepQLearners(multigrid, alpha=alpha, gamma=0.724,
                                        explore_policy=expl,
                                        update_method='RUQL')
    elif args.method == 'QL':
        tau = qlearn.lin_decay(start=1.271,
                               const=3.098697e-08,
                               stop=1e-3)
        expl = qlearn.SoftmaxPolicy(tau)
        alpha = qlearn.hyp_decay(start=0.970, exp=0.529704)
        base_ql = qlearn.IndepQLearners(multigrid, alpha=alpha, gamma=0.436,
                                        explore_policy=expl,
                                        update_method='QL')
    elif args.method == 'FAQL':
        tau = qlearn.lin_decay(start=1.210,
                               const=4.442685e-07,
                               stop=1e-3)
        expl = qlearn.SoftmaxPolicy(tau)
        alpha = qlearn.hyp_decay(start=0.565, exp=0.704049)
        faql_beta = qlearn.hyp_decay(start=0.061, exp=0.004)
        base_ql = qlearn.IndepQLearners(multigrid, alpha=alpha, gamma=0.437,
                                        explore_policy=expl,
                                        update_method='FAQL',
                                        faql_beta=faql_beta)
base_packed = base_ql.pack()

if args.train:
    one_task = hrl_app.time_training.si(base_packed, args.steps)
    all_tasks = [one_task for _ in range(args.avgcount)]
    results = utils.sched.retry_group(all_tasks, timeout=4*3600)
else:
    if args.method not in ['RW', 'LA']:
        tr_task = hrl_app.run_single_training.si(base_packed, args.train_steps)
        one_task = chain(tr_task, hrl_app.time_test.s(test_iters=args.steps))
    else:
        one_task = hrl_app.time_test.si(base_packed, test_iters=args.steps)
    all_tasks = [one_task for _ in range(args.avgcount)]
    results = utils.sched.retry_group(all_tasks, timeout=4*3600)

times = np.empty((args.steps, args.avgcount))
for j, r in enumerate(map(operator.itemgetter(1), results)):
    times[:, j] = r['time_samples']
    logger.debug("Single-run average time: %e", times[:, j].mean())

with h5py.File(args.output, 'w') as h5f:
    h5f.attrs['git_sha1'] = hrl.GIT_SHA1

    g = h5f.create_group('time_data')
    g.attrs['params'] = utils.pack_objects(
        args=args,
        average_count=args.avgcount,
        learner=base_ql,
        steps=args.steps,
    )

    utils.pack.np2hdfds(g, times=times)

if args.upload:
    prefix = 'multi' if args.multi else 'single'
    label = prefix + '_timing_' + args.method.lower()
    key = utils.aws_upload_data(args.output, label)
    logger.info("Uploaded with key {!s}".format(key))
else:
    logger.warning("Running with --no-upload:"
                   " the results were not uploaded")
