import operator
import itertools

import numpy as np
from scipy.special import binom
import matplotlib.pyplot as plt

def avgdelta(mu_m, mu_h, poff):
    return mu_m + mu_h * (1/poff + 1)

def avg_positions(mu_m, mu_h, poff):
    the_avgdelta = avgdelta(mu_m, mu_h, poff)
    return (1 / the_avgdelta, 1 /the_avgdelta + 1)

def avg_reward(mu_m, mu_h, poff):
    (Klow, Khigh) = avg_positions(mu_m, mu_h, poff)
    return (Klow * 1/poff, Khigh * 1/poff)

def avg_inner_cycle(mu_m, mu_h, poff):
    (Klow, Khigh) = avg_positions(mu_m, mu_h, poff)
    the_avgdelta = avgdelta(mu_m, mu_h, poff)
    return (
        Klow * the_avgdelta,
        Khigh * the_avgdelta
    )

# T = 10
# L = 100
poff = 1/5
def mu_m(T):
    return 0.038/100 * 8 * T
def mu_h(T):
    return 0.122/100 * T

def avg_long_term_reward(mu_m, mu_h, poff):
    K = avg_positions(mu_m, mu_h, poff)[1]
    avgR = 1/poff * K
    avgT = 1 + 2 * K + 1/poff * K
    return avgR / avgT

final_avgR = avg_long_term_reward(mu_m(15), mu_h(15), poff)
print("Avg upper bound = {:f}".format(final_avgR))

plt.figure()
Ts = np.linspace(3, 100, 4096)
Rs = np.array([avg_long_term_reward(mu_m(T), mu_h(T), poff) for T in Ts])
Ks = np.array([avg_positions(mu_m(T), mu_h(T), poff) for T in Ts])
plt.plot(Ts, Rs)
plt.figure()
plt.plot(Ts, Ks)

plt.show()